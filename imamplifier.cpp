#include "imamplifier.h"

IMAmplifier::IMAmplifier(QObject *parent) : QObject(parent)
{
    m_canHandle = -1;
    m_connected = false;
    m_busOn = false;
    m_isOpen = false;
    m_state = IM_StateNone;
    m_da = 255; // broadcast address (was: 129, than means the first IM1 device)
    m_sa = 128; // it's always the central module IM2
    m_listenOnly = false;
    m_saveToLog = false;
    connect(this, &IMAmplifier::stopStarted, this, &IMAmplifier::onStopStarted);
    connect(this, &IMAmplifier::initStarted, this, &IMAmplifier::onInitStarted);
    connect(this, &IMAmplifier::acksUpdated, this, &IMAmplifier::onAcksUpdated);
    connect(this, &IMAmplifier::portChanged, this, &IMAmplifier::onPortChanged);
    connect(this, &IMAmplifier::setActiveChans, this, &IMAmplifier::onSetActiveChans);
    canInitializeLibrary();
}

double IMAmplifier::getAverage(int ch, int filterLength) {
    return m_average[ch-1].GetAverage(filterLength == 0? 1: filterLength);
}

double IMAmplifier::getAverage(int ch) {
    int filterLength = m_filterLength[ch-1];
    if(filterLength == 0) {
        filterLength = 1;
    }
    return m_average[ch-1].GetAverage(filterLength);
}

uint32_t IMAmplifier::getCanId(int amp)
{
    return m_canIds[amp-1];
}

void IMAmplifier::setCanId(int amp, uint32_t value)
{
    m_canIds[amp-1] = value;
}

uint32_t IMAmplifier::getPgaGain(int amp)
{
    return m_pgaGain[amp-1];
}

void IMAmplifier::setPgaGain(int amp, uint32_t value)
{
    m_pgaGain[amp-1] = value;
}

int IMAmplifier::getFilterLength(int chan)
{
    return m_filterLength[chan-1];
}

void IMAmplifier::setFilterLength(int chan, int value)
{
    m_filterLength[chan-1] = value;
}

int IMAmplifier::getWeightingLimit(int chan)
{
    return m_weightingLimit[chan-1];
}

void IMAmplifier::setWeightingLimit(int chan, int value)
{
    m_weightingLimit[chan-1] = value;
}

double IMAmplifier::getSensitivity(int chan)
{
    return m_sensitivity[chan-1];
}

void IMAmplifier::setSensitivity(int chan, double value)
{
    m_sensitivity[chan-1] = value;
}

void IMAmplifier::setPort(QString port) {
    if(m_connected) {
        if(m_canHandle >= 0) {
            if(m_busOn) {
                canBusOff(m_canHandle);
                m_busOn = false;
            }
            canClose(m_canHandle);
            m_canHandle = -1;
            m_connected = false;
        }
    }
    int ch = QString(port).remove(0, 3).toInt();
    m_canHandle = canOpenChannel(ch, canOPEN_ACCEPT_VIRTUAL);
    if(m_canHandle < 0) {
        qDebug() << QString("IMAmplifier::setPort: canOpenChannel (%1) failed").arg(ch);
        emit portChanged("");
        return;
    }
    m_connected = true;
    int br = -1; // corresponds to 1M speed
    canStatus stat = canSetBusParams(m_canHandle, br, 0, 0, 0, 0, 0); // -1 corresponds to 1M speed
    if(stat < 0) {
        char buf[128];
        canGetErrorText(stat, buf, 128);
        qDebug() << "IMAmplifier::setPort: canSetBusParam failed:" << buf;
        emit portChanged("");
        return;
    }
    stat = canSetBusOutputControl(m_canHandle, canDRIVER_NORMAL);
    if (stat < 0) {
        char buf[128];
        canGetErrorText(stat, buf, 128);
        qDebug() << "IMAmplifier::setPort: canSetBusOutputControl failed:" << buf;
        emit portChanged("");
        return;
    }
    stat = canBusOn(m_canHandle);
    if (stat < 0) {
        char buf[128];
        canGetErrorText(stat, buf, 128);
        qDebug() << "IMAmplifier::setPort: canBusOn failed:" << buf;
        emit portChanged("");
        return;
    }
    m_busOn = true;
    m_port = port;
    if(m_listenOnly) {
        emit portChanged(m_port);
        return;
    }
    long id = j1939_set_ExtId(PRIORITY_CONTROL, PGN_STOP_MEASURING, m_da, m_sa);
    char data[8] = {0};
    canWrite(m_canHandle, id, data, 8, canMSG_EXT);
    emit stopStarted();
}

void IMAmplifier::run() {
    m_initTimer = new QTimer(this);
    m_initTimer->setSingleShot(true);
    m_initTimer->setInterval(1000);
    connect(m_initTimer, &QTimer::timeout, this, &IMAmplifier::onInitTimeout);
    m_running = true;
    qDebug() << "IMAmplifier started";
    while (m_running) {
        long id;
        unsigned char data[8];
        unsigned int dlc, flags;
        unsigned long timestamp;
        canStatus stat = canRead(m_canHandle, &id, data, &dlc, &flags, &timestamp);
        if(stat >= 0) {
            if(m_saveToLog) {
                writeToLog(timestamp, id, dlc, data);
            }
            if(flags & canMSG_ERROR_FRAME) {
                qDebug() << "canRead() error";
                continue;
            }
            uint32_t pgn = j1939_get_pgn(id);
            uint32_t sa = J1939_GET_SA(id);
            switch (pgn) {
            case PGN_START_MEASURING:
                if(sa == 128) {
                    emit measuringStarted();
                }
            case PGN_ACKNOWLEDGMENT:
                if(data[0] == 0) { // success
                    m_acks[sa] = true;
                    emit acksUpdated();
                }
                break;
            case PGN_DATA_ADC:
                qint32 chan1Weight, chan2Weight;
                chan1Weight = (data[2] << 16) | (data[1] << 8) | data[0];
                chan2Weight = (data[5] << 16) | (data[4] << 8) | data[3];
                if(chan1Weight & 0x00800000) {
                    chan1Weight |= 0xff000000;
                }
                if(chan2Weight & 0x00800000) {
                    chan2Weight |= 0xff000000;
                }
                for(int i = 0; i < 6; i++) {
                    if(m_canIds[i] == sa) {
                        int weightingLimit = m_weightingLimit[2*i];
                        double sensitivity = m_sensitivity[2*i];
                        int pgaGain = m_pgaGain[i];
                        double k = (double)(weightingLimit * 1000) / (double) (sensitivity * pgaGain * 8388608); // 8388608 is 2^23
                        m_average[2*i].Add(k * chan1Weight);
                        weightingLimit = m_weightingLimit[2*i+1];
                        sensitivity = m_sensitivity[2*i+1];
                        k = (double)(weightingLimit * 1000) / (double) (sensitivity * pgaGain * 8388608);
                        m_average[2*i+1].Add(k * chan2Weight);
                    } //else {
                        //m_average[2*i].Add(0);
                        //m_average[2*i+1].Add(0);
                    //}
                }
            }
        }
        QApplication::processEvents();
    }
    qDebug() << "IMAmplifier stopped";
    emit finished();
}

void IMAmplifier::onStopStarted() {
    m_state = IM_StateStop;
    m_acks.clear();
    m_initTimer->start();
}

void IMAmplifier::onInitStarted()
{
    m_state = IM_StateInit;
    m_acks.clear();
}

void IMAmplifier::onAcksUpdated() {
    int count = m_acks.count();
    if(m_listenOnly) {
        return;
    }
    if(count == 1) {
        if(m_state == IM_StateStop) {
            long id = j1939_set_ExtId(PRIORITY_INFO, PGN_INIT_IM, m_da, m_sa);
            char data[8] = {0};
            data[0] = 0; // 0 = 100 SPS, 1 = 200 SPS
            data[1] = 0; // sync period in seconds. 0 = disabled
            data[2] = 1; // without req
            data[3] = 10; // heartbeat period in seconds.
            data[4] = (char)0xFF;
            data[5] = (char)0xFF;
            data[6] = (char)0xFF;
            data[7] = (char)0xFF;
            canWrite(m_canHandle, id, data, 8, canMSG_EXT); // init
            emit initStarted();
            return;
        }
        if(m_state == IM_StateInit) {
            emit portChanged(m_port);
            long id = j1939_set_ExtId(PRIORITY_CONTROL, PGN_START_MEASURING, m_da, m_sa);
            char data[8] = {0};
            canWrite(m_canHandle, id, data, 8, canMSG_EXT); // start
        }
    }
}

void IMAmplifier::onPortChanged(QString port)
{
    qDebug() << "IMAmplifier::onPortChanged:" << port;
    m_initTimer->stop();
    m_isOpen = port != "";
    qDebug() << "m_isOpen:" << isOpen();
}

void IMAmplifier::onInitTimeout()
{
    emit portChanged("");
}

void IMAmplifier::onSetActiveChans(QList<int> chans) {
    m_activeChans.clear();
    for(QList<int>::iterator i = chans.begin(); i != chans.end(); i++) {
        m_activeChans[*i] = true;
    }
}

bool IMAmplifier::isOpen() const
{
    return m_isOpen;
}

void IMAmplifier::refresh()
{
    setPort(m_port);
}

void IMAmplifier::writeToLog(unsigned long timestamp, long id, unsigned int dlc, unsigned char *pData)
{
    QString l;
    l.append(QString().asprintf("%ld\tID:0x%08X\tData: ", timestamp, (unsigned int)id));
    for(quint32 i = 0; i < dlc; i++) {
        l.append(QString().asprintf("%02X", (quint8)pData[i]));
    }
    QFile file(m_logFilename);
    if(file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
        QTextStream stream(&file);
        stream << l << "\n";
        file.close();
    }
}

void IMAmplifier::setSaveToLog(bool saveToLog)
{
    int fileIndex = 1;
    m_saveToLog = saveToLog;
    if(saveToLog) {
        QString logDir = QApplication::applicationDirPath() + "/logs/";
        if(!QDir(logDir).exists()) {
            QDir().mkdir(logDir);
        }
        do {
            m_logFilename = logDir + QDate::currentDate().toString("ddMMyyyy_%1.txt").arg(fileIndex++);
        } while(QFile::exists(m_logFilename));
        qDebug() << "new log filename:" << m_logFilename;
    }
}

void IMAmplifier::setListenOnly(bool listenOnly)
{
    m_listenOnly = listenOnly;
}
