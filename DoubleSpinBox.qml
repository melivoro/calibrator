import QtQuick 2.15
import QtQuick.Controls 2.15

SpinBox {
    id: spinbox
    from: 0
//    value: 1000000
    to: 2000000
    stepSize: 1

    property int decimals: 6
    property real realValue: value / 1000000

    validator: DoubleValidator {
        bottom: Math.min(spinbox.from, spinbox.to)
        top:  Math.max(spinbox.from, spinbox.to)
    }

    textFromValue: function(value, locale) {
        return Number(value / 1000000).toLocaleString(locale, 'f', spinbox.decimals)
    }

    valueFromText: function(text, locale) {
        return Number.fromLocaleString(locale, text) * 1000000
    }

    onValueChanged: {
        realValue = value / 1000000
    }
}
