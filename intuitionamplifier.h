#ifndef INTUITIONAMPLIFIER_H
#define INTUITIONAMPLIFIER_H

#include <QDebug>
#include <QTime>
#include "serialport.h"
#include "circularbuffer.h"

class IntuitionAmplifier : public SerialPort
{
    Q_OBJECT
public:
    explicit IntuitionAmplifier(QObject *parent = nullptr);
    double getAverage();

signals:
    void newData(qint64, double);

private:
    CircularBuffer average;
    void checkData(QString);
    QMap<int, bool> m_activeChans;
};

#endif // INTUITIONAMPLIFIER_H
