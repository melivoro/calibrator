#ifndef IM2AMPLIFIER_H
#define IM2AMPLIFIER_H

#include <QApplication>
#include <QDir>
#include <QDebug>
#include <QTime>
#include <QTimer>
#include "serialport.h"
#include "circularbuffer.h"

class IM2Amplifier : public SerialPort
{
    Q_OBJECT
public:
    explicit IM2Amplifier(QObject *parent = nullptr);
    double getAverage();
    void setSaveToLog(bool saveToLog);

signals:
    void newData(qint64, int, double);

private slots:
    void onComPortChanged(QString);
    void onTimeout();
    void onReadyRead();

public slots:
    void onMeasuringStarted();
    void onCanPortChanged(QString);

private:
    QTimer *m_timer;
    CircularBuffer average;
    bool m_started;
    bool m_saveToLog;
    QString m_logFilename;
    void checkData(QString);
    void sendDataRequest();
    void writeToLog(QString);
};

#endif // IM2AMPLIFIER_H
