#include "im2amplifier.h"

IM2Amplifier::IM2Amplifier(QObject *parent): SerialPort(parent) {
    m_started = false;
    setBaudRate(9600);
    m_timer = new QTimer(this);
    m_timer->setInterval(25);
    connect(m_timer, &QTimer::timeout, this, &IM2Amplifier::onTimeout);
    connect(this, &IM2Amplifier::portChanged, this, &IM2Amplifier::onComPortChanged);
}

double IM2Amplifier::getAverage()
{
    return average.GetAverage(10);
}

void IM2Amplifier::setSaveToLog(bool saveToLog)
{
    int fileIndex = 1;
    m_saveToLog = saveToLog;
    if(saveToLog) {
        QString logDir = QApplication::applicationDirPath() + "/logs/";
        if(!QDir(logDir).exists()) {
            QDir().mkdir(logDir);
        }
        do {
            m_logFilename = logDir + QDate::currentDate().toString("rs485ddMMyyyy_%1.txt").arg(fileIndex++);
        } while(QFile::exists(m_logFilename));
        qDebug() << "new log filename:" << m_logFilename;
    }
}

void IM2Amplifier::onComPortChanged(QString port)
{
    if(port == "") {
        return;
    }
    m_timer->stop();
    if(!m_started) {
        return;
    }
    m_timer->start();
}

void IM2Amplifier::onTimeout()
{
    sendDataRequest();
}

void IM2Amplifier::onReadyRead()
{
    static char pkg[9];
    static int i;
    int n = read(pkg + i, 1);
    if(n != 1) {
        return;
    }
    if(i == 0) {
        if((int)pkg[i] == 1) {
            i++;
            return;
        } else {
            i = 0;
            return;
        }
    }
    if(i < 3) {
        if((int)pkg[i] == 4) {
            i++;
            return;
        } else {
            i = 0;
            return;
        }
    } else {
        if(i == 8) {
            i = 0;
            checkData(QString(QByteArray(pkg).toHex()));
            return;
        } else {
            i++;
        }
    }
}

void IM2Amplifier::onMeasuringStarted() {
    if(m_started == true) {
        return;
    }
    m_started = true;
    setPort(m_port);
}

void IM2Amplifier::onCanPortChanged(QString)
{
    m_started = false;
    setPort(m_port);
}

void IM2Amplifier::checkData(QString data)
{
    QString hex = data;
    if(data.length() != 18) {
        return;
    }
    data = data.mid(6, 8);
    QByteArray arr = QByteArray::fromHex(data.toLatin1());
    quint32 word = quint32((quint8(arr.at(2)) << 24) | (quint8(arr.at(3)) << 16) | (quint8(arr.at(0)) <<  8) | (quint8(arr.at(1)) <<  0));
    float *f = reinterpret_cast<float *>(&word);
    double weight = (double) *f;
    average.Add(weight);
    QString toLog = QString("%1 (%2 kg)").arg(hex).arg(weight);
    if(m_saveToLog) {
        writeToLog(toLog);
    }
}

void IM2Amplifier::sendDataRequest()
{
    QString hexString = "01040000000271cb";
    QByteArray hex = QByteArray::fromHex(hexString.toLatin1());
    if(isOpen()) {
        write(hex, hex.size());
    }
}

void IM2Amplifier::writeToLog(QString data)
{
    QFile file(m_logFilename);
    if(file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
        QTextStream stream(&file);
        stream << QDateTime::currentSecsSinceEpoch() << "\t" << data << "\n";
        file.close();
    }
}
