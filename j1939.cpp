//#include "mainwindow.h"
//#include "ui_mainwindow.h"
#include "canlib.h"
#include "j1939.h"

//-----------------------------------------------------------------------------
/**
 * @brief Get number PGN from CAN extended identifier.
 * @param ExtId - CAN extended identifier.
 * @return number PGN.
 */
uint32_t j1939_get_pgn(uint32_t ExtId)
{
  uint8_t pf = J1939_GET_PF(ExtId);
  uint32_t pgn;

  if (pf < 240) { pgn = (ExtId & 0x01FF0000) >> 8;}
  else          { pgn = (ExtId & 0x01FFFF00) >> 8;}
  return pgn;
}

//-----------------------------------------------------------------------------
uint32_t j1939_set_ExtId(J1939_PRIORITY_Type priority, J1939_PGN_Type pgn, uint8_t da, uint8_t sa)
{
  uint8_t ps  =  ((uint32_t)pgn) & 0xFF;
  uint8_t pf  = (((uint32_t)pgn) >>  8) & 0xFF;
  uint8_t dp  = (((uint32_t)pgn) >> 16) & 0x01;
  uint8_t edp = 0;

  uint32_t ExtId;

  if (pf < 240)
  {
    ExtId = (((uint32_t)priority & 0x07) << 26) | (((uint32_t)edp & 0x01) << 25) | (((uint32_t)dp  & 0x01) << 24) |
            ((uint32_t)pf << 16) | ((uint32_t)da <<  8) | (uint32_t)sa;
  }
  else
  {
    ExtId = (((uint32_t)priority & 0x07) << 26) | (((uint32_t)edp & 0x01) << 25) | (((uint32_t)dp  & 0x01) << 24) |
            ((uint32_t)pf << 16) | ((uint32_t)ps <<  8) | (uint32_t)sa;
  }
  return ExtId;
}

//-----------------------------------------------------------------------------
void can_send_request(int canHandle, J1939_PGN_Type pgn, uint8_t da, uint8_t sa)
{
    long id = j1939_set_ExtId(PRIORITY_REQUEST, PGN_REQUEST, da, sa);

    char data[8] = {0};
    data[0] = (uint8_t) (pgn & 0xFF);
    data[1] = (uint8_t)((pgn >>  8) & 0xFF);
    data[2] = (uint8_t)((pgn >> 16) & 0xFF);

    canWrite(canHandle, id, data, 3, canMSG_EXT);
}

/*----------------------------------------------------------------------------*/
/* Print Acknowledgment */
/*----------------------------------------------------------------------------*/
char *Acknowledgment2char(uint8_t ack)
{
  switch (ack)
  {
    case J1939_ACK:            return "ACK";            break;
    case J1939_NACK:           return "NACK";           break;
    case J1939_ACCESS_DENIED : return "ACCESS DENIED";  break;
    case J1939_CANNOT_RESPOND: return "CANNOT RESPOND"; break;
  }
  return "UNKNOWN";
}
