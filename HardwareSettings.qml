import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15
import QtQml.Models 2.15

RowLayout {
    property var listViewWidth
    ListView {
        id: hardwareListView
        Layout.fillHeight: true
        Layout.topMargin: 10
        Layout.bottomMargin: 10
        Layout.leftMargin: 10
        Layout.preferredWidth: listViewWidth
        spacing: 5
        clip: true
        delegate: Label {
            width: parent.width
            height: 30
            verticalAlignment: Qt.AlignVCenter
            text: txt
            color: index == hardwareListView.currentIndex ? "white" : "black"
            font.pointSize: 10
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hardwareListView.currentIndex = index
                }
            }
        }
        model: ListModel {
            ListElement {
                txt: qsTr("OT-EP01-10")
            }
            ListElement {
                txt: qsTr("Intuition 20i")
            }
            ListElement {
                txt: qsTr("IM")
            }
            ListElement {
                txt: qsTr("Tenzo-M VK-10")
            }
        }
        highlight: Rectangle {
            color: Material.accent
            y: hardwareListView.currentItem.y
            Behavior on y {
                SpringAnimation {}
            }
        }
        highlightFollowsCurrentItem: true
    }
    ToolSeparator {
        Layout.fillHeight: true
    }
    ColumnLayout {
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.topMargin: 10
        Layout.rightMargin: 10
        Layout.bottomMargin: 10
        spacing: 20
        Label {
            Layout.fillWidth: true
            font.pointSize: 20
            text: hardwareListView.currentItem.text
        }
        StackLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true
            currentIndex: hardwareListView.currentIndex
            SzobteHardwareSettings {}
            IntuitionHardwareSettings {}
            IMHardwareSettings {}
            TenzoMHardwareSettings {}
            onCurrentIndexChanged: {
                if (currentIndex < 3) {
                    comPortModel.reload()
                }
                if(currentIndex == 2) {
                    canPortModel.reload()
                }
            }
        }
    }
}
