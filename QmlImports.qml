import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15
import QtCharts 2.15
import QtQml.Models 2.15
import Qt.labs.calendar 1.0

/*
    QmlImports.qml

    Declaration of QML Imports required by project.

    This is necessary if we want to keep qml files in a folder
    separate from .pro file because  of the way qmlimportscanner works.
    If these imports are not declared, qmake will not recognize them,
    and QtQuick will not be packaged with statically built apps and imported
    at runtime.

    This must be kept in the same directory as your .pro file
*/

QtObject {}
