#include "dbworker.h"

DBWorker::DBWorker(QObject *parent) : QObject(parent) {
    int res;
    if((res = dbConnect()) != 0) {
        QApplication::exit(res);
    }
    if((res = init()) != 0) {
        QApplication::exit(res);
    }
    connect(this, &DBWorker::szobteChannelStateChanged, this, &DBWorker::onSzobteChannelStateChanged);
    connect(this, &DBWorker::imChannelStateChanged, this, &DBWorker::onImChannelStateChanged);
    szobteRefreshEnabledChansModel();
    imRefreshEnabledChansModel();
}

int DBWorker::dbConnect() {
    const QString driver("QSQLITE");
    if(QSqlDatabase::isDriverAvailable(driver)) {
        QSqlDatabase db = QSqlDatabase::addDatabase(driver);
        db.setDatabaseName(QApplication::applicationDirPath() + "/Calibrator.db");
        if(!db.open()) {
            qWarning() << "DBWorker::dbConnect - ERROR: " << db.lastError().text();
            return EOF;
        }
    } else {
        qWarning() << "DBWorker::dbConnect - ERROR: no driver " << driver << " available";
        return EOF;
    }
    return 0;
}

int DBWorker::init() {
    QSqlQuery configQuery("CREATE TABLE IF NOT EXISTS config (param TEXT PRIMARY KEY NOT NULL, value TEXT)");
    if(!configQuery.isActive()) {
        qWarning() << "DBWorker::init - ERROR: " << configQuery.lastError().text();
        return EOF;
    }
    QSqlQuery measurementSettingsQuery("CREATE TABLE IF NOT EXISTS measurementSettings (_id INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, dayIndex INTEGER, referenceIndex INTEGER, referenceSerial TEXT, calibratedIndex INTEGER, calibratedSerial TEXT, referenceChan INTEGER, calibratedSerial2 TEXT, calibratedSerial3 TEXT, calibratedSerial4 TEXT, calibratedSerial5 TEXT, calibratedSerial6 TEXT, calibratedSerial7 TEXT)");
    if(!measurementSettingsQuery.isActive()) {
        qWarning() << "DBWorker::init - ERROR: " << measurementSettingsQuery.lastError().text();
        return EOF;
    }
    QSqlQuery sensorSettingsQuery("CREATE TABLE IF NOT EXISTS sensorSettings (_id INTEGER PRIMARY KEY AUTOINCREMENT, experimentId INTEGER, chan INTEGER, chanIndex INTEGER, serial TEXT, correctionFactor REAL, sensitivity REAL, weightingLimit INTEGER, filterLength INTEGER, adcPgaGain REAL, softwareCorrectionFactor REAL)");
    if(!sensorSettingsQuery.isActive()) {
        qWarning() << "DBWorker::init - ERROR: " << sensorSettingsQuery.lastError().text();
        return EOF;
    }
    QSqlQuery experimentDataQuery("CREATE TABLE IF NOT EXISTS experimentData (_id INTEGER PRIMARY KEY AUTOINCREMENT, experimentId INTEGER, line INTEGER, chan INTEGER, data REAL)");
    if(!experimentDataQuery.isActive()) {
        qWarning() << "DBWorker::init - ERROR: " << experimentDataQuery.lastError().text();
        return EOF;
    }
    QSqlQuery singleChanResultsQuery("CREATE TABLE IF NOT EXISTS singleChanResults (_id INTEGER PRIMARY KEY AUTOINCREMENT, experimentId INTEGER, softwareCorrectionFactor REAL)");
    if(!singleChanResultsQuery.isActive()) {
        qWarning() << "DBWorker::init - ERROR: " << singleChanResultsQuery.lastError().text();
        return EOF;
    }
    QSqlQuery commentsQuery("CREATE TABLE IF NOT EXISTS comments (experimentId INTEGER PRIMARY KEY, comment varchar(255))");
    if(!singleChanResultsQuery.isActive()) {
        qWarning() << "DBWorker::init - ERROR: " << commentsQuery.lastError().text();
        return EOF;
    }
    return 0;
}

QString DBWorker::getConfigValue(QString param) {
    QSqlQuery query;
    query.prepare("SELECT value FROM config WHERE param = ?");
    query.addBindValue(param);
    if(!query.exec()) {
        qWarning() << "DBWorker::getConfigValue - ERROR: " << query.lastError().text();
    }
    QString value;
    if(query.first()) {
        value = query.value(0).toString();
    }
    return value;
}

int DBWorker::setConfigValue(QString param, QString value) {
    QSqlQuery query;
    query.prepare("INSERT OR REPLACE INTO config(param, value) VALUES(?, ?)");
    query.addBindValue(param);
    query.addBindValue(value);
    if(!query.exec()) {
        qWarning() << "DBWorker::setConfigValue - ERROR: " << query.lastError().text();
        return EOF;
    }
    return 0;
}

QString DBWorker::getSzobtePort() {
    return getConfigValue("szobtePort");
}

void DBWorker::setSzobtePort(QString port) {
    if(setConfigValue("szobtePort", port) == 0) {
        emit szobtePortChanged();
    }
}

QString DBWorker::getSzobteSerialNumber() {
    return getConfigValue("szobteSerialNumber");
}

void DBWorker::setSzobteSerialNumber(QString serialNumber) {
    if(setConfigValue("szobteSerialNumber", serialNumber) == 0) {
        emit szobteSerialNumberChanged();
    }
}

QString DBWorker::getIntuitionPort() {
    return getConfigValue("intuitionPort");
}

void DBWorker::setIntuitionPort(QString port) {
    if(setConfigValue("intuitionPort", port) == 0) {
        emit intuitionPortChanged();
    }
}

QString DBWorker::getIntuitionSerialNumber() {
    return getConfigValue("intuitionSerialNumber");
}

QString DBWorker::getTenzoMIPAddress() {
    return getConfigValue("tenzoMIPAddress");
}

int DBWorker::getTenzoMPort() {
    return getConfigValue("tenzoMPort").toInt();
}

QString DBWorker::getTenzoMSerialNumber()
{
    return getConfigValue("tenzoMSerialNumber");
}

int DBWorker::getTenzoMBigBagsWeight()
{
    return getConfigValue("tenzoMBigBagsWeight").toInt();
}

bool DBWorker::getTenzoMState()
{
    QString stringState = getConfigValue("tenzoMState");
    if(stringState == "true") {
        return true;
    }
    return false;
}

bool DBWorker::getImListenOnly()
{
    QString stringListenOnly = getConfigValue("imListenOnly");
    if(stringListenOnly == "true") {
        return true;
    }
    return false;
}

bool DBWorker::getImSaveToLog()
{
    QString stringSaveToLog = getConfigValue("imSaveToLog");
    if(stringSaveToLog == "true") {
        return true;
    }
    return false;
}

void DBWorker::getMeasurementSettings(QDate date, int dayIndex, int *referenceIndex, QString *referenceSerial, int *calibratedIndex, QString *calibratedSerial, int *referenceChan, int *experimentId) {
    *referenceIndex = *calibratedIndex = *referenceChan = *experimentId = -1;
    *referenceSerial = *calibratedSerial = "";
    QString dateString = date.toString(Qt::ISODate);
    QSqlQuery query;
    query.prepare("SELECT * FROM measurementSettings WHERE date = ? AND dayIndex = ?");
    query.addBindValue(dateString);
    query.addBindValue(dayIndex);
    if(!query.exec()) {
        qWarning() << "DBWorker::getMeasurementSettings - ERROR: " << query.lastError().text();
    }
    if(query.first()) {
        *experimentId = query.value(0).toInt();
        *referenceIndex = query.value(3).toInt();
        *referenceSerial = query.value(4).toString();
        *calibratedIndex = query.value(5).toInt();
        *calibratedSerial = query.value(6).toString();
        *referenceChan = query.value(7).toInt();
    }
}

void DBWorker::setMeasurementSettings(QDate date, int referenceIndex, QString referenceSerial, int calibratedIndex, QString calibratedSerial, int referenceChan, int* id)
{
    QString dateString = date.toString(Qt::ISODate);
    int dayIndex = -1;
    QSqlQuery query;
    query.prepare("SELECT COUNT(dayIndex) FROM measurementSettings WHERE date = ?");
    query.addBindValue(dateString);
    if(!query.exec()) {
        qWarning() << "DBWorker::setMeasurementSettings - ERROR: " << query.lastError().text();
    }
    if(query.first()) {
        dayIndex = query.value(0).toInt()+1;
    }
    query.prepare("INSERT INTO measurementSettings(date, dayIndex, referenceIndex, referenceSerial, calibratedIndex, calibratedSerial, referenceChan) VALUES(?, ?, ?, ?, ?, ?, ?)");
    query.addBindValue(dateString);
    query.addBindValue(dayIndex);
    query.addBindValue(referenceIndex);
    query.addBindValue(referenceSerial);
    query.addBindValue(calibratedIndex);
    query.addBindValue(calibratedSerial);
    query.addBindValue(referenceChan);
    if(!query.exec()) {
        qWarning() << "DBWorker::setMeasurementSettings - ERROR: " << query.lastError().text();
    }
    *id = query.lastInsertId().toInt();
}

QList<SensorSettings> DBWorker::getSensorSettingsList(int experimentId) {
    QSqlQuery query;
    query.prepare("SELECT * FROM sensorSettings WHERE experimentId = ? ORDER BY chan");
    query.addBindValue(experimentId);
    if(!query.exec()) {
        qWarning() << "DBWorker::getSensorSettingsList - ERROR: " << query.lastError().text();
    }
    QList<SensorSettings> result;
    while(query.next()) {
        SensorSettings sensor;
        sensor.setChan(query.value(2).toInt());
        sensor.setIndex(query.value(3).toInt());
        sensor.setSerial(query.value(4).toString());
        sensor.setCorrectionFactor(query.value(5).toDouble());
        sensor.setSensitivity(query.value(6).toDouble());
        sensor.setWeightingLimit(query.value(7).toInt());
        sensor.setFilterLength(query.value(8).toInt());
        sensor.setAdcPgaGain(query.value(9).toDouble());
        sensor.setSoftwareCorrectionFactor(query.value(10).toDouble());
        result.append(sensor);
    }
    return result;
}

QList<QList<double> > DBWorker::getData(int experimentId) {
    QSqlQuery query;
    query.prepare("SELECT * FROM experimentData WHERE experimentId = ? ORDER BY line");
    query.addBindValue(experimentId);
    if(!query.exec()) {
        qWarning() << "DBWorker::getSensorSettingsList - ERROR: " << query.lastError().text();
    }
    QList<QList<double>> result;
    QList<double> line = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int i = 0;
    while(query.next()) {
        int lineNum = query.value(2).toInt();
        int chan = query.value(3).toInt();
        double value = query.value(4).toDouble();
        if(lineNum == i) {
            line[chan-1] = value;
        } else {
            result.append(line);
            line = line = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            line[chan-1] = value;
            i = lineNum;
        }
    }
    result.append(line);
    return result;
}

void DBWorker::setSensorSettings(int experimentId, int chan, int index, QString serial, double correctionFactor, double sensitivity, int weightingLimit, int filterLength, double adcPgaGain, double softwareCorrectionFactor) {
    QSqlQuery query;
    query.prepare("INSERT INTO sensorSettings(experimentId, chan, chanIndex, serial, correctionFactor, sensitivity, weightingLimit, filterLength, adcPgaGain, softwareCorrectionFactor) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    query.addBindValue(experimentId);
    query.addBindValue(chan);
    query.addBindValue(index);
    query.addBindValue(serial);
    query.addBindValue(correctionFactor);
    query.addBindValue(sensitivity);
    query.addBindValue(weightingLimit);
    query.addBindValue(filterLength);
    query.addBindValue(adcPgaGain);
    query.addBindValue(softwareCorrectionFactor);
    if(!query.exec()) {
        qWarning() << "DBWorker::setSensorSettings - ERROR: " << query.lastError().text();
    }
}

void DBWorker::setExperimentData(int experimentId, int line, int chan, double data) {
    QSqlQuery query;
    query.prepare("INSERT INTO experimentData(experimentId, line, chan, data) VALUES(?, ?, ?, ?)");
    query.addBindValue(experimentId);
    query.addBindValue(line);
    query.addBindValue(chan);
    query.addBindValue(data);
    if(!query.exec()) {
        qWarning() << "DBWorker::setExperimentData - ERROR: " << query.lastError().text();
    }
}

void DBWorker::setSingleChanResults(int experimentId, double softwareCorrectionFactor)
{
    QSqlQuery query;
    query.prepare("INSERT INTO singleChanResults(experimentId, softwareCorrectionFactor) VALUES(?, ?)");
    query.addBindValue(experimentId);
    query.addBindValue(softwareCorrectionFactor);
    if(!query.exec()) {
        qWarning() << "DBWorker::setSingleChanResults - ERROR: " << query.lastError().text();
    }
}

QString DBWorker::getComment(QDate date, int dayIndex) {
    QString dateString = date.toString(Qt::ISODate);
    QString comment;
    int experimentId = -1;
    QSqlQuery experimentIdQuery;
    experimentIdQuery.prepare("SELECT _id FROM measurementSettings WHERE date = ? AND dayIndex = ?");
    experimentIdQuery.addBindValue(dateString);
    experimentIdQuery.addBindValue(dayIndex);
    if(!experimentIdQuery.exec()) {
        qWarning() << "DBWorker::getComment - ERROR: " << experimentIdQuery.lastError().text();
        return comment;
    }
    if(experimentIdQuery.first()) {
        experimentId = experimentIdQuery.value(0).toInt();
    }
    QSqlQuery query;
    query.prepare("SELECT comment FROM comments WHERE experimentId = ?");
    query.addBindValue(experimentId);
    if(!query.exec()) {
        return comment;
    }
    if(query.first()) {
        comment = query.value(0).toString();
    }
    return comment;
}

void DBWorker::setComment(int experimentId, QString comment)
{
    QSqlQuery query;
    query.prepare("INSERT INTO comments(experimentId, comment) VALUES(?, ?)");
    query.addBindValue(experimentId);
    query.addBindValue(comment);
    if(!query.exec()) {
        qWarning() << "DBWorker::setComment - ERROR: " << query.lastError().text();
    }
}

QStringList DBWorker::getMeasurementsByDate(QDate date)
{
    QStringList result;
    QSqlQuery query;
    QString dateString = date.toString(Qt::ISODate);
    query.prepare("SELECT dayIndex FROM measurementSettings WHERE date = ?");
    query.addBindValue(dateString);
    if(!query.exec()) {
        qWarning() << "DBWorker::getMeasurementsByDate - ERROR: " << query.lastError().text();
    }
    while(query.next()) {
        int dayIndex = query.value(0).toInt();
        result.append(QString::number(dayIndex));
    }
    return result;
}

void DBWorker::getSoftwareCorrectionFactor(int index, QString calibratedSerial, int chan, int sensorIndex, QString sensorSerial, double correctionFactor, double sensitivity, int weightingLimit, int filterLength, double adcPgaGain, QVariantList *result)
{
    QSqlQuery query;
    query.prepare("SELECT measurementSettings.date, measurementSettings.dayIndex, singleChanResults.softwareCorrectionFactor, sensorSettings.chan as cal, CASE referenceIndex WHEN 1 THEN 11 WHEN 2 THEN 12 ELSE referenceChan END as ref FROM measurementSettings, sensorSettings, singleChanResults WHERE measurementSettings._id = sensorSettings.experimentId AND sensorSettings.experimentId = singleChanResults.experimentId AND sensorSettings.chan <> ref AND measurementSettings.calibratedIndex = ? AND measurementSettings.calibratedSerial = ? AND sensorSettings.chan = ? AND sensorSettings.chanIndex = ? AND sensorSettings.serial = ? AND sensorSettings.correctionFactor = ? AND sensorSettings.sensitivity = ? AND sensorSettings.weightingLimit = ? AND sensorSettings.filterLength = ? AND sensorSettings.adcPgaGain = ?");
    query.addBindValue(index);
    query.addBindValue(calibratedSerial);
    query.addBindValue(chan);
    query.addBindValue(sensorIndex);
    query.addBindValue(sensorSerial);
    query.addBindValue(correctionFactor);
    query.addBindValue(sensitivity);
    query.addBindValue(weightingLimit);
    query.addBindValue(filterLength);
    query.addBindValue(adcPgaGain);
    if(!query.exec()) {
        qWarning() << "DBWorker::getMeasurementsByDate - ERROR: " << query.lastError().text();
    }
    while(query.next()) {
        QString date = query.value(0).toString();
        int dayIndex = query.value(1).toInt();
        double softwareCorrectionFactor = query.value(2).toDouble();
        QVariantMap pair;
        pair.insert("first", softwareCorrectionFactor);
        pair.insert("second", QString(tr("Result %1 from %2").arg(dayIndex).arg(date)));
        result->append(pair);
    }
}

void DBWorker::setIntuitionSerialNumber(QString serialNumber) {
    if(setConfigValue("intuitionSerialNumber", serialNumber) == 0) {
        emit szobteSerialNumberChanged();
    }
}

QString DBWorker::getImPort()
{
    return getConfigValue("imPort");
}

void DBWorker::setImPort(QString port)
{
    if(setConfigValue("imPort", port) == 0) {
        emit imPortChanged();
    }
}

QString DBWorker::getImComPort()
{
    return getConfigValue("imComPort");
}

void DBWorker::setImComPort(QString port)
{
    if(setConfigValue("imComPort", port) == 0) {
        emit imComPortChanged();
    }
}

bool DBWorker::szobteGetChannelState(int chan) {
    QString stringState = getConfigValue(QString("szobteChan%1State").arg(chan));
    if(stringState == "true") {
        return true;
    }
    return false;
}

void DBWorker::szobteSetChannelState(int chan, bool state) {
    QString stringState = state ? "true": "false";
    if(setConfigValue(QString("szobteChan%1State").arg(chan), stringState) == 0) {
        emit szobteChannelStateChanged(chan, state);
    }
}

void DBWorker::szobteRefreshEnabledChansModel() {
    m_szobteEnabledChansModel.clear();
    for(int i = 1; i <=10; i++) {
        if(szobteGetChannelState(i)) {
            m_szobteEnabledChansModel.append(QString::number(i));
        }
    }
    emit szobteEnabledChansModelChanged();
}

void DBWorker::setTenzoMIPAddress(QString ipAddress) {
    if(setConfigValue("tenzoMIPAddress", ipAddress) == 0) {
        emit tenzoMIPAddressChanged();
    }
}

void DBWorker::setTenzoMPort(int port) {
    QString stringPort = QString::number(port);
    if(setConfigValue("tenzoMPort", stringPort) == 0) {
        emit tenzoMPortChanged();
    }
}

void DBWorker::setTenzoMSerialNumber(QString serial) {
    if(setConfigValue("tenzoMSerialNumber", serial) == 0) {
        emit tenzoMSerialNumberChanged();
    }
}

void DBWorker::setTenzoMBigBagsWeight(int weight)
{
    QString stringWeight = QString::number(weight);
    if(setConfigValue("tenzoMBigBagsWeight", stringWeight) == 0) {
        emit tenzoMBigBagsWeightChanged();
    }
}

void DBWorker::setTenzoMState(bool state)
{
    QString stringState = state ? "true": "false";
    if(setConfigValue("tenzoMState", stringState) == 0) {
        emit tenzoMStateChanged(state);
    }
}

void DBWorker::setImListenOnly(bool listenOnly)
{
    QString stringListenOnly = listenOnly ? "true": "false";
    if(setConfigValue("imListenOnly", stringListenOnly) == 0) {
        emit imListenOnlyChanged(listenOnly);
    }
}

void DBWorker::setImSaveToLog(bool saveToLog)
{
    QString stringSaveToLog = saveToLog ? "true": "false";
    if(setConfigValue("imSaveToLog", stringSaveToLog) == 0) {
        emit imSaveToLogChanged(saveToLog);
    }
}

void DBWorker::imRefreshEnabledChansModel()
{
    m_imEnabledChansModel.clear();
    for(int i = 1; i <=12; i++) {
        if(imGetChannelState(i)) {
            m_imEnabledChansModel.append(QString::number(i));
        }
    }
    emit imEnabledChansModelChanged();
}

void DBWorker::onSzobteChannelStateChanged(int chan, bool state) {
    Q_UNUSED(chan);
    Q_UNUSED(state);
    szobteRefreshEnabledChansModel();
}

void DBWorker::onImChannelStateChanged(int chan, bool state)
{
    Q_UNUSED(chan);
    Q_UNUSED(state);
    imRefreshEnabledChansModel();
}

bool DBWorker::intuitionGetState() {
    QString stringState = getConfigValue("intuitionState");
    if(stringState == "true") {
        return true;
    }
    return false;
}

void DBWorker::intuitionSetState(bool state) {
    QString stringState = state ? "true": "false";
    if(setConfigValue("intuitionState", stringState) == 0) {
        emit intuitionStateChanged(state);
    }
}

qreal DBWorker::szobteGetChannelCorrectionFactor(int chan) {
    QString stringCorFactor = getConfigValue(QString("szobteChan%1CorFactor").arg(chan));
    return stringCorFactor.toDouble();
}

void DBWorker::szobteSetChannelCorrectionFactor(int chan, qreal sensitivity) {
    QString stringCorFactor = QString::number(sensitivity, 'f', 6);
    if(setConfigValue(QString("szobteChan%1CorFactor").arg(chan), stringCorFactor) == 0) {
        emit szobteChannelCorrectionFactorChanged(chan, sensitivity);
    }
}

qreal DBWorker::szobteGetChannelSoftwareCorrectionFactor(int chan) {
    QString stringCorFactor = getConfigValue(QString("szobteChan%1SwCorFactor").arg(chan));
    return stringCorFactor.toDouble();
}

void DBWorker::szobteSetChannelSoftwareCorrectionFactor(int chan, qreal sensitivity) {
    QString stringCorFactor = QString::number(sensitivity, 'f', 6);
    if(setConfigValue(QString("szobteChan%1SwCorFactor").arg(chan), stringCorFactor) == 0) {
        emit szobteChannelSoftwareCorrectionFactorChanged(chan, sensitivity);
    }
}

qreal DBWorker::szobteGetChannelSensitivity(int chan) {
    QString stringSensitivity = getConfigValue(QString("szobteChan%1Sensitivity").arg(chan));
    return stringSensitivity.toDouble();
}

void DBWorker::szobteSetChannelSensitivity(int chan, qreal sensitivity) {
    QString stringSensitivity = QString::number(sensitivity, 'f', 6);
    if(setConfigValue(QString("szobteChan%1Sensitivity").arg(chan), stringSensitivity) == 0) {
        emit szobteChannelSensitivityChanged(chan, sensitivity);
    }
}

int DBWorker::szobteGetChannelWeightingLimit(int chan) {
    QString stringLimit = getConfigValue(QString("szobteChan%1WeightingLimit").arg(chan));
    return stringLimit.toInt();
}

void DBWorker::szobteSetChannelWeightingLimit(int chan, int limit) {
    QString stringLimit = QString::number(limit);
    if(setConfigValue(QString("szobteChan%1WeightingLimit").arg(chan), stringLimit) == 0) {
        emit szobteChannelWeightingLimitChanged(chan, limit);
    }
}

int DBWorker::szobteGetChannelFilterLength(int chan) {
    QString stringLength = getConfigValue(QString("szobteChan%1FilterLength").arg(chan));
    return stringLength.toInt();
}

void DBWorker::szobteSetChannelFilterLength(int chan, int length) {
    QString stringLength = QString::number(length);
    if(setConfigValue(QString("szobteChan%1FilterLength").arg(chan), stringLength) == 0) {
        emit szobteChannelFilterLengthChanged(chan, length);
    }
}

QString DBWorker::szobteGetSensorSerialNumber(int chan) {
    return getConfigValue(QString("szobteChan%1SensorSerialNumber").arg(chan));
}

void DBWorker::szobteSetSensorSerialNumber(int chan, QString serialNumber) {
    if(setConfigValue(QString("szobteChan%1SensorSerialNumber").arg(chan), serialNumber) == 0) {
        emit szobteSensorSerialNumberChanged(chan, serialNumber);
    }
}

int DBWorker::szobteGetSensorModelIndex(int chan) {
    QString stringIndex = getConfigValue(QString("szobteChan%1SensorModelIndex").arg(chan));
    return stringIndex.toInt();
}

void DBWorker::szobteSetSensorModelIndex(int chan, int index) {
    QString stringIndex = QString::number(index);
    if(setConfigValue(QString("szobteChan%1SensorModelIndex").arg(chan), stringIndex) == 0) {
        emit szobteSensorModelIndexChanged(chan, index);
    }
}

int DBWorker::intuitionGetSensorModelIndex() {
    QString stringIndex = getConfigValue("szobteSensorModelIndex");
    return stringIndex.toInt();
}

void DBWorker::intuitionSetSensorModelIndex(int index) {
    QString stringIndex = QString::number(index);
    if(setConfigValue("szobteSensorModelIndex", stringIndex) == 0) {
        emit intuitionSensorModelIndexChanged(index);
    }
}

QString DBWorker::intuitionGetSensorSerialNumber() {
    return getConfigValue("szobteSensorSerialNumber");
}

void DBWorker::intuitionSetSensorSerialNumber(QString serialNumber) {
    if(setConfigValue("szobteSensorSerialNumber", serialNumber) == 0) {
        emit intuitionSensorSerialNumberChanged(serialNumber);
    }
}

QString DBWorker::imGetSerialNumber(int amp)
{
    QString serial = getConfigValue(QString("imAmp%1SerialNumber").arg(amp));
    return serial;
}

void DBWorker::imSetSerialNumber(int amp, QString serial)
{
    if(setConfigValue(QString("imAmp%1SerialNumber").arg(amp), serial) == 0) {
        emit imSerialNumberChanged(amp, serial);
    }
}

int DBWorker::imGetCanId(int amp)
{
    QString canId = getConfigValue(QString("imAmp%1CanId").arg(amp));
    return canId.toInt();
}

void DBWorker::imSetCanId(int amp, int id)
{
    QString stringId = QString::number(id);
    if(setConfigValue(QString("imAmp%1CanId").arg(amp), stringId) == 0) {
        emit imCanIdChanged(amp, id);
    }
}

int DBWorker::imGetPgaGain(int amp)
{
    QString pgaGain = getConfigValue(QString("imAmp%1PgaGain").arg(amp));
    return pgaGain.toInt();
}

void DBWorker::imSetPgaGain(int amp, int pgaGain)
{
    QString stringPgaGain = QString::number(pgaGain);
    if(setConfigValue(QString("imAmp%1PgaGain").arg(amp), stringPgaGain) == 0) {
        emit imPgaGainChanged(amp, pgaGain);
    }
}

int DBWorker::imGetFilterLength(int chan)
{
    QString stringFilterLength = getConfigValue(QString("imChan%1FilterLength").arg(chan));
    return stringFilterLength.toInt();
}

void DBWorker::imSetFilterLength(int chan, int value)
{
    QString stringFilterLength = QString::number(value);
    if(setConfigValue(QString("imChan%1FilterLength").arg(chan), stringFilterLength) == 0) {
        emit imFilterLengthChanged(chan, value);
    }
}

qreal DBWorker::imGetChannelSensitivity(int chan) {
    QString stringSensitivity = getConfigValue(QString("imChan%1Sensitivity").arg(chan));
    return stringSensitivity.toDouble();
}

void DBWorker::imSetChannelSensitivity(int chan, qreal sensitivity) {
    QString stringSensitivity = QString::number(sensitivity, 'f', 6);
    if(setConfigValue(QString("imChan%1Sensitivity").arg(chan), stringSensitivity) == 0) {
        emit imChannelSensitivityChanged(chan, sensitivity);
    }
}

int DBWorker::imGetChannelWeightingLimit(int chan) {
    QString stringLimit = getConfigValue(QString("imChan%1WeightingLimit").arg(chan));
    return stringLimit.toInt();
}

void DBWorker::imSetChannelWeightingLimit(int chan, int limit) {
    QString stringLimit = QString::number(limit);
    if(setConfigValue(QString("imChan%1WeightingLimit").arg(chan), stringLimit) == 0) {
        emit imChannelWeightingLimitChanged(chan, limit);
    }
}

bool DBWorker::imGetChannelState(int chan) {
    QString stringState = getConfigValue(QString("imChan%1State").arg(chan));
    if(stringState == "true") {
        return true;
    }
    return false;
}

void DBWorker::imSetChannelState(int chan, bool state) {
    QString stringState = state ? "true": "false";
    if(setConfigValue(QString("imChan%1State").arg(chan), stringState) == 0) {
        emit imChannelStateChanged(chan, state);
    }
}

qreal DBWorker::imGetChannelSoftwareCorrectionFactor(int chan) {
    QString stringCorFactor = getConfigValue(QString("imChan%1SwCorFactor").arg(chan));
    return stringCorFactor.toDouble();
}

void DBWorker::imSetChannelSoftwareCorrectionFactor(int chan, qreal sensitivity) {
    QString stringCorFactor = QString::number(sensitivity, 'f', 6);
    if(setConfigValue(QString("imChan%1SwCorFactor").arg(chan), stringCorFactor) == 0) {
        emit imChannelSoftwareCorrectionFactorChanged(chan, sensitivity);
    }
}

QString DBWorker::imGetSensorSerialNumber(int chan) {
    return getConfigValue(QString("imChan%1SensorSerialNumber").arg(chan));
}

void DBWorker::imSetSensorSerialNumber(int chan, QString serialNumber) {
    if(setConfigValue(QString("imChan%1SensorSerialNumber").arg(chan), serialNumber) == 0) {
        emit imSensorSerialNumberChanged(chan, serialNumber);
    }
}

int DBWorker::imGetSensorModelIndex(int chan) {
    QString stringIndex = getConfigValue(QString("imChan%1SensorModelIndex").arg(chan));
    return stringIndex.toInt();
}

void DBWorker::imSetSensorModelIndex(int chan, int index) {
    QString stringIndex = QString::number(index);
    if(setConfigValue(QString("imChan%1SensorModelIndex").arg(chan), stringIndex) == 0) {
        emit imSensorModelIndexChanged(chan, index);
    }
}

QList<int> DBWorker::szobteGetEnabledChans() {
    QList<int> enabledChans;
    for(int i = 1; i <=10; i++) {
        if(szobteGetChannelState(i)) {
            enabledChans.append(i);
        }
    }
    return enabledChans;
}

QList<int> DBWorker::imGetEnabledChans() {
    QList<int> enabledChans;
    for(int i = 1; i <=12; i++) {
        if(imGetChannelState(i)) {
            enabledChans.append(i);
        }
    }
    return enabledChans;
}

int DBWorker::getCalibratedIndex() {
    QString stringIndex = getConfigValue("calibratedIndex");
    return stringIndex.toInt();
}

void DBWorker::setCalibratedIndex(int index) {
    QString stringIndex = QString::number(index);
    if(setConfigValue("calibratedIndex", stringIndex) == 0) {
        emit calibratedIndexChanged(index);
    }
}

int DBWorker::getReferenceIndex() {
    QString stringIndex = getConfigValue("referenceIndex");
    return stringIndex.toInt();
}

void DBWorker::setReferenceIndex(int index) {
    QString stringIndex = QString::number(index);
    if(setConfigValue("referenceIndex", stringIndex) == 0) {
        emit referenceIndexChanged(index);
    }
}

int DBWorker::getReferenceChannel() {
    QString stringChan = getConfigValue("referenceChannel");
    return stringChan.toInt();
}

void DBWorker::setReferenceChannel(int ch) {
    QString stringChan = QString::number(ch);
    if(setConfigValue("referenceChannel", stringChan) == 0) {
        emit referenceChanChanged(ch);
    }
}
