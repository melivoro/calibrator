#ifndef J1939_H
#define J1939_H

#include <stdint.h>

#define ADR_IM1   (129)

/*----------------------------------------------------------------------------*/
/* J1939 definitions */
/*----------------------------------------------------------------------------*/
// Default Priorities
typedef enum
{
  PRIORITY_CONTROL     = 3,
  PRIORITY_INFO        = 6,
  PRIORITY_PROPRIETARY = 6,
  PRIORITY_REQUEST     = 6,
  PRIORITY_ACK         = 6,
  PRIORITY_TP_CM       = 7,
  PRIORITY_TP_DT       = 7
} J1939_PRIORITY_Type;

// Defined Addresses
typedef enum
{
  ADDRESS_MODUL_IM2 = 128,
  ADDRESS_NULL      = 254,
  ADDRESS_GLOBAL    = 255
} J1939_ADDRESS_Type;

// Some Parameter Group Number
typedef enum
{
  PGN_ACKNOWLEDGMENT     = 0x00E800,  // slave  -> master
  PGN_REQUEST            = 0x00EA00,  // master -> slave
  PGN_PROPRIETARY_B      = 0x00FF00,
  PGN_START_MEASURING    = 0x00FF10,  // master -> slave
  PGN_STOP_MEASURING     = 0x00FF11,  // master -> slave
  PGN_DATA_ADC_ACC       = 0x00FF12,  // slave  -> master
  PGN_DATA_ADC           = 0x00FF13,  // slave  -> master
  PGN_DATA_ACC           = 0x00FF14,  // slave  -> master
  PGN_STATUS_HEARTBEAT   = 0x00FF15,  // slave  -> master
  PGN_FIRMWARE_VERSION   = 0x00FF16,  // slave  -> master
  PGN_ERROR_DETAILS      = 0x00FF17,  // slave  -> master
  PGN_MASTER_SUSPEND     = 0x00FF18,  // PK     -> master, slave
  PGN_MASTER_RUN         = 0x00FF19,  // PK     -> master, slave
  PGN_SENSOR_0_PARAM     = 0x00FF1A,  // slave  -> master
  PGN_SENSOR_1_PARAM     = 0x00FF1B,  // slave  -> master
  PGN_IM_PARAM           = 0x00FF1C,  // slave  -> master
  PGN_DATA_WEIGHT        = 0x00FF1D,  // PK     -> master
  PGN_INIT_IM            = 0x00FF20,  // master -> slave
  PGN_READ_EEPROM        = 0x00FF30,  // master -> slave
  PGN_WRITE_EEPROM       = 0x00FF31,  // master -> slave
  PGN_DATA_EEPROM        = 0x00FF32,  // slave  -> master
  PGN_GOTO_BOOTLOADER    = 0x00FF40,  // master -> slave
  PGN_GOTO_FIRMWARE      = 0x00FF41,  // master -> slave
  PGN_ERASE_FLASH        = 0x00FF42,  // master -> slave
  PGN_READ_FLASH         = 0x00FF43,  // master -> slave
  PGN_WRITE1_FLASH       = 0x00FF44,  // master -> slave
  PGN_WRITE2_FLASH       = 0x00FF45,  // master -> slave
  PGN_WRITE3_FLASH       = 0x00FF46,  // master -> slave
  PGN_DATA_FLASH         = 0x00FF47,  // slave  -> master
  PGN_BOOTLOADER_VERSION = 0x00FF48   // slave  -> master
} J1939_PGN_Type;

// CAN ExtId - J1939 macro
#define J1939_SA(_sa_)     (_sa_  & 0xFF)
#define J1939_PS(_ps_)    ((_ps_  & 0xFF) <<  8)
#define J1939_DA(_da_)    ((_da_  & 0xFF) <<  8)
#define J1939_PF(_pf_)    ((_pf_  & 0xFF) << 16)
#define J1939_DP(_dp_)    ((_dp_  & 0x01) << 24)
#define J1939_EDP(_edp_)  ((_edp_ & 0x01) << 25)
#define J1939_P(_p_)      ((_p_   & 0x07) << 26)

#define J1939_PGN(_pgn_)  ((_pgn_ & 0xFFFF) << 8)

#define J1939_SA_MASK   (0xFF)
#define J1939_PS_MASK   (0xFF <<  8)
#define J1939_DA_MASK   (0xFF <<  8)
#define J1939_PF_MASK   (0xFF << 16)
#define J1939_DP_MASK   (0x01 << 24)
#define J1939_EDP_MASK  (0x01 << 25)
#define J1939_P_MASK    (0x07 << 26)

#define J1939_GET_SA(_extid_)   ( ((uint32_t)_extid_)        & 0xFF)
#define J1939_GET_PS(_extid_)   ((((uint32_t)_extid_) >>  8) & 0xFF)
#define J1939_GET_DA(_extid_)   ((((uint32_t)_extid_) >>  8) & 0xFF)
#define J1939_GET_PF(_extid_)   ((((uint32_t)_extid_) >> 16) & 0xFF)
#define J1939_GET_DP(_extid_)   ((((uint32_t)_extid_) >> 24) & 0x01)
#define J1939_GET_EDP(_extid_)  ((((uint32_t)_extid_) >> 25) & 0x01)
#define J1939_GET_P(_extid_)    ((((uint32_t)_extid_) >> 26) & 0x07)

#define J1939_SET_SA(_extid_, _sa_)    _extid_ = ((((uint32_t)_extid_) & 0xFFFFFF00) |  (_sa_  & 0xFF))
#define J1939_SET_PS(_extid_, _ps_)    _extid_ = ((((uint32_t)_extid_) & 0xFFFF00FF) | ((_ps_  & 0xFF) <<  8))
#define J1939_SET_DA(_extid_, _da_)    _extid_ = ((((uint32_t)_extid_) & 0xFFFF00FF) | ((_da_  & 0xFF) <<  8))
#define J1939_SET_PF(_extid_, _pf_)    _extid_ = ((((uint32_t)_extid_) & 0xFF00FFFF) | ((_pf_  & 0xFF) << 16))
#define J1939_SET_DP(_extid_, _dp_)    _extid_ = ((((uint32_t)_extid_) & 0xFEFFFFFF) | ((_dp_  & 0x01) << 24))
#define J1939_SET_EDP(_extid_, _edp_)  _extid_ = ((((uint32_t)_extid_) & 0xFDFFFFFF) | ((_edp_ & 0x01) << 25))
#define J1939_SET_P(_extid_, _p_)      _extid_ = ((((uint32_t)_extid_) & 0xE3FFFFFF) | ((_p_   & 0x07) << 26))

// PGN_ACKNOWLEDGMENT Control Bytes
#define J1939_ACK             0
#define J1939_NACK            1
#define J1939_ACCESS_DENIED   2
#define J1939_CANNOT_RESPOND  3

/*----------------------------------------------------------------------------*/
/* Function prototypes */
/*----------------------------------------------------------------------------*/
uint32_t j1939_get_pgn(uint32_t ExtId);
uint32_t j1939_set_ExtId(J1939_PRIORITY_Type priority, J1939_PGN_Type pgn, uint8_t da, uint8_t sa);

void can_send_request(int canHandle, J1939_PGN_Type pf, uint8_t da, uint8_t sa);
char *Acknowledgment2char(uint8_t ack);

#endif // J1939_H
