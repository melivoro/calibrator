#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

#include <vector>
#include <QDebug>

class CircularBuffer
{
public:
    CircularBuffer();
    void Add(double);
    double GetAverage(int);
private:
    std::vector<double> m_buf;
    int i;
};

#endif // CIRCULARBUFFER_H
