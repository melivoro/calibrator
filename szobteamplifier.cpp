#include "szobteamplifier.h"

SzobteAmplifier::SzobteAmplifier(QObject *parent): SerialPort(parent) {
    connect(this, &SzobteAmplifier::setActiveChans, this, &SzobteAmplifier::onSetActiveChans);
    setBaudRate(115200);
}

void SzobteAmplifier::onSetActiveChans(QList<int> chans) {
    m_activeChans.clear();
    for(QList<int>::iterator i = chans.begin(); i != chans.end(); i++) {
        m_activeChans[*i] = true;
    }
}

void SzobteAmplifier::checkData(QString line) {
    double data;
    QRegExp rx1("^([+-]\\d+\\.\\d+)\\,([+-]\\d+\\.\\d+)\\,([+-]\\d+\\.\\d+)\\,([+-]\\d+\\.\\d+)\\,"
               "([+-]\\d+\\.\\d+)\\,([+-]\\d+\\.\\d+)\\,([+-]\\d+\\.\\d+)\\,([+-]\\d+\\.\\d+)\\,"
               "([+-]\\d+\\.\\d+)\\,([+-]\\d+\\.\\d+)$");
    int pos = rx1.indexIn(line);
    if(pos > -1) {
        for(int ch = 1; ch <= 10; ch++) {
            bool ok;
            data = rx1.capturedTexts()[ch].toDouble(&ok);
            if(ok) {
                if(m_activeChans[ch]) {
                    emit newData(QDateTime::currentDateTime().toMSecsSinceEpoch(), ch, data * 1000);
                    average[ch-1].Add(data * 1000);
                }
            }
        }
    }
}

double SzobteAmplifier::getAverage(int ch) {
    return average[ch-1].GetAverage(10);
}
