import QtQuick 2.15
import QtQuick.Controls 2.15
import QtCharts 2.15

ChartView {
    id: resultsChartView
    ValueAxis {
        id: xAxis
        min: 0
        max: 100
    }
    ValueAxis {
        id: yAxis
        min: 0
        max: 100
    }
    ScatterSeries {
        id: measuredUpScatterSeries
        visible: true
        name: qsTr("Measured up points")
        axisX: xAxis
        axisY: yAxis
        useOpenGL: true
    }
    ScatterSeries {
        id: measuredDownScatterSeries
        visible: true
        name: qsTr("Measured down points")
        axisX: xAxis
        axisY: yAxis
        useOpenGL: true
    }
    LineSeries {
        id: interpolatedLineSeries
        visible: true
        name: qsTr("Interpolated line")
        axisX: xAxis
        axisY: yAxis
        useOpenGL: true
    }
    LineSeries {
        id: interpolatedUpLineSeries
        visible: true
        name: qsTr("Interpolated up line")
        axisX: xAxis
        axisY: yAxis
        useOpenGL: true
    }
    LineSeries {
        id: interpolatedDownLineSeries
        visible: true
        name: qsTr("Interpolated down line")
        axisX: xAxis
        axisY: yAxis
        useOpenGL: true
    }
    LineSeries {
        id: errorLineSeries
        visible: true
        name: qsTr("Error value")
        axisX: xAxis
        axisY: yAxis
        useOpenGL: true
    }
    MouseArea {
        property int prevX
        property int prevY
        anchors.fill: parent
        hoverEnabled: true
        onPressed: {
            prevX = mouseX
            prevY = mouseY
        }
        onMouseXChanged: {
            if (containsPress) {
                resultsChartView.scrollLeft(mouseX - prevX)
                prevX = mouseX
            }
        }
        onMouseYChanged: {
            if (containsPress) {
                resultsChartView.scrollUp(mouseY - prevY)
                prevY = mouseY
            }
        }
        onWheel: {
            if (wheel.angleDelta.y > 0) {
                resultsChartView.zoom(1.1)
            } else {
                resultsChartView.zoom(0.9)
            }
        }
        onDoubleClicked: {
            xAxis.min = 0
            xAxis.max = 100
            yAxis.min = 0
            yAxis.max = 100
            resultsChartView.zoomReset()
        }
    }
    Column {
        anchors {
            bottom: parent.bottom
            right: parent.right
            bottomMargin: 70
            rightMargin: 80
        }
        Button {
            implicitHeight: 40
            implicitWidth: 35
            font.pointSize: 20
            text: "+"
            onClicked: {
                resultsChartView.zoom(1.1)
            }
        }
        Button {
            implicitHeight: 40
            implicitWidth: 35
            font.pointSize: 20
            text: "-"
            onClicked: {
                resultsChartView.zoom(0.9)
            }
        }
    }
    Connections {
        target: resultsModel
        function onNewDataProcessed(a, b, aUp, bUp, aDown, bDown) {
            measuredUpScatterSeries.clear()
            for(var i = 0; i < resultsModel.getDataUpSize(); i++) {
                measuredUpScatterSeries.append(resultsModel.getPointX(0, i), resultsModel.getPointY(0, i))
            }
            measuredDownScatterSeries.clear()
            for(i = 0; i < resultsModel.getDataDownSize(); i++) {
                measuredDownScatterSeries.append(resultsModel.getPointX(1, i), resultsModel.getPointY(1, i))
            }
            errorLineSeries.clear()
            for(i = 0; i < resultsModel.getErrorSize(); i++) {
                errorLineSeries.append(resultsModel.getPointX(2, i), resultsModel.getPointY(2, i))
            }
            interpolatedLineSeries.clear()
            interpolatedUpLineSeries.clear()
            interpolatedDownLineSeries.clear()
            interpolatedLineSeries.append(resultsModel.getMinX(), a * resultsModel.getMinX() + b)
            interpolatedLineSeries.append(resultsModel.getMaxX(), a * resultsModel.getMaxX() + b)
            interpolatedUpLineSeries.append(resultsModel.getMinX(), aUp * resultsModel.getMinX() + bUp)
            interpolatedUpLineSeries.append(resultsModel.getMaxX(), aUp * resultsModel.getMaxX() + bUp)
            interpolatedDownLineSeries.append(resultsModel.getMinX(), aDown * resultsModel.getMinX() + bDown)
            interpolatedDownLineSeries.append(resultsModel.getMaxX(), aDown * resultsModel.getMaxX() + bDown)
        }
    }
}
