#include "serialport.h"

SerialPort::SerialPort(QObject *parent): QSerialPort(parent) {
    connect(this, &SerialPort::readyRead, this, &SerialPort::onReadyRead);
}

void SerialPort::setPort(QString port) {
    if(isOpen()) {
        close();
    }
    setPortName(port);
    const bool result = open(QIODevice::ReadWrite);
    m_port = result ? port: "";
    emit portChanged(m_port);
}

void SerialPort::onReadyRead()
{
    while(canReadLine()) {
        QString line = readLine().trimmed();
        checkData(line);
    }
}
