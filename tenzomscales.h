#ifndef TENZOMSCALES_H
#define TENZOMSCALES_H

#include <QTcpSocket>
#include <QHostAddress>
#include <QTimer>
#include <QDebug>
#include "circularbuffer.h"

class TenzoMScales: public QTcpSocket
{
    Q_OBJECT
public:
    TenzoMScales();
    bool checkTenzoMCrc(QByteArray);
    QByteArray createCmdBuffer(unsigned char, unsigned char);
    QByteArray createCmdBuffer(unsigned char, unsigned char, QByteArray);
    double getAverage();
    bool running();

public slots:
    void onIpAddressChanged(QString);
    void onPortChanged(int);

private slots:
    void onStateChanged(QAbstractSocket::SocketState);
    virtual void onReadyRead();

private:
    unsigned char tenzoMCrc(unsigned char*, unsigned char, unsigned char);
    void requestData();
    void verifyAndConnectToHost();
    QString m_ipAddr;
    int m_port;
    QTimer m_timer;
    bool m_running;
    bool m_goodIP;
    CircularBuffer average;
};

#endif // TENZOMSCALES_H
