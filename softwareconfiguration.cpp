#include "softwareconfiguration.h"
#include "mathfunctions.h"

ResultsModel::ResultsModel() {
    connect(this, &ResultsModel::newData, this, &ResultsModel::onNewData);
    connect(this, &ResultsModel::clear, this, &ResultsModel::onClear);
}

void ResultsModel::onClear() {
    beginResetModel();
    m_data.clear();
    endResetModel();
}

QList<QList<double> > ResultsModel::getData() const
{
    return m_data;
}

double ResultsModel::getMaxX() const
{
    return m_maxX;
}

double ResultsModel::getMinX() const
{
    return m_minX;
}

int ResultsModel::rowCount(const QModelIndex&) const {
    return m_data.count();
}

int ResultsModel::columnCount(const QModelIndex &) const {
    return m_calibratedHeaders.size() + 2;
}

QVariant ResultsModel::data(const QModelIndex &index, int role) const {
    switch (role) {
        case Qt::DisplayRole:
            if(index.column() < m_calibratedHeaders.size()+1) {
                return QString::number(m_data[index.row()][index.column()]);
            } else {
                double sum = 0;
                for(int i = 1; i <= m_calibratedHeaders.size(); i++) {
                    sum += m_data[index.row()][i];
                }
                return QString::number(sum);
            }

        default:
            break;
    }
    return QVariant();
}

bool ResultsModel::setData(const QModelIndex &index, const QVariant& data, int role) {
    switch (role) {
        case Qt::EditRole:
        beginResetModel();
        m_data[index.row()][index.column()] = data.toDouble();
        endResetModel();
            emit newData();
        return true;
        default:
            break;
    }
    return false;
}

QHash<int, QByteArray> ResultsModel::roleNames() const {
    return { {Qt::DisplayRole, "display"} };
}

void ResultsModel::setCalibratedList(QList<bool> list) {
    beginResetModel();
    m_calibratedHeaders.clear();
    for(int i = 0; i < list.size(); i++) {
        bool status = list[i];
        if(status) {
            if (i < 10) {
                m_calibratedHeaders.append(QString(tr("SZOBTE Ch %1")).arg(i+1));
            } else {
                m_calibratedHeaders.append(QString(tr("IM Ch %1")).arg(i-9));
            }
        }
    }
    m_calibratedChannelSet = list;
    endResetModel();
    emit headersChanged();
}

void ResultsModel::setReferenceChannel(int ch) {
    beginResetModel();
    if(ch <= 10) {
        m_referenceHeader = QString(tr("SZOBTE Ch %1*")).arg(ch);
    }
    if(ch == 11) {
        m_referenceHeader = tr("Intuition*");
    }
    if(ch == 24) {
        m_referenceHeader = tr("Tenzo-M*");
    }
    m_referenceChan = ch;
    endResetModel();
    emit headersChanged();
}

QStringList ResultsModel::getHeaders() {
    QStringList result;
    result.append(m_referenceHeader);
    result.append(m_calibratedHeaders);
    result.append(QString(tr("Sum")));
    return result;
}

Q_INVOKABLE void ResultsModel::append(QList<double> list) {
    beginResetModel();
    QList<double> line;
    double referenceValue = 0;
    if(m_referenceChan <= 10) {
        referenceValue = list.at(m_referenceChan-1);
    } else {
        if(m_referenceChan == 11) {
            referenceValue = list.at(10);
        } else {
            if(m_referenceChan == 24) {
                referenceValue = list.at(23);
            }
        }
    }
    line.append(referenceValue);
    for(int i = 0; i < list.size(); i++) {
        if(i < 10) {
            if(m_calibratedChannelSet[i]) {
                line.append(list.at(i));
            }
        }
        if((i > 10) && (i < 23)) {
            if(m_calibratedChannelSet[i-1]) {
                line.append(list.at(i));
            }
        }
    }
    m_data.append(line);
    endResetModel();
    emit newData();
}

int ResultsModel::getDataUpSize() {
    return m_dataUp.size();
}

int ResultsModel::getDataDownSize() {
    return m_dataDown.size();
}

int ResultsModel::getErrorSize() {
    return m_error.size();
}

double ResultsModel::getPointX(int array, int index) {
    switch(array) {
    case 0:
        return m_dataUp[index].first;
    case 1:
        return m_dataDown[index].first;
    case 2:
        return m_error[index].first;
    }
    return 0;
}

double ResultsModel::getPointY(int array, int index) {
    switch(array) {
    case 0:
        return m_dataUp[index].second;
    case 1:
        return m_dataDown[index].second;
    case 2:
        return m_error[index].second;
    }
    return 0;
}

void ResultsModel::onNewData() {
    QList<QPair<double, double>> data;
    double prevReferenceForce;
    m_dataUp.clear();
    m_dataDown.clear();
    m_error.clear();
    m_minX = 0;
    m_maxX = 0;
    for(int i = 0; i < m_data.count(); i++) {
        double referenceForce = getReference(i);
        if(m_minX > referenceForce) {
            m_minX = referenceForce;
        }
        if(m_maxX < referenceForce) {
            m_maxX = referenceForce;
        }
        double calibratedForceSum = getCalibrated(i);
        bool up = true;
        if(i == 0) {
            up = true;
        } else {
            if(referenceForce <= prevReferenceForce) {
                up = false;
            }
        }
        prevReferenceForce = referenceForce;
        QPair<double, double> pair;
        pair.first = referenceForce;
        pair.second = calibratedForceSum;
        data.append(pair);
        if(up) {
            m_dataUp.append(pair);
        } else {
            m_dataDown.append(pair);
        }
        m_error.append(QPair<double, double>(pair.first, pair.second - pair.first));
    }
    double a, b, aUp, bUp, aDown, bDown, errorMax, aq, bq, cq, xminq, xmaxq;
    approximate(m_dataUp, &aUp, &bUp, &errorMax);
    approximate(m_dataDown, &aDown, &bDown, &errorMax);
    //approximate(data, &a, &b, &errorMax);
    a = (aUp + aDown) / 2; // считаем коэффициенты как среднее арифметическое
    b = (bUp + bDown) / 2; // между ветвями нагрузки и разгрузки
    approximateq(data, &aq, &bq, &cq, &xminq, &xmaxq);
    double nonLinearity = calculateNonLinearity(aq, bq-a, cq-b, xminq, xmaxq);
    emit newDataProcessed(a, b, aUp, bUp, aDown, bDown, aq, bq, cq, nonLinearity);
}

double ResultsModel::getReference(int row) {
    return m_data[row][0];
}

double ResultsModel::getCalibrated(int row) {
    double sum = 0;
    for(int i = 1; i < m_data[row].count(); i++) {
        sum += m_data[row][i];
    }
    return sum;
}

double ResultsAndSettings::a() const
{
    return m_a;
}

void ResultsAndSettings::setA(double a)
{
    m_a = a;
    refresh();
}

double ResultsAndSettings::b() const
{
    return m_b;
}

void ResultsAndSettings::setB(double b)
{
    m_b = b;
    refresh();
}

double ResultsAndSettings::aUp() const
{
    return m_aUp;
}

void ResultsAndSettings::setAUp(double aUp)
{
    m_aUp = aUp;
    refresh();
}

double ResultsAndSettings::bUp() const
{
    return m_bUp;
}

void ResultsAndSettings::setBUp(double bUp)
{
    m_bUp = bUp;
    refresh();
}

double ResultsAndSettings::aDown() const
{
    return m_aDown;
}

void ResultsAndSettings::setADown(double aDown)
{
    m_aDown = aDown;
    refresh();
}

double ResultsAndSettings::bDown() const
{
    return m_bDown;
}

void ResultsAndSettings::setBDown(double bDown)
{
    m_bDown = bDown;
    refresh();
}

double ResultsAndSettings::aq() const
{
    return m_aq;
}

void ResultsAndSettings::setAq(double aq)
{
    m_aq = aq;
    refresh();
}

double ResultsAndSettings::bq() const
{
    return m_bq;
}

void ResultsAndSettings::setBq(double bq)
{
    m_bq = bq;
    refresh();
}

double ResultsAndSettings::cq() const
{
    return m_cq;
}

void ResultsAndSettings::setCq(double cq)
{
    m_cq = cq;
    refresh();
}

double ResultsAndSettings::nonLinearity() const
{
    return m_nonLinearity;
}

void ResultsAndSettings::setNonLinearity(double nonLinearity)
{
    m_nonLinearity = nonLinearity;
    refresh();
}

int ResultsAndSettings::pNom() const
{
    return m_pNom;
}

void ResultsAndSettings::setPNom(int pNom)
{
    m_pNom = pNom;
    refresh();
}

int ResultsAndSettings::percent() const
{
    return m_percent;
}

void ResultsAndSettings::setPercent(int percent)
{
    m_percent = percent;
    refresh();
}

double ResultsAndSettings::fUp() const
{
    return m_fUp;
}

double ResultsAndSettings::fDown() const
{
    return m_fDown;
}

double ResultsAndSettings::hyst() const
{
    return m_hyst;
}

double ResultsAndSettings::hystPercent() const
{
    return m_hystPercent;
}

double ResultsAndSettings::nonLinearityPercent() const
{
    return m_nonLinearityPercent;
}

SensorSettings ResultsAndSettings::referenceSensor() const
{
    return m_referenceSensor;
}

void ResultsAndSettings::setReferenceSensor(const SensorSettings &referenceSensor)
{
    m_referenceSensor = referenceSensor;
}

QList<SensorSettings> ResultsAndSettings::calibratedSensors() const
{
    return m_calibratedSensors;
}

void ResultsAndSettings::setCalibratedSensors(const QList<SensorSettings> &calibratedSensors)
{
    m_calibratedSensors = calibratedSensors;
}

int ResultsAndSettings::referenceIndex() const
{
    return m_referenceIndex;
}

void ResultsAndSettings::setReferenceIndex(int referenceIndex)
{
    m_referenceIndex = referenceIndex;
}

int ResultsAndSettings::calibratedIndex() const
{
    return m_calibratedIndex;
}

void ResultsAndSettings::setCalibratedIndex(int calibratedIndex)
{
    m_calibratedIndex = calibratedIndex;
}

QString ResultsAndSettings::referenceSerial() const
{
    return m_referenceSerial;
}

void ResultsAndSettings::setReferenceSerial(const QString &referenceSerial)
{
    m_referenceSerial = referenceSerial;
}

QString ResultsAndSettings::calibratedSerial() const
{
    return m_calibratedSerial;
}

void ResultsAndSettings::setCalibratedSerial(const QString &calibratedSerial)
{
    m_calibratedSerial = calibratedSerial;
}

void ResultsAndSettings::refresh()
{
    m_fUp = m_aUp * m_percent / 100 * m_pNom + m_bUp;
    m_fDown = m_aDown * m_percent / 100 * m_pNom + m_bDown;
    m_hyst = m_fUp - m_fDown;
    m_hystPercent = 100 * m_hyst / (double(m_percent) / 100 * m_pNom);
    m_nonLinearityPercent = 100 * m_nonLinearity / (double(m_percent) / 100 * m_pNom);
}

SoftwareConfiguration::SoftwareConfiguration(QObject *parent) : QObject(parent)
{
    m_blockedConfiguration = false;
    connect(&m_dbWorker, &DBWorker::szobteEnabledChansModelChanged, &m_hardwareManager, &HardwareManager::onSzobteNeedUpdateActiveChans);
    connect(&m_dbWorker, &DBWorker::imEnabledChansModelChanged, &m_hardwareManager, &HardwareManager::onImNeedUpdateActiveChans);
    connect(this, &SoftwareConfiguration::calibratedIndexChanged, this, &SoftwareConfiguration::onCalibratedAmplifierIndexChanged);
    connect(&m_dbWorker, &DBWorker::szobteEnabledChansModelChanged, this, &SoftwareConfiguration::onSzobteEnabledChansModelChanged);
    connect(&m_resultsModel, &ResultsModel::newDataProcessed, this, &SoftwareConfiguration::onCalculateResult);
    connect(this, &SoftwareConfiguration::blockedConfigurationChanged, this, &SoftwareConfiguration::onBlockedConfigurationChanged);
    connect(&m_dbWorker, &DBWorker::szobteChannelSoftwareCorrectionFactorChanged, &m_hardwareManager, &HardwareManager::onSzobteSoftwareCorrectionFactorChanged);
    connect(&m_dbWorker, &DBWorker::imChannelSoftwareCorrectionFactorChanged, &m_hardwareManager, &HardwareManager::onImSoftwareCorrectionFactorChanged);
    for(int i = 1; i <=12; i++) {
        m_hardwareManager.setImFilterLength(i, m_dbWorker.imGetFilterLength(i));
        m_hardwareManager.setImWeightingLimit(i, m_dbWorker.imGetChannelWeightingLimit(i));
        m_hardwareManager.setImSensitivity(i, m_dbWorker.imGetChannelSensitivity(i));
        if(i%2 == 0) {
            int amp = i/2;
            m_hardwareManager.setImCanId(amp, m_dbWorker.imGetCanId(amp));
            m_hardwareManager.setImPgaGain(amp, m_dbWorker.imGetPgaGain(amp)==0 ? 1 : m_dbWorker.imGetPgaGain(amp));
        }
    }
    connect(&m_dbWorker, &DBWorker::imFilterLengthChanged, &m_hardwareManager, &HardwareManager::onImFilterLengthChanged);
    connect(&m_dbWorker, &DBWorker::imChannelWeightingLimitChanged, &m_hardwareManager, &HardwareManager::onImWeightingLimitChanged);
    connect(&m_dbWorker, &DBWorker::imChannelSensitivityChanged, &m_hardwareManager, &HardwareManager::onImSensitivityChanged);
    connect(&m_dbWorker, &DBWorker::imCanIdChanged, &m_hardwareManager, &HardwareManager::onImCanIdChanged);
    connect(&m_dbWorker, &DBWorker::imPgaGainChanged, &m_hardwareManager, &HardwareManager::onImPgaGainChanged);
    connect(&m_dbWorker, &DBWorker::imListenOnlyChanged, &m_hardwareManager, &HardwareManager::onImListenOnlyChanged);
    connect(&m_dbWorker, &DBWorker::imSaveToLogChanged, &m_hardwareManager, &HardwareManager::onImSaveToLogChanged);
}

DBWorker* SoftwareConfiguration::getDbWorker() {
    return &m_dbWorker;
}

HardwareManager* SoftwareConfiguration::getHwManager() {
    return &m_hardwareManager;
}

ResultsModel* SoftwareConfiguration::getResultsModel() {
    return &m_resultsModel;
}

void SoftwareConfiguration::saveData(QString comment)
{
    QDate date = QDate::currentDate();
    int referenceIndex = m_resultsAndSettings.referenceIndex();
    QString referenceSerial = m_resultsAndSettings.referenceSerial();
    int calibratedIndex = m_resultsAndSettings.calibratedIndex();
    QString calibratedSerial = m_resultsAndSettings.calibratedSerial();
    int referenceChan = m_resultsAndSettings.referenceSensor().chan();
    int experimentId = 0;
    m_dbWorker.setMeasurementSettings(date, referenceIndex, referenceSerial, calibratedIndex, calibratedSerial, referenceChan, &experimentId);
    int absoluteChanNumber;
    switch (referenceIndex) {
    case 0:
        absoluteChanNumber = referenceChan;
        break;
    case 1:
        absoluteChanNumber = 11;
        break;
    case 2:
        absoluteChanNumber = 24;
        break;
    default:
        absoluteChanNumber = -1;
    }
    QList<int> chanNumbers;
    chanNumbers.append(absoluteChanNumber);
    int index = m_resultsAndSettings.referenceSensor().index();
    QString serial = m_resultsAndSettings.referenceSensor().serial();
    double correctionFactor = m_resultsAndSettings.referenceSensor().correctionFactor();
    double sensitivity = m_resultsAndSettings.referenceSensor().sensitivity();
    int weightingLimit = m_resultsAndSettings.referenceSensor().weightingLimit();
    int filterLength = m_resultsAndSettings.referenceSensor().filterLength();
    double adcPgaGain = m_resultsAndSettings.referenceSensor().adcPgaGain();
    double softwareCorrectionFactor = m_resultsAndSettings.referenceSensor().softwareCorrectionFactor();
    m_dbWorker.setSensorSettings(experimentId, absoluteChanNumber, index, serial, correctionFactor, sensitivity, weightingLimit, filterLength, adcPgaGain, softwareCorrectionFactor);
    foreach(SensorSettings sensor, m_resultsAndSettings.calibratedSensors()) {
        absoluteChanNumber = calibratedIndex == 0 ? sensor.chan(): sensor.chan() + 11;
        chanNumbers.append(absoluteChanNumber);
        index = sensor.index();
        serial = sensor.serial();
        correctionFactor = sensor.correctionFactor();
        sensitivity = sensor.sensitivity();
        weightingLimit = sensor.weightingLimit();
        filterLength = sensor.filterLength();
        adcPgaGain = sensor.adcPgaGain();
        softwareCorrectionFactor = sensor.softwareCorrectionFactor();
        m_dbWorker.setSensorSettings(experimentId, absoluteChanNumber, index, serial, correctionFactor, sensitivity, weightingLimit, filterLength, adcPgaGain, softwareCorrectionFactor);
    }
    for(int i = 0; i < m_resultsModel.getData().count(); i++) {
        QList<double> line = m_resultsModel.getData()[i];
        for(int j = 0; j < line.count(); j++) {
            m_dbWorker.setExperimentData(experimentId, i, chanNumbers[j], line[j]);
        }
    }
    if(m_resultsAndSettings.calibratedSensors().count() == 1) {
        m_dbWorker.setSingleChanResults(experimentId, m_resultsAndSettings.a());
    }
    if(!comment.isEmpty()) {
        m_dbWorker.setComment(experimentId, comment);
    }
}

void SoftwareConfiguration::setDataFromDb(QDate date, int dayIndex)
{
    int referenceIndex, calibratedIndex, referenceChan, experimentId;
    QString referenceSerial, calibratedSerial;
    m_dbWorker.getMeasurementSettings(date, dayIndex+1, &referenceIndex, &referenceSerial, &calibratedIndex, &calibratedSerial, &referenceChan, &experimentId);
    m_resultsAndSettings.setReferenceIndex(referenceIndex);
    m_resultsAndSettings.setReferenceSerial(referenceSerial);
    m_resultsAndSettings.setCalibratedIndex(calibratedIndex);
    m_resultsAndSettings.setCalibratedSerial(calibratedSerial);
    SensorSettings referenceSensor;
    int relativeChanNumber = referenceChan > 10 ? 1: referenceChan;
    if(referenceIndex == 1) {
        referenceChan = 11;
    }
    if(referenceIndex == 2) {
        referenceChan = 24;
    }
    referenceSensor.setChan(relativeChanNumber);
    QList<SensorSettings> sensors = m_dbWorker.getSensorSettingsList(experimentId);
    QList<SensorSettings> calibratedSensors;
    bool first = true;
    double minimumWeightingLimit = 0;
    QList<bool> calibratedList = {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
    foreach(SensorSettings sensor, sensors) {
        if(sensor.chan() == referenceChan) {
            referenceSensor.setIndex(sensor.index());
            referenceSensor.setSerial(sensor.serial());
            referenceSensor.setCorrectionFactor(sensor.correctionFactor());
            referenceSensor.setSensitivity(sensor.sensitivity());
            referenceSensor.setWeightingLimit(sensor.weightingLimit());
            referenceSensor.setFilterLength(sensor.filterLength());
            referenceSensor.setAdcPgaGain(sensor.adcPgaGain());
            referenceSensor.setSoftwareCorrectionFactor(sensor.softwareCorrectionFactor());
        } else {
            SensorSettings calibratedSensor;
            if(first) {
                minimumWeightingLimit = sensor.weightingLimit();
                first = false;
            } else {
                if(minimumWeightingLimit > sensor.weightingLimit()) {
                    minimumWeightingLimit = sensor.weightingLimit();
                }
            }
            calibratedList[sensor.chan() - 2] = true;
            calibratedSensor.setChan(sensor.chan());
            calibratedSensor.setIndex(sensor.index());
            calibratedSensor.setSerial(sensor.serial());
            calibratedSensor.setCorrectionFactor(sensor.correctionFactor());
            calibratedSensor.setSensitivity(sensor.sensitivity());
            calibratedSensor.setWeightingLimit(sensor.weightingLimit());
            calibratedSensor.setFilterLength(sensor.filterLength());
            calibratedSensor.setAdcPgaGain(sensor.adcPgaGain());
            calibratedSensor.setSoftwareCorrectionFactor(sensor.softwareCorrectionFactor());
            calibratedSensors.append(calibratedSensor);
        }
    }
    m_resultsAndSettings.setReferenceSensor(referenceSensor);
    m_resultsAndSettings.setCalibratedSensors(calibratedSensors);
    m_resultsAndSettings.setPNom(minimumWeightingLimit);
    m_resultString.clear();
    emit resultStringChanged(m_resultString);
    m_resultsModel.setReferenceChannel(referenceChan);
    m_resultsModel.setCalibratedList(calibratedList);
    foreach(QList<double> line, m_dbWorker.getData(experimentId)) {
        m_resultsModel.append(line);
    }
}

QVariantList SoftwareConfiguration::getSoftwareCorrectionFactor(int index, int chan)
{
    QString calibratedSerial;
    int sensorIndex = -1;
    QString sensorSerial;
    double correctionFactor = -1;
    double sensitivity = -1;
    int weightingLimit = -1;
    int filterLength = -1;
    double adcPgaGain = -1;
    if(index == 0) {
        calibratedSerial = m_dbWorker.getSzobteSerialNumber();
        sensorIndex = m_dbWorker.szobteGetSensorModelIndex(chan);
        sensorSerial = m_dbWorker.szobteGetSensorSerialNumber(chan);
        correctionFactor = m_dbWorker.szobteGetChannelCorrectionFactor(chan);
        sensitivity = m_dbWorker.szobteGetChannelSensitivity(chan);
        weightingLimit = m_dbWorker.szobteGetChannelWeightingLimit(chan);
        filterLength = m_dbWorker.szobteGetChannelFilterLength(chan);
        adcPgaGain = -1;
    } else {
        int ch = chan;
        if(ch % 2 == 1) {
            ch++;
        }
        int amp = ch / 2;
        calibratedSerial = QString("%1:%2").arg(amp).arg(m_dbWorker.imGetSerialNumber(amp));
        sensorIndex = m_dbWorker.imGetSensorModelIndex(chan);
        sensorSerial = m_dbWorker.imGetSensorSerialNumber(chan);
        correctionFactor = -1;
        sensitivity = m_dbWorker.imGetChannelSensitivity(chan);
        weightingLimit = m_dbWorker.imGetChannelWeightingLimit(chan);
        filterLength = m_dbWorker.imGetFilterLength(chan);
        adcPgaGain = m_dbWorker.imGetPgaGain(amp);
        chan += 11;
    }
    QVariantList result;
    m_dbWorker.getSoftwareCorrectionFactor(index, calibratedSerial, chan, sensorIndex, sensorSerial, correctionFactor, sensitivity, weightingLimit, filterLength, adcPgaGain, &result);
    return result;
}

void SoftwareConfiguration::onCalibratedAmplifierIndexChanged() {
    emit calibratedEnabledChannelsNumberChanged();
}

void SoftwareConfiguration::onSzobteEnabledChansModelChanged() {
    if(getCalibratedIndex() == 0) {
        emit calibratedEnabledChannelsNumberChanged();
    }
}

int SoftwareConfiguration::getCalibratedEnabledChannelsNumber() {
    if(getCalibratedIndex() == 0) {
        return m_dbWorker.szobteGetEnabledChans().length();
    } else {
        return m_dbWorker.imGetEnabledChans().length();
    }
}

int SoftwareConfiguration::getCalibratedIndex() {
    return m_dbWorker.getCalibratedIndex();
}

void SoftwareConfiguration::setCalibratedIndex(int index) {
    m_dbWorker.setCalibratedIndex(index);
}

int SoftwareConfiguration::getReferenceIndex() {
    return m_dbWorker.getReferenceIndex();
}

void SoftwareConfiguration::setReferenceIndex(int index) {
    m_dbWorker.setReferenceIndex(index);
}

int SoftwareConfiguration::getReferenceChannel() {
    return m_dbWorker.getReferenceChannel();
}

void SoftwareConfiguration::setReferenceChannel(int ch) {
    m_dbWorker.setReferenceChannel(ch);
}

QList<int> ResultsModel::getCalibratedChannels(int index) {
    QList<int> list;
    for(int i = 0; i < m_calibratedChannelSet.size(); i++) {
        if(m_calibratedChannelSet[i]) {
            list.append(index == 0 ? i+1: i-9);
        }
    }
    return list;
}

void SoftwareConfiguration::onCalculateResult(double a, double b, double aUp, double bUp, double aDown, double bDown,
                                              double aq, double bq, double cq, double nonLinearity) {
    m_resultsAndSettings.setA(a);
    m_resultsAndSettings.setB(b);
    m_resultsAndSettings.setAUp(aUp);
    m_resultsAndSettings.setBUp(bUp);
    m_resultsAndSettings.setADown(aDown);
    m_resultsAndSettings.setBDown(bDown);
    m_resultsAndSettings.setAq(aq);
    m_resultsAndSettings.setBq(bq);
    m_resultsAndSettings.setCq(cq);
    m_resultsAndSettings.setNonLinearity(nonLinearity);
    m_resultsAndSettings.setPercent(40); // задавать из графической части

    m_resultString.clear();
    m_resultString.append(QString("a: %1\n").arg(m_resultsAndSettings.a()));
    m_resultString.append(QString("b: %1\n").arg(m_resultsAndSettings.b()));
    m_resultString.append(QString("aUp: %1\n").arg(m_resultsAndSettings.aUp()));
    m_resultString.append(QString("bUp :%1\n").arg(m_resultsAndSettings.bUp()));
    m_resultString.append(QString("aDown: %1\n").arg(m_resultsAndSettings.aDown()));
    m_resultString.append(QString("bDown: %1\n").arg(m_resultsAndSettings.bDown()));
    m_resultString.append(QString("aq: %1\n").arg(m_resultsAndSettings.aq()));
    m_resultString.append(QString("bq: %1\n").arg(m_resultsAndSettings.bq()));
    m_resultString.append(QString("cq: %1\n").arg(m_resultsAndSettings.cq()));
    m_resultString.append(QString("nonlinearity [kg]: %1\n").arg(m_resultsAndSettings.nonLinearity()));
    m_resultString.append(QString("fUp: %1\n").arg(m_resultsAndSettings.fUp()));
    m_resultString.append(QString("fDown: %1\n").arg(m_resultsAndSettings.fDown()));
    m_resultString.append(QString("hysteresis [kg]: %1\n").arg(m_resultsAndSettings.hyst()));
    m_resultString.append(QString("hysteresis [%]: %1\n").arg(m_resultsAndSettings.hystPercent()));
    m_resultString.append(QString("nonlinearity [%]: %1\n").arg(m_resultsAndSettings.nonLinearityPercent()));
    m_resultString.append(QString("reference index: %1\n").arg(m_resultsAndSettings.referenceIndex())); // 0 = szobte, 1 = intuition, 2 = tenzo-m
    m_resultString.append(QString("reference serial: %1\n").arg(m_resultsAndSettings.referenceSerial()));
    m_resultString.append(QString("reference channel: %1\n").arg(m_resultsAndSettings.referenceSensor().chan()));
    m_resultString.append(QString("reference sensor index: %1\n").arg(m_resultsAndSettings.referenceSensor().index()));
    m_resultString.append(QString("reference sensor serial: %1\n").arg(m_resultsAndSettings.referenceSensor().serial()));
    m_resultString.append(QString("reference correction factor: %1\n").arg(m_resultsAndSettings.referenceSensor().correctionFactor()));
    m_resultString.append(QString("reference sensitivity: %1\n").arg(m_resultsAndSettings.referenceSensor().sensitivity()));
    m_resultString.append(QString("reference weighting limit: %1\n").arg(m_resultsAndSettings.referenceSensor().weightingLimit()));
    m_resultString.append(QString("reference filter length: %1\n").arg(m_resultsAndSettings.referenceSensor().filterLength()));
    m_resultString.append(QString("reference ADC PGA gain: %1\n").arg(m_resultsAndSettings.referenceSensor().adcPgaGain()));
    m_resultString.append(QString("reference software correction factor: %1\n").arg(m_resultsAndSettings.referenceSensor().softwareCorrectionFactor()));
    if(m_resultsAndSettings.referenceIndex() == 2) { // if tenzo-M, we need to add the weight of big bags
        m_resultString.append(QString("big bags weight: %1\n").arg(m_dbWorker.getTenzoMBigBagsWeight()));
    }
    m_resultString.append(QString("calibrated index: %1\n").arg(m_resultsAndSettings.calibratedIndex())); // 0 = szobte, 1 = im
    m_resultString.append(QString("calibrated serial: %1\n").arg(m_resultsAndSettings.calibratedSerial()));

    foreach(SensorSettings sensor, m_resultsAndSettings.calibratedSensors()) {
        m_resultString.append(QString("calibrated channel: %1\n").arg(sensor.chan()));
        m_resultString.append(QString("calibrated sensor index: %1\n").arg(sensor.index()));
        m_resultString.append(QString("calibrated sensor serial: %1\n").arg(sensor.serial()));
        m_resultString.append(QString("calibrated correction factor: %1\n").arg(sensor.correctionFactor()));
        m_resultString.append(QString("calibrated sensitivity: %1\n").arg(sensor.sensitivity()));
        m_resultString.append(QString("calibrated weighting limit: %1\n").arg(sensor.weightingLimit()));
        m_resultString.append(QString("calibrated filter length: %1\n").arg(sensor.filterLength()));
        m_resultString.append(QString("calibrated ADC PGA gain: %1\n").arg(sensor.adcPgaGain()));
        m_resultString.append(QString("calibrated software correction factor: %1\n").arg(sensor.softwareCorrectionFactor()));
    }
    emit resultStringChanged(m_resultString);
}

void SoftwareConfiguration::onBlockedConfigurationChanged(bool blocked) {
    if(blocked == true) {
        if(m_openFromDb) {
        } else {
            m_resultsAndSettings.setReferenceIndex(m_dbWorker.getReferenceIndex());
            SensorSettings referenceSensor;
            referenceSensor.setChan(m_resultsAndSettings.referenceIndex() == 0 ? getReferenceChannel(): 1);
            switch(m_resultsAndSettings.referenceIndex()) {
            case 0:
                m_resultsAndSettings.setReferenceSerial(m_dbWorker.getSzobteSerialNumber());
                referenceSensor.setIndex(m_dbWorker.szobteGetSensorModelIndex(referenceSensor.chan()));
                referenceSensor.setSerial(m_dbWorker.szobteGetSensorSerialNumber(referenceSensor.chan()));
                referenceSensor.setCorrectionFactor(m_dbWorker.szobteGetChannelCorrectionFactor(referenceSensor.chan()));
                referenceSensor.setSensitivity(m_dbWorker.szobteGetChannelSensitivity(referenceSensor.chan()));
                referenceSensor.setWeightingLimit(m_dbWorker.szobteGetChannelWeightingLimit(referenceSensor.chan()));
                referenceSensor.setFilterLength(m_dbWorker.szobteGetChannelFilterLength(referenceSensor.chan()));
                referenceSensor.setAdcPgaGain(-1); //  нет такого параметра в китайском усилителе
                referenceSensor.setSoftwareCorrectionFactor(m_dbWorker.szobteGetChannelSoftwareCorrectionFactor(referenceSensor.chan()));
                break;
            case 1:
                m_resultsAndSettings.setReferenceSerial(m_dbWorker.getIntuitionSerialNumber());
                referenceSensor.setIndex(m_dbWorker.intuitionGetSensorModelIndex());
                referenceSensor.setSerial(m_dbWorker.intuitionGetSensorSerialNumber());
                referenceSensor.setCorrectionFactor(-1); // не настраивается для английского усилителя
                referenceSensor.setSensitivity(-1); // не настраивается для английского усилителя
                referenceSensor.setWeightingLimit(-1); // не настраивается для английского усилителя
                referenceSensor.setFilterLength(-1); // не настраивается для английского усилителя
                referenceSensor.setAdcPgaGain(-1); // не настраивается для английского усилителя
                referenceSensor.setSoftwareCorrectionFactor(1); // не надо ни на что делить английский усилитель
                break;
            case 2:
                m_resultsAndSettings.setReferenceSerial(m_dbWorker.getTenzoMSerialNumber()); // tenzo-m serial
                referenceSensor.setIndex(-1); // tenzo-m doesn't have any sensors
                referenceSensor.setSerial(""); // tenzo-m doesn't have any sensors
                referenceSensor.setCorrectionFactor(-1); // не настраивается для весов
                referenceSensor.setSensitivity(-1); // не настраивается для весов
                referenceSensor.setWeightingLimit(-1); // не настраивается для весов
                referenceSensor.setFilterLength(-1); // не настраивается для весов
                referenceSensor.setAdcPgaGain(-1); // не настраивается для весов
                referenceSensor.setSoftwareCorrectionFactor(1); // не надо ни на что делить весы
                break;
            }
            m_resultsAndSettings.setReferenceSensor(referenceSensor);
            m_resultsAndSettings.setCalibratedIndex(m_dbWorker.getCalibratedIndex());
            switch (m_resultsAndSettings.calibratedIndex()) {
            case 0:
                m_resultsAndSettings.setCalibratedSerial(m_dbWorker.getSzobteSerialNumber());
                break;
            case 1:
                QList<int> imEnabledChans = m_resultsModel.getCalibratedChannels(1);
                bool amps[6] = {false, false, false, false, false, false};
                QString imMetaSerial;
                for(int i = 0; i < imEnabledChans.size(); i++) {
                    int ch = imEnabledChans[i];
                    if(ch % 2 == 1) {
                        ch++;
                    }
                    amps[ch / 2 - 1] = true;
                }
                for(int i = 0; i < 6; i++) {
                    if(amps[i]) {
                        imMetaSerial.append(QString("%1:%2,").arg(i+1).arg(m_dbWorker.imGetSerialNumber(i+1)));
                    }
                }
                if(imMetaSerial.size() > 0) {
                    imMetaSerial.chop(1);
                }
                m_resultsAndSettings.setCalibratedSerial(imMetaSerial);
                break;
            }
            QList<int> calibratedChans = m_resultsModel.getCalibratedChannels(m_resultsAndSettings.calibratedIndex());
            QList<SensorSettings> calibratedSensors;
            int minimumWeightingLimit = 0; // за предел взвешивания принимаем минимальный сконфигурированный вес из всех калибруемых каналов
            for(int i = 0; i < calibratedChans.size(); i++) {
                int chan = calibratedChans[i];
                SensorSettings sensor;
                switch(m_resultsAndSettings.calibratedIndex()) {
                case 0:
                    sensor.setChan(chan);
                    sensor.setIndex(m_dbWorker.szobteGetSensorModelIndex(chan));
                    sensor.setSerial(m_dbWorker.szobteGetSensorSerialNumber(chan));
                    sensor.setCorrectionFactor(m_dbWorker.szobteGetChannelCorrectionFactor(chan));
                    sensor.setSensitivity(m_dbWorker.szobteGetChannelSensitivity(chan));
                    sensor.setWeightingLimit(m_dbWorker.szobteGetChannelWeightingLimit(chan));
                    sensor.setFilterLength(m_dbWorker.szobteGetChannelFilterLength(chan));
                    sensor.setAdcPgaGain(-1); //  нет такого параметра в китайском усилителе
                    sensor.setSoftwareCorrectionFactor(m_dbWorker.szobteGetChannelSoftwareCorrectionFactor(chan));
                    break;
                case 1:
                    sensor.setChan(chan);
                    sensor.setIndex(m_dbWorker.imGetSensorModelIndex(chan));
                    sensor.setSerial(m_dbWorker.imGetSensorSerialNumber(chan));
                    sensor.setCorrectionFactor(-1);
                    sensor.setSensitivity(m_dbWorker.imGetChannelSensitivity(chan));
                    sensor.setWeightingLimit(m_dbWorker.imGetChannelWeightingLimit(chan));
                    sensor.setFilterLength(m_dbWorker.imGetFilterLength(chan));
                    int ch = chan;
                    if(ch % 2 == 1) {
                        ch++;
                    }
                    int amp = ch / 2;
                    sensor.setAdcPgaGain(m_dbWorker.imGetPgaGain(amp));
                    sensor.setSoftwareCorrectionFactor(m_dbWorker.imGetChannelSoftwareCorrectionFactor(chan));
                    break;
                }
                if(i == 0) {
                    minimumWeightingLimit = sensor.weightingLimit();
                } else {
                    if(minimumWeightingLimit > sensor.weightingLimit()) {
                        minimumWeightingLimit = sensor.weightingLimit();
                    }
                }
                calibratedSensors.append(sensor);
            }
            m_resultsAndSettings.setCalibratedSensors(calibratedSensors);
            m_resultsAndSettings.setPNom(minimumWeightingLimit);
        }
    }
}

int SensorSettings::chan() const
{
    return m_chan;
}

void SensorSettings::setChan(int chan)
{
    m_chan = chan;
}

int SensorSettings::index() const
{
    return m_index;
}

void SensorSettings::setIndex(int index)
{
    m_index = index;
}

QString SensorSettings::serial() const
{
    return m_serial;
}

void SensorSettings::setSerial(const QString &serial)
{
    m_serial = serial;
}

double SensorSettings::correctionFactor() const
{
    return m_correctionFactor;
}

void SensorSettings::setCorrectionFactor(double correctionFactor)
{
    m_correctionFactor = correctionFactor;
}

double SensorSettings::sensitivity() const
{
    return m_sensitivity;
}

void SensorSettings::setSensitivity(double sensitivity)
{
    m_sensitivity = sensitivity;
}

int SensorSettings::weightingLimit() const
{
    return m_weightingLimit;
}

void SensorSettings::setWeightingLimit(int weightingLimit)
{
    m_weightingLimit = weightingLimit;
}

int SensorSettings::filterLength() const
{
    return m_filterLength;
}

void SensorSettings::setFilterLength(int filterLength)
{
    m_filterLength = filterLength;
}

double SensorSettings::adcPgaGain() const
{
    return m_adcPgaGain;
}

void SensorSettings::setAdcPgaGain(double adcPgaGain)
{
    m_adcPgaGain = adcPgaGain;
}

double SensorSettings::softwareCorrectionFactor() const
{
    return m_softwareCorrectionFactor;
}

void SensorSettings::setSoftwareCorrectionFactor(double softwareCorrectionFactor)
{
    m_softwareCorrectionFactor = softwareCorrectionFactor;
}
