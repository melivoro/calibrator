import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtCharts 2.15

Item {
    property var szobteChan1LineSeries
    property var szobteChan2LineSeries
    property var szobteChan3LineSeries
    property var szobteChan4LineSeries
    property var szobteChan5LineSeries
    property var szobteChan6LineSeries
    property var szobteChan7LineSeries
    property var szobteChan8LineSeries
    property var szobteChan9LineSeries
    property var szobteChan10LineSeries
    property  var intuitionLineSeries
    property var tenzoMLineSeries
    property var imChan1LineSeries
    property var imChan2LineSeries
    property var imChan3LineSeries
    property var imChan4LineSeries
    property var imChan5LineSeries
    property var imChan6LineSeries
    property var imChan7LineSeries
    property var imChan8LineSeries
    property var imChan9LineSeries
    property var imChan10LineSeries
    property var imChan11LineSeries
    property var imChan12LineSeries
    property var imLineSeries
    property double szobteChan1Value
    property double szobteChan2Value
    property double szobteChan3Value
    property double szobteChan4Value
    property double szobteChan5Value
    property double szobteChan6Value
    property double szobteChan7Value
    property double szobteChan8Value
    property double szobteChan9Value
    property double szobteChan10Value
    property double intuitionValue
    property double imValue
    property double imChan1Value
    property double imChan2Value
    property double imChan3Value
    property double imChan4Value
    property double imChan5Value
    property double imChan6Value
    property double imChan7Value
    property double imChan8Value
    property double imChan9Value
    property double imChan10Value
    property double imChan11Value
    property double imChan12Value
    property double tenzoMValue
    property double tenzoMFixedValue
    property double tenzoMSumValue
    property double calibratedTotal: {
       var szobteCh1 = isSzobteChan1Calibrated? szobteChan1Value: 0
       var szobteCh2 = isSzobteChan2Calibrated? szobteChan2Value: 0
       var szobteCh3 = isSzobteChan3Calibrated? szobteChan3Value: 0
       var szobteCh4 = isSzobteChan4Calibrated? szobteChan4Value: 0
       var szobteCh5 = isSzobteChan5Calibrated? szobteChan5Value: 0
       var szobteCh6 = isSzobteChan6Calibrated? szobteChan6Value: 0
       var szobteCh7 = isSzobteChan7Calibrated? szobteChan7Value: 0
       var szobteCh8 = isSzobteChan8Calibrated? szobteChan8Value: 0
       var szobteCh9 = isSzobteChan9Calibrated? szobteChan9Value: 0
       var szobteCh10 = isSzobteChan10Calibrated? szobteChan10Value: 0
       var imCh1 = isImChan1Calibrated? imChan1Value: 0
       var imCh2 = isImChan2Calibrated? imChan2Value: 0
       var imCh3 = isImChan3Calibrated? imChan3Value: 0
       var imCh4 = isImChan4Calibrated? imChan4Value: 0
       var imCh5 = isImChan5Calibrated? imChan5Value: 0
       var imCh6 = isImChan6Calibrated? imChan6Value: 0
       var imCh7 = isImChan7Calibrated? imChan7Value: 0
       var imCh8 = isImChan8Calibrated? imChan8Value: 0
       var imCh9 = isImChan9Calibrated? imChan9Value: 0
       var imCh10 = isImChan10Calibrated? imChan10Value: 0
       var imCh11 = isImChan11Calibrated? imChan11Value: 0
       var imCh12 = isImChan12Calibrated? imChan12Value: 0
       return szobteCh1 + szobteCh2 + szobteCh3 + szobteCh4 + szobteCh5 +
       szobteCh6 + szobteCh7 + szobteCh8 + szobteCh9 + szobteCh10 +
       imCh1 + imCh2 + imCh3 + imCh4 + imCh5 + imCh6 + imCh7 + imCh8 + imCh9 + imCh10 + imCh11 + imCh12
    }
    property double imGroup1: {
        var imCh1 = isImChan1Calibrated? imChan1Value: 0
        var imCh2 = isImChan2Calibrated? imChan2Value: 0
        var imCh3 = isImChan3Calibrated? imChan3Value: 0
        var imCh4 = isImChan4Calibrated? imChan4Value: 0
        var imCh5 = isImChan5Calibrated? imChan5Value: 0
        var imCh6 = isImChan6Calibrated? imChan6Value: 0
        return imCh1 + imCh2 + imCh3 + imCh4 + imCh5 + imCh6
    }
    property double imGroup2: {
        var imCh7 = isImChan7Calibrated? imChan1Value: 0
        var imCh8 = isImChan8Calibrated? imChan2Value: 0
        var imCh9 = isImChan9Calibrated? imChan3Value: 0
        var imCh10 = isImChan10Calibrated? imChan4Value: 0
        var imCh11 = isImChan11Calibrated? imChan5Value: 0
        var imCh12 = isImChan12Calibrated? imChan6Value: 0
        return imCh7 + imCh8 + imCh9 + imCh10 + imCh11 + imCh12
    }
    signal appendSzobteChan1(var t, var data)
    signal appendSzobteChan2(var t, var data)
    signal appendSzobteChan3(var t, var data)
    signal appendSzobteChan4(var t, var data)
    signal appendSzobteChan5(var t, var data)
    signal appendSzobteChan6(var t, var data)
    signal appendSzobteChan7(var t, var data)
    signal appendSzobteChan8(var t, var data)
    signal appendSzobteChan9(var t, var data)
    signal appendSzobteChan10(var t, var data)
    signal appendImChan1(var t, var data)
    signal appendImChan2(var t, var data)
    signal appendImChan3(var t, var data)
    signal appendImChan4(var t, var data)
    signal appendImChan5(var t, var data)
    signal appendImChan6(var t, var data)
    signal appendImChan7(var t, var data)
    signal appendImChan8(var t, var data)
    signal appendImChan9(var t, var data)
    signal appendImChan10(var t, var data)
    signal appendImChan11(var t, var data)
    signal appendImChan12(var t, var data)
    signal appendIm(var t, var data)
    signal appendIntuition(var t, var data)
    signal appendTenzoM(var t, var data)
    property bool isSzobteChan1Calibrated
    property bool isSzobteChan2Calibrated
    property bool isSzobteChan3Calibrated
    property bool isSzobteChan4Calibrated
    property bool isSzobteChan5Calibrated
    property bool isSzobteChan6Calibrated
    property bool isSzobteChan7Calibrated
    property bool isSzobteChan8Calibrated
    property bool isSzobteChan9Calibrated
    property bool isSzobteChan10Calibrated
    property bool isImChan1Calibrated
    property bool isImChan2Calibrated
    property bool isImChan3Calibrated
    property bool isImChan4Calibrated
    property bool isImChan5Calibrated
    property bool isImChan6Calibrated
    property bool isImChan7Calibrated
    property bool isImChan8Calibrated
    property bool isImChan9Calibrated
    property bool isImChan10Calibrated
    property bool isImChan11Calibrated
    property bool isImChan12Calibrated
    property bool isImCalibrated: isImChan1Calibrated | isImChan2Calibrated | isImChan3Calibrated | isImChan4Calibrated | isImChan5Calibrated | isImChan6Calibrated |
                                  isImChan7Calibrated | isImChan8Calibrated | isImChan9Calibrated | isImChan10Calibrated | isImChan11Calibrated | isImChan12Calibrated
    property bool isSzobteChan1Reference
    property bool isSzobteChan2Reference
    property bool isSzobteChan3Reference
    property bool isSzobteChan4Reference
    property bool isSzobteChan5Reference
    property bool isSzobteChan6Reference
    property bool isSzobteChan7Reference
    property bool isSzobteChan8Reference
    property bool isSzobteChan9Reference
    property bool isSzobteChan10Reference
    property bool isIntuitionReference
    property bool isTenzoMReference
    property bool isTenzoMFixPressed

    ChartView {
        id: chartView
        anchors {
            fill: parent
            rightMargin: 270
            bottomMargin: 50
        }
        title: qsTr("Time charts")
        titleFont.pointSize: 20
        antialiasing: true
        legend.visible: false
        DateTimeAxis {
            id: dateTimeAxis
            format: "h:mm:ss"
            tickCount: 5
        }
        ValueAxis {
            id: valueAxis
            min: 0
            max: 100
        }
        Timer {
            interval: 250
            running: true
            repeat: true
            onTriggered: {
                var t = Date.now()
                dateTimeAxis.min = new Date(t - 60000)
                dateTimeAxis.max = new Date(t)
                if (szobteChan1LineSeries !== null && szobteChan1LineSeries !== undefined ? (szobteChan1LineSeries.count > 300): false) {
                    szobteChan1LineSeries.removePoints(0, 100)
                    console.log("ch1", szobteChan1LineSeries.count)
                }
                if (szobteChan2LineSeries !== null && szobteChan2LineSeries !== undefined ? (szobteChan2LineSeries.count > 300): false) {
                    szobteChan2LineSeries.removePoints(0, 100)
                    console.log("ch2", szobteChan2LineSeries.count)
                }
                if (szobteChan3LineSeries !== null && szobteChan3LineSeries !== undefined ? (szobteChan3LineSeries.count > 300): false) {
                    szobteChan3LineSeries.removePoints(0, 100)
                    console.log("ch3", szobteChan3LineSeries.count)
                }
                if (szobteChan4LineSeries !== null && szobteChan4LineSeries !== undefined ? (szobteChan4LineSeries.count > 300): false) {
                    szobteChan4LineSeries.removePoints(0, 100)
                    console.log("ch4", szobteChan4LineSeries.count)
                }
                if (szobteChan5LineSeries !== null && szobteChan5LineSeries !== undefined ? (szobteChan5LineSeries.count > 300): false) {
                    szobteChan5LineSeries.removePoints(0, 100)
                    console.log("ch5", szobteChan5LineSeries.count)
                }
                if (szobteChan6LineSeries !== null && szobteChan6LineSeries !== undefined ? (szobteChan6LineSeries.count > 300): false) {
                    szobteChan6LineSeries.removePoints(0, 100)
                    console.log("ch6", szobteChan6LineSeries.count)
                }
                if (szobteChan7LineSeries !== null && szobteChan7LineSeries !== undefined ? (szobteChan7LineSeries.count > 300): false) {
                    szobteChan7LineSeries.removePoints(0, 100)
                    console.log("ch7", szobteChan7LineSeries.count)
                }
                if (szobteChan8LineSeries !== null && szobteChan8LineSeries !== undefined ? (szobteChan8LineSeries.count > 300): false) {
                    szobteChan8LineSeries.removePoints(0, 100)
                    console.log("ch8", szobteChan8LineSeries.count)
                }
                if (szobteChan9LineSeries !== null && szobteChan9LineSeries !== undefined ? (szobteChan9LineSeries.count > 300): false) {
                    szobteChan9LineSeries.removePoints(0, 100)
                    console.log("ch9", szobteChan9LineSeries.count)
                }
                if (szobteChan10LineSeries !== null && szobteChan10LineSeries !== undefined ? (szobteChan10LineSeries.count > 300): false) {
                    szobteChan10LineSeries.removePoints(0, 100)
                    console.log("ch10", szobteChan10LineSeries.count)
                }
                if (imChan1LineSeries !== null && imChan1LineSeries !== undefined ? (imChan1LineSeries.count > 300): false) {
                    imChan1LineSeries.removePoints(0, 100)
                    console.log("IM ch1", imChan1LineSeries.count)
                }
                if (imChan2LineSeries !== null && imChan2LineSeries !== undefined ? (imChan2LineSeries.count > 300): false) {
                    imChan2LineSeries.removePoints(0, 100)
                    console.log("IM ch2", imChan2LineSeries.count)
                }
                if (imChan3LineSeries !== null && imChan3LineSeries !== undefined ? (imChan3LineSeries.count > 300): false) {
                    imChan3LineSeries.removePoints(0, 100)
                    console.log("IM ch3", imChan3LineSeries.count)
                }
                if (imChan4LineSeries !== null && imChan4LineSeries !== undefined ? (imChan4LineSeries.count > 300): false) {
                    imChan4LineSeries.removePoints(0, 100)
                    console.log("IM ch4", imChan4LineSeries.count)
                }
                if (imChan5LineSeries !== null && imChan5LineSeries !== undefined ? (imChan5LineSeries.count > 300): false) {
                    imChan5LineSeries.removePoints(0, 100)
                    console.log("IM ch5", imChan5LineSeries.count)
                }
                if (imChan6LineSeries !== null && imChan6LineSeries !== undefined ? (imChan6LineSeries.count > 300): false) {
                    imChan6LineSeries.removePoints(0, 100)
                    console.log("IM ch6", imChan6LineSeries.count)
                }
                if (imChan7LineSeries !== null && imChan7LineSeries !== undefined ? (imChan7LineSeries.count > 300): false) {
                    imChan7LineSeries.removePoints(0, 100)
                    console.log("IM ch7", imChan7LineSeries.count)
                }
                if (imChan8LineSeries !== null && imChan8LineSeries !== undefined ? (imChan8LineSeries.count > 300): false) {
                    imChan8LineSeries.removePoints(0, 100)
                    console.log("IM ch8", imChan8LineSeries.count)
                }
                if (imChan9LineSeries !== null && imChan9LineSeries !== undefined ? (imChan9LineSeries.count > 300): false) {
                    imChan9LineSeries.removePoints(0, 100)
                    console.log("IM ch9", imChan9LineSeries.count)
                }
                if (imChan10LineSeries !== null && imChan10LineSeries !== undefined ? (imChan10LineSeries.count > 300): false) {
                    imChan10LineSeries.removePoints(0, 100)
                    console.log("IM ch10", imChan10LineSeries.count)
                }
                if (imChan11LineSeries !== null && imChan11LineSeries !== undefined ? (imChan11LineSeries.count > 300): false) {
                    imChan11LineSeries.removePoints(0, 100)
                    console.log("IM ch11", imChan11LineSeries.count)
                }
                if (imChan12LineSeries !== null && imChan12LineSeries !== undefined ? (imChan12LineSeries.count > 300): false) {
                    imChan12LineSeries.removePoints(0, 100)
                    console.log("IM ch12", imChan12LineSeries.count)
                }
                if (imLineSeries !== null && imLineSeries !== undefined ? (imLineSeries.count > 300): false) {
                    imLineSeries.removePoints(0, 100)
                    console.log("IM", imLineSeries.count)
                }
                if (intuitionLineSeries !== null && intuitionLineSeries !== undefined ? (intuitionLineSeries.count > 300): false) {
                    intuitionLineSeries.removePoints(0, 100)
                    console.log("intuition", intuitionLineSeries.count)
                }
                if (tenzoMLineSeries !== null && tenzoMLineSeries !== undefined ? (tenzoMLineSeries.count > 300): false) {
                    tenzoMLineSeries.removePoints(0, 100)
                    console.log("tenzo-m", tenzoMLineSeries.count)
                }
            }
        }
        MouseArea {
            property int prevY
            anchors.fill: parent
            hoverEnabled: true
            onPressed: {
                prevY = mouseY
            }
            onMouseYChanged: {
                if(containsPress) {
                    chartView.scrollUp(mouseY - prevY)
                    prevY = mouseY
                }
            }
            onWheel: {
                if(wheel.angleDelta.y > 0) {
                    chartView.zoom(1.1)
                } else {
                    chartView.zoom(0.9)
                }
            }
            onDoubleClicked: {
                valueAxis.min = 0
                valueAxis.max = 100
                chartView.zoomReset()
            }
        }
        function recreateLineSeries() {
            chartView.removeAllSeries()
            if(dbworker.szobteGetChannelState(1)) {
                szobteChan1LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 1", dateTimeAxis, valueAxis)
                szobteChan1LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan1LineSeries.useOpenGL = true
            }
            szobteChan1LegendLabel.visible = dbworker.szobteGetChannelState(1)
            if(dbworker.szobteGetChannelState(2)) {
                szobteChan2LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 2", dateTimeAxis, valueAxis)
                szobteChan2LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan2LineSeries.useOpenGL = true
            }
            szobteChan2LegendLabel.visible = dbworker.szobteGetChannelState(2)
            if(dbworker.szobteGetChannelState(3)) {
                szobteChan3LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 3", dateTimeAxis, valueAxis)
                szobteChan3LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan3LineSeries.useOpenGL = true
            }
            szobteChan3LegendLabel.visible = dbworker.szobteGetChannelState(3)
            if(dbworker.szobteGetChannelState(4)) {
                szobteChan4LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 4", dateTimeAxis, valueAxis)
                szobteChan4LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan4LineSeries.useOpenGL = true
            }
            szobteChan4LegendLabel.visible = dbworker.szobteGetChannelState(4)
            if(dbworker.szobteGetChannelState(5)) {
                szobteChan5LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 5", dateTimeAxis, valueAxis)
                szobteChan5LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan5LineSeries.useOpenGL = true
            }
            szobteChan5LegendLabel.visible = dbworker.szobteGetChannelState(5)
            if(dbworker.szobteGetChannelState(6)) {
                szobteChan6LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 6", dateTimeAxis, valueAxis)
                szobteChan6LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan6LineSeries.useOpenGL = true
            }
            szobteChan6LegendLabel.visible = dbworker.szobteGetChannelState(6)
            if(dbworker.szobteGetChannelState(7)) {
                szobteChan7LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 7", dateTimeAxis, valueAxis)
                szobteChan7LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan7LineSeries.useOpenGL = true
            }
            szobteChan7LegendLabel.visible = dbworker.szobteGetChannelState(7)
            if(dbworker.szobteGetChannelState(8)) {
                szobteChan8LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 8", dateTimeAxis, valueAxis)
                szobteChan8LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan8LineSeries.useOpenGL = true
            }
            szobteChan8LegendLabel.visible = dbworker.szobteGetChannelState(8)
            if(dbworker.szobteGetChannelState(9)) {
                szobteChan9LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 9", dateTimeAxis, valueAxis)
                szobteChan9LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan9LineSeries.useOpenGL = true
            }
            szobteChan9LegendLabel.visible = dbworker.szobteGetChannelState(9)
            if(dbworker.szobteGetChannelState(10)) {
                szobteChan10LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 10", dateTimeAxis, valueAxis)
                szobteChan10LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                szobteChan10LineSeries.useOpenGL = true
            }
            szobteChan10LegendLabel.visible = dbworker.szobteGetChannelState(10)
            if(dbworker.imGetChannelState(1)) {
                imChan1LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 1", dateTimeAxis, valueAxis)
                imChan1LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan1LineSeries.useOpenGL = true
            }
            imChan1LegendLabel.visible = dbworker.imGetChannelState(1)
            if(dbworker.imGetChannelState(2)) {
                imChan2LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 2", dateTimeAxis, valueAxis)
                imChan2LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan2LineSeries.useOpenGL = true
            }
            imChan2LegendLabel.visible = dbworker.imGetChannelState(2)
            if(dbworker.imGetChannelState(3)) {
                imChan3LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 3", dateTimeAxis, valueAxis)
                imChan3LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan3LineSeries.useOpenGL = true
            }
            imChan3LegendLabel.visible = dbworker.imGetChannelState(3)
            if(dbworker.imGetChannelState(4)) {
                imChan4LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 4", dateTimeAxis, valueAxis)
                imChan4LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan4LineSeries.useOpenGL = true
            }
            imChan4LegendLabel.visible = dbworker.imGetChannelState(4)
            if(dbworker.imGetChannelState(5)) {
                imChan5LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 5", dateTimeAxis, valueAxis)
                imChan5LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan5LineSeries.useOpenGL = true
            }
            imChan5LegendLabel.visible = dbworker.imGetChannelState(5)
            if(dbworker.imGetChannelState(6)) {
                imChan6LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 6", dateTimeAxis, valueAxis)
                imChan6LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan6LineSeries.useOpenGL = true
            }
            imChan6LegendLabel.visible = dbworker.imGetChannelState(6)
            if(dbworker.imGetChannelState(7)) {
                imChan7LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 7", dateTimeAxis, valueAxis)
                imChan7LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan7LineSeries.useOpenGL = true
            }
            imChan7LegendLabel.visible = dbworker.imGetChannelState(7)
            if(dbworker.imGetChannelState(8)) {
                imChan8LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 8", dateTimeAxis, valueAxis)
                imChan8LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan8LineSeries.useOpenGL = true
            }
            imChan8LegendLabel.visible = dbworker.imGetChannelState(8)
            if(dbworker.imGetChannelState(9)) {
                imChan9LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 9", dateTimeAxis, valueAxis)
                imChan9LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan9LineSeries.useOpenGL = true
            }
            imChan9LegendLabel.visible = dbworker.imGetChannelState(9)
            if(dbworker.imGetChannelState(10)) {
                imChan10LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 10", dateTimeAxis, valueAxis)
                imChan10LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan10LineSeries.useOpenGL = true
            }
            imChan10LegendLabel.visible = dbworker.imGetChannelState(10)
            if(dbworker.imGetChannelState(11)) {
                imChan11LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 11", dateTimeAxis, valueAxis)
                imChan11LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan11LineSeries.useOpenGL = true
            }
            imChan11LegendLabel.visible = dbworker.imGetChannelState(11)
            if(dbworker.imGetChannelState(12)) {
                imChan12LineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 12", dateTimeAxis, valueAxis)
                imChan12LineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imChan12LineSeries.useOpenGL = true
            }
            imChan12LegendLabel.visible = dbworker.imGetChannelState(12)
            if(true /*dbworker.imGetState()*/) {
                imLineSeries = createSeries(ChartView.SeriesTypeLine, "IM", dateTimeAxis, valueAxis)
                imLineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                imLineSeries.useOpenGL = true
            }
            imLegendLabel.visible = true //dbworker.imGetState()
            if(dbworker.intuitionGetState()) {
                intuitionLineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 1", dateTimeAxis, valueAxis)
                intuitionLineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                intuitionLineSeries.useOpenGL = true
            }
            intuitionLegendLabel.visible = dbworker.intuitionGetState()
            if(dbworker.tenzoMState) {
                tenzoMLineSeries = createSeries(ChartView.SeriesTypeLine, "Channel 1", dateTimeAxis, valueAxis)
                tenzoMLineSeries.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                tenzoMLineSeries.useOpenGL = true
            }
            tenzoMLegendLabel.visible = dbworker.tenzoMState
        }
        Column {
            anchors {
                bottom: parent.bottom
                right: parent.right
                bottomMargin: 70
                rightMargin: 80
            }
            Button {
                implicitHeight: 40
                implicitWidth: 35
                font.pointSize: 20
                text: "+"
                onClicked: {
                    chartView.zoom(1.1)
                }
            }
            Button {
                implicitHeight: 40
                implicitWidth: 35
                font.pointSize: 20
                text: "-"
                onClicked: {
                    chartView.zoom(0.9)
                }
            }
        }
    }
    Label {
        property var textSzobte: qsTr("Calibrated total: %1 kg").arg(calibratedTotal)
        property var textIm: qsTr("Calibrated total: %1 kg     |     IM group 1: %2 kg     |     IM group 2: %3 kg").arg(calibratedTotal).arg(imGroup1).arg(imGroup2)
        anchors {
            top: chartView.bottom
            left: parent.left
            topMargin: 5
            leftMargin: 20
        }
        text: isImCalibrated ? textIm: textSzobte
        font.pointSize: 20
        verticalAlignment: Qt.AlignBottom
    }
    ScrollView {
        anchors {
            left: chartView.right
            right: parent.right
            top: chartView.top
            bottom: chartView.bottom
            rightMargin: chartView.plotArea.x / 2
            bottomMargin: chartView.plotArea.y / 2
        }
        clip: true
        Column {
            spacing: 15
            RowLayout {
                Button {
                    text: qsTr("Zero")
                    icon.source: "images/zero.png"
                    font.capitalization: Font.MixedCase
                    font.pointSize: 10
                    Layout.preferredHeight: 40
                    onClicked: {
                        hardwareManager.zero()
                        tenzoMFixedValue = 0
                        isTenzoMFixPressed = false
                        tenzoMSumValue = 0
                    }
                }
                Button {
                    enabled: !isTenzoMReference | isTenzoMFixPressed
                    text: qsTr("Single weighting")
                    icon.source: "images/single.png"
                    font.capitalization: Font.MixedCase
                    font.pointSize: 10
                    Layout.preferredHeight: 40
                    onClicked: {
                        softwareConfiguration.openFromDb = false
                        softwareConfiguration.blockedConfiguration = true
                        var values= [ szobteChan1Value, szobteChan2Value, szobteChan3Value, szobteChan4Value,
                                     szobteChan5Value, szobteChan6Value, szobteChan7Value, szobteChan8Value,
                                     szobteChan9Value, szobteChan10Value, intuitionValue, imChan1Value,
                                     imChan2Value, imChan3Value, imChan4Value, imChan5Value, imChan6Value, imChan7Value,
                                     imChan8Value, imChan9Value, imChan10Value, imChan11Value, imChan12Value, tenzoMValue ]
                        if((imTypeComboBox.currentIndex == 1) && (softwareConfiguration.calibratedIndex == 1)) {
                            var imEnabledChans = [false, false, false, false, false, false, false, false, false, false, false, false]
                            if(isImChan1Calibrated) {
                                imEnabledChans[0] = true
                            }
                            if(isImChan2Calibrated) {
                                imEnabledChans[1] = true
                            }
                            if(isImChan3Calibrated) {
                                imEnabledChans[2] = true
                            }
                            if(isImChan4Calibrated) {
                                imEnabledChans[3] = true
                            }
                            if(isImChan5Calibrated) {
                                imEnabledChans[4] = true
                            }
                            if(isImChan6Calibrated) {
                                imEnabledChans[5] = true
                            }
                            if(isImChan7Calibrated) {
                                imEnabledChans[6] = true
                            }
                            if(isImChan8Calibrated) {
                                imEnabledChans[7] = true
                            }
                            if(isImChan9Calibrated) {
                                imEnabledChans[8] = true
                            }
                            if(isImChan10Calibrated) {
                                imEnabledChans[9] = true
                            }
                            if(isImChan11Calibrated) {
                                imEnabledChans[10] = true
                            }
                            if(isImChan12Calibrated) {
                                imEnabledChans[11] = true
                            }
                            for(var i = 11; i < 23; i++) {
                                values[i] = 0
                            }
                            for(i = 11; i < 23; i++) {
                                if(imEnabledChans[i-11]) {
                                    values[i] = imValue
                                    break
                                }
                            }
                        }
                        resultsModel.append(values)
                    }
                }
            }
            Button {
                text: qsTr("New cycle")
                visible: isTenzoMReference
                font.capitalization: Font.MixedCase
                font.pointSize: 10
                Layout.preferredHeight: 40
                onClicked: {
                    tenzoMFixedValue = 0
                    isTenzoMFixPressed = false
                    tenzoMSumValue = 0
                }
            }

            Label {
                text: qsTr("OT-EP01-10")
                font.pointSize: 15
                verticalAlignment: Qt.AlignBottom
            }
            Label {
                id: szobteChan1LegendLabel
                property bool selected: isSzobteChan1Calibrated || isSzobteChan1Reference
                property bool marked: isSzobteChan1Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 1%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan1Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan1LineSeries !== null && szobteChan1LineSeries !== undefined ? szobteChan1LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                id: szobteChan2LegendLabel
                property bool selected: isSzobteChan2Calibrated || isSzobteChan2Reference
                property bool marked: isSzobteChan2Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 2%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan2Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan2LineSeries !== null && szobteChan2LineSeries !== undefined ? szobteChan2LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                id: szobteChan3LegendLabel
                property bool selected: isSzobteChan3Calibrated || isSzobteChan3Reference
                property bool marked: isSzobteChan3Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 3%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan3Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan3LineSeries !== null && szobteChan3LineSeries !== undefined ? szobteChan3LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                id: szobteChan4LegendLabel
                property bool selected: isSzobteChan4Calibrated || isSzobteChan4Reference
                property bool marked: isSzobteChan4Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 4%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan4Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan4LineSeries !== null && szobteChan4LineSeries !== undefined ? szobteChan4LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                id: szobteChan5LegendLabel
                property bool selected: isSzobteChan5Calibrated || isSzobteChan5Reference
                property bool marked: isSzobteChan5Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 5%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan5Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan5LineSeries !== null && szobteChan5LineSeries !== undefined ? szobteChan5LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                id: szobteChan6LegendLabel
                property bool selected: isSzobteChan6Calibrated || isSzobteChan6Reference
                property bool marked: isSzobteChan6Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 6%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan6Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan6LineSeries !== null && szobteChan6LineSeries !== undefined ? szobteChan6LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                id: szobteChan7LegendLabel
                property bool selected: isSzobteChan7Calibrated || isSzobteChan7Reference
                property bool marked: isSzobteChan7Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 7%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan7Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan7LineSeries !== null && szobteChan7LineSeries !== undefined ? szobteChan7LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                id: szobteChan8LegendLabel
                property bool selected: isSzobteChan8Calibrated || isSzobteChan8Reference
                property bool marked: isSzobteChan8Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 8%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan8Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan8LineSeries !== null && szobteChan8LineSeries !== undefined ? szobteChan8LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                id: szobteChan9LegendLabel
                property bool selected: isSzobteChan9Calibrated || isSzobteChan9Reference
                property bool marked: isSzobteChan9Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 9%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan9Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan9LineSeries !== null && szobteChan9LineSeries !== undefined ? szobteChan9LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                id: szobteChan10LegendLabel
                property bool selected: isSzobteChan10Calibrated || isSzobteChan10Reference
                property bool marked: isSzobteChan10Reference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 10%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(szobteChan10Value).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: szobteChan10LineSeries !== null && szobteChan10LineSeries !== undefined ? szobteChan10LineSeries.color: "black"
                font.pointSize: selected? 12: 10
            }
            Label {
                text: qsTr("Intuition 20i")
                font.pointSize: 15
                lineHeight: 1.5
                verticalAlignment: Qt.AlignBottom
            }
            Label {
                id: intuitionLegendLabel
                property bool selected: isIntuitionReference
                property bool marked: isIntuitionReference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 1%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(intuitionValue).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: intuitionLineSeries !== null && intuitionLineSeries !== undefined ? intuitionLineSeries.color: "black"
                font.pointSize: selected ? 12: 10
            }
            Row {
                spacing: 10
                Label {
                    text: qsTr("IM")
                    font.pointSize: 15
                    lineHeight: 1.5
                    verticalAlignment: Qt.AlignBottom
                }
                ComboBox {
                    id: imTypeComboBox
                    implicitHeight: 40
                    model: [qsTr("CAN"), qsTr("RS485")]
                }
            }
            Label {
                id: imLegendLabel
                text: {
                    var txt = qsTr("Common <font color=\"black\">= %1 %2</font>").arg(imValue).arg(qsTr("kg"))
                    if(isImCalibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imLineSeries !== null && imLineSeries !== undefined ? imLineSeries.color: "black"
                font.pointSize: isImCalibrated? 12: 10
            }
            Label {
                id: imChan1LegendLabel
                text: {
                    var txt = qsTr("Channel 1 <font color=\"black\">= %1 %2</font>").arg(imChan1Value).arg(qsTr("kg"))
                    if(isImChan1Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan1LineSeries !== null && imChan1LineSeries !== undefined ? imChan1LineSeries.color: "black"
                font.pointSize: isImChan1Calibrated? 12: 10
            }
            Label {
                id: imChan2LegendLabel
                text: {
                    var txt = qsTr("Channel 2 <font color=\"black\">= %1 %2</font>").arg(imChan2Value).arg(qsTr("kg"))
                    if(isImChan2Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan2LineSeries !== null && imChan2LineSeries !== undefined ? imChan2LineSeries.color: "black"
                font.pointSize: isImChan2Calibrated? 12: 10
            }
            Label {
                id: imChan3LegendLabel
                text: {
                    var txt = qsTr("Channel 3 <font color=\"black\">= %1 %2</font>").arg(imChan3Value).arg(qsTr("kg"))
                    if(isImChan3Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan3LineSeries !== null && imChan3LineSeries !== undefined ? imChan3LineSeries.color: "black"
                font.pointSize: isImChan3Calibrated? 12: 10
            }
            Label {
                id: imChan4LegendLabel
                text: {
                    var txt = qsTr("Channel 4 <font color=\"black\">= %1 %2</font>").arg(imChan4Value).arg(qsTr("kg"))
                    if(isImChan4Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan4LineSeries !== null && imChan4LineSeries !== undefined ? imChan4LineSeries.color: "black"
                font.pointSize: isImChan4Calibrated? 12: 10
            }
            Label {
                id: imChan5LegendLabel
                text: {
                    var txt = qsTr("Channel 5 <font color=\"black\">= %1 %2</font>").arg(imChan5Value).arg(qsTr("kg"))
                    if(isImChan5Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan5LineSeries !== null && imChan5LineSeries !== undefined ? imChan5LineSeries.color: "black"
                font.pointSize: isImChan5Calibrated? 12: 10
            }
            Label {
                id: imChan6LegendLabel
                text: {
                    var txt = qsTr("Channel 6 <font color=\"black\">= %1 %2</font>").arg(imChan6Value).arg(qsTr("kg"))
                    if(isImChan6Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan6LineSeries !== null && imChan6LineSeries !== undefined ? imChan6LineSeries.color: "black"
                font.pointSize: isImChan6Calibrated? 12: 10
            }
            Label {
                id: imChan7LegendLabel
                text: {
                    var txt = qsTr("Channel 7 <font color=\"black\">= %1 %2</font>").arg(imChan7Value).arg(qsTr("kg"))
                    if(isImChan7Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan7LineSeries !== null && imChan7LineSeries !== undefined ? imChan7LineSeries.color: "black"
                font.pointSize: isImChan7Calibrated? 12: 10
            }
            Label {
                id: imChan8LegendLabel
                text: {
                    var txt = qsTr("Channel 8 <font color=\"black\">= %1 %2</font>").arg(imChan8Value).arg(qsTr("kg"))
                    if(isImChan8Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan8LineSeries !== null && imChan8LineSeries !== undefined ? imChan8LineSeries.color: "black"
                font.pointSize: isImChan8Calibrated? 12: 10
            }
            Label {
                id: imChan9LegendLabel
                text: {
                    var txt = qsTr("Channel 9 <font color=\"black\">= %1 %2</font>").arg(imChan9Value).arg(qsTr("kg"))
                    if(isImChan9Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan9LineSeries !== null && imChan9LineSeries !== undefined ? imChan9LineSeries.color: "black"
                font.pointSize: isImChan9Calibrated? 12: 10
            }
            Label {
                id: imChan10LegendLabel
                text: {
                    var txt = qsTr("Channel 10 <font color=\"black\">= %1 %2</font>").arg(imChan10Value).arg(qsTr("kg"))
                    if(isImChan10Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan10LineSeries !== null && imChan10LineSeries !== undefined ? imChan10LineSeries.color: "black"
                font.pointSize: isImChan10Calibrated? 12: 10
            }
            Label {
                id: imChan11LegendLabel
                text: {
                    var txt = qsTr("Channel 11 <font color=\"black\">= %1 %2</font>").arg(imChan11Value).arg(qsTr("kg"))
                    if(isImChan11Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan11LineSeries !== null && imChan11LineSeries !== undefined ? imChan11LineSeries.color: "black"
                font.pointSize: isImChan11Calibrated? 12: 10
            }
            Label {
                id: imChan12LegendLabel
                text: {
                    var txt = qsTr("Channel 12 <font color=\"black\">= %1 %2</font>").arg(imChan12Value).arg(qsTr("kg"))
                    if(isImChan12Calibrated) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: imChan12LineSeries !== null && imChan12LineSeries !== undefined ? imChan12LineSeries.color: "black"
                font.pointSize: isImChan12Calibrated? 12: 10
            }
            Label {
                text: qsTr("Tenzo-M VK-10")
                font.pointSize: 15
                lineHeight: 1.5
                verticalAlignment: Qt.AlignBottom
            }
            Label
            {
                id: tenzoMLegendLabel
                property bool selected: isTenzoMReference
                property bool marked: isTenzoMReference
                text: {
                    var mark = marked ? "*": ""
                    var txt = qsTr("Channel 1%1 <font color=\"black\">= %2 %3</font>").arg(mark).arg(tenzoMValue).arg(qsTr("kg"))
                    if(selected) {
                                  txt = "<strong>" + txt + "</strong>"
                                }
                    return txt
                }
                color: tenzoMLineSeries !== null && tenzoMLineSeries !== undefined ? tenzoMLineSeries.color: "black"
                font.pointSize: selected ? 12: 10
            }
            Button {
                text: qsTr("Fix weight")
                visible: isTenzoMReference
                enabled: !isTenzoMFixPressed
                font.capitalization: Font.MixedCase
                font.pointSize: 10
                Layout.preferredHeight: 40
                onClicked: {
                    tenzoMFixedValue = tenzoMValue
                    isTenzoMFixPressed = true
                }
            }
            Label {
                visible: isTenzoMReference
                text: {
                    var txt = qsTr("Fixed <font color=\"black\">= %1 %2</font>").arg(tenzoMFixedValue).arg(qsTr("kg"))
                    txt = "<strong>" + txt + "</strong>"
                    return txt
                }
                color: tenzoMLineSeries !== null && tenzoMLineSeries !== undefined ? tenzoMLineSeries.color: "black"
                font.pointSize: 12
            }
            Label {
                visible: isTenzoMReference
                text: {
                    var txt = qsTr("Sum <font color=\"black\">= %1 %2</font>").arg(tenzoMSumValue).arg(qsTr("kg"))
                    txt = "<strong>" + txt + "</strong>"
                    return txt
                }
                color: tenzoMLineSeries !== null && tenzoMLineSeries !== undefined ? tenzoMLineSeries.color: "black"
                font.pointSize: 12
            }
        }
    }
    onAppendSzobteChan1: {
        if(szobteChan1LineSeries !== null && szobteChan1LineSeries !== undefined) {
            szobteChan1LineSeries.append(t, data)
        }
    }
    onAppendSzobteChan2: {
        if(szobteChan2LineSeries !== null && szobteChan2LineSeries !== undefined) {
            szobteChan2LineSeries.append(t, data)
        }
    }
    onAppendSzobteChan3: {
        if(szobteChan3LineSeries !== null && szobteChan3LineSeries !== undefined) {
            szobteChan3LineSeries.append(t, data)
        }
    }
    onAppendSzobteChan4: {
        if(szobteChan4LineSeries !== null && szobteChan4LineSeries !== undefined) {
            szobteChan4LineSeries.append(t, data)
        }
    }
    onAppendSzobteChan5: {
        if(szobteChan5LineSeries !== null && szobteChan5LineSeries !== undefined) {
            szobteChan5LineSeries.append(t, data)
        }
    }
    onAppendSzobteChan6: {
        if(szobteChan6LineSeries !== null && szobteChan6LineSeries !== undefined) {
            szobteChan6LineSeries.append(t, data)
        }
    }
    onAppendSzobteChan7: {
        if(szobteChan7LineSeries !== null && szobteChan7LineSeries !== undefined) {
            szobteChan7LineSeries.append(t, data)
        }
    }
    onAppendSzobteChan8: {
        if(szobteChan8LineSeries !== null && szobteChan8LineSeries !== undefined) {
            szobteChan8LineSeries.append(t, data)
        }
    }
    onAppendSzobteChan9: {
        if(szobteChan9LineSeries !== null && szobteChan9LineSeries !== undefined) {
            szobteChan9LineSeries.append(t, data)
        }
    }
    onAppendSzobteChan10: {
        if(szobteChan10LineSeries !== null && szobteChan10LineSeries !== undefined) {
            szobteChan10LineSeries.append(t, data)
        }
    }
    onAppendImChan1: {
        if(imChan1LineSeries !== null && imChan1LineSeries !== undefined) {
            imChan1LineSeries.append(t, data)
        }
    }
    onAppendImChan2: {
        if(imChan2LineSeries !== null && imChan2LineSeries !== undefined) {
            imChan2LineSeries.append(t, data)
        }
    }
    onAppendImChan3: {
        if(imChan3LineSeries !== null && imChan3LineSeries !== undefined) {
            imChan3LineSeries.append(t, data)
        }
    }
    onAppendImChan4: {
        if(imChan4LineSeries !== null && imChan4LineSeries !== undefined) {
            imChan4LineSeries.append(t, data)
        }
    }
    onAppendImChan5: {
        if(imChan5LineSeries !== null && imChan5LineSeries !== undefined) {
            imChan5LineSeries.append(t, data)
        }
    }
    onAppendImChan6: {
        if(imChan6LineSeries !== null && imChan6LineSeries !== undefined) {
            imChan6LineSeries.append(t, data)
        }
    }
    onAppendImChan7: {
        if(imChan7LineSeries !== null && imChan7LineSeries !== undefined) {
            imChan7LineSeries.append(t, data)
        }
    }
    onAppendImChan8: {
        if(imChan8LineSeries !== null && imChan8LineSeries !== undefined) {
            imChan8LineSeries.append(t, data)
        }
    }
    onAppendImChan9: {
        if(imChan9LineSeries !== null && imChan9LineSeries !== undefined) {
            imChan9LineSeries.append(t, data)
        }
    }
    onAppendImChan10: {
        if(imChan10LineSeries !== null && imChan10LineSeries !== undefined) {
            imChan10LineSeries.append(t, data)
        }
    }
    onAppendImChan11: {
        if(imChan11LineSeries !== null && imChan11LineSeries !== undefined) {
            imChan11LineSeries.append(t, data)
        }
    }
    onAppendImChan12: {
        if(imChan12LineSeries !== null && imChan12LineSeries !== undefined) {
            imChan12LineSeries.append(t, data)
        }
    }
    onAppendIm: {
        if(imLineSeries !== null && imLineSeries !== undefined) {
            imLineSeries.append(t, data)
        }
    }
    onAppendIntuition: {
        if(intuitionLineSeries !== null && intuitionLineSeries !== undefined) {
            intuitionLineSeries.append(t, data)
        }
    }
    onAppendTenzoM: {
        if(tenzoMLineSeries !== null && tenzoMLineSeries !== undefined) {
            tenzoMLineSeries.append(t, data)
        }
    }

    function recreateLineSeries() {
        chartView.recreateLineSeries()
    }
}
