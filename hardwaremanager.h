#ifndef HARDWAREMANAGER_H
#define HARDWAREMANAGER_H

#include <QThread>
#include <QTimer>
#include "szobteamplifier.h"
#include "imamplifier.h"
#include "im2amplifier.h"
#include "intuitionamplifier.h"
#include "tenzomscales.h"

class HardwareManager : public QObject
{
    Q_OBJECT
public:
    explicit HardwareManager(QObject *parent = nullptr);
    ~HardwareManager();
    Q_PROPERTY(QString szobtePort MEMBER m_szobtePort WRITE setSzobtePort NOTIFY szobtePortChanged)
    Q_PROPERTY(QString imPort MEMBER m_imPort WRITE setImPort NOTIFY imPortChanged)
    Q_PROPERTY(QString imComPort MEMBER m_imComPort WRITE setImComPort NOTIFY imComPortChanged)
    Q_PROPERTY(QString intuitionPort MEMBER m_intuitionPort WRITE setIntuitionPort NOTIFY intuitionPortChanged)
    Q_PROPERTY(double commonCorrectionFactor MEMBER m_commonCorrectionFactor NOTIFY commonCorrectionFactorChanged)
    Q_INVOKABLE void szobteSetActiveChans(QList<int>);
    Q_INVOKABLE void imSetActiveChans(QList<int>);
    enum HardwareType { SZOBTE, Intuition, IM, TenzoM };
    Q_ENUM(HardwareType)
    void setImFilterLength(int, int);
    void setImWeightingLimit(int, int);
    void setImSensitivity(int, double);
    void setImCanId(int, int);
    void setImPgaGain(int, int);

signals:
    void setSzobtePort(QString);
    void szobtePortChanged(QString);
    void setImPort(QString);
    void setImComPort(QString);
    void imPortChanged(QString);
    void imComPortChanged(QString);
    void setIntuitionPort(QString);
    void intuitionPortChanged(QString);
    void newData(HardwareType, qint64, int, double);
    void newAverageData(HardwareType, qint64, int, double);
    void zero();
    void imNeedUpdateActiveChansList();
    void szobteNeedUpdateActiveChansList();
    void tenzoMIpAddrChanged(QString);
    void tenzoMPortChanged(int);
    void tenzoMBigBagsWeightChanged(double);
    void commonCorrectionFactorChanged(double);
    void imSetListenOnly(bool);
    void imSetSaveToLog(bool);

public slots:
    void onSzobtePortChanged(QString);
    void onImPortChanged(QString);
    void onImComPortChanged(QString);
    void onIntuitionPortChanged(QString);
    void onSzobteNeedUpdateActiveChans();
    void onSzobteSoftwareCorrectionFactorChanged(int, qreal);
    void onImSoftwareCorrectionFactorChanged(int, qreal);
    void onImFilterLengthChanged(int, int);
    void onImWeightingLimitChanged(int, int);
    void onImSensitivityChanged(int, double);
    void onImCanIdChanged(int, int);
    void onImPgaGainChanged(int, int);
    void onImNeedUpdateActiveChans();
    void onImListenOnlyChanged(bool);
    void onImSaveToLogChanged(bool);

private slots:
    void onSzobteNewData(qint64, int, double);
    void onIntuitionNewData(qint64, double);
    void onTimer();
    void onZero();
    void onTenzoMBigBagsWeightChanged(double);

private:
    QString m_szobtePort;
    QString m_imPort;
    QString m_imComPort;
    QString m_intuitionPort;
    SzobteAmplifier* m_szobteAmplifier;
    IMAmplifier* m_imAmplifier;
    IM2Amplifier* m_im2Amplifier;
    IntuitionAmplifier* m_intuitionAmplifier;
    TenzoMScales* m_tenzoMScales;
    QThread* m_szobteThread;
    QThread* m_imThread;
    QThread* m_im2Thread;
    QThread* m_intuitionThread;
    QTimer m_timer;
    double m_szobteZero[10];
    double m_intuitionZero;
    double m_imZero[12];
    double m_im2Zero;
    double m_tenzoMZero;
    double m_tenzoMBigBagsWeight;
    double m_szobteSoftwareCorrectionFactors[10];
    double m_imSoftwareCorrectionFactors[12];
    double m_commonCorrectionFactor;
};

#endif // HARDWAREMANAGER_H
