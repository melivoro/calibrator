import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15
import QtQml.Models 2.15
import Qt.labs.calendar 1.0

ApplicationWindow {
    id: window
    property bool hamburgerButtonEnabled: true
    width: 1120
    height: 630
    minimumWidth: 800
    minimumHeight: 600
    visible: true
    title: qsTr("Calibrator")
    header: Toolbar {
        hamburgerButtonEnabled: window.hamburgerButtonEnabled
        enableButtons: sidePanel.currentIndex == 1
        toolbarHeight: 30
        backgroundColor: Material.primary
        hamburgerButtonColor: Material.accent
        text: sidePanel.currentText
        onHamburgerButtonClicked: {
            sidePanel.open()
        }
        onNewClicked: {
            resultsModel.clear()
            softwareConfiguration.openFromDb = false
            softwareConfiguration.blockedConfiguration = false
            softwareSettings.updateCalibratedChannelSet()
            hamburgerButtonEnabled = true
        }
        onOpenClicked: {
            dialog.open()
        }
        onSaveClicked: {
            saveDialog.open()
        }
    }
    SidePanel {
        id: sidePanel
        width: 0.2 * window.width
        height: window.height
        backgroundColor: Material.accent
    }
    TabBar {
        id: tabBar
        width: sidePanel.currentIndex < 1 ? 300 : 200
        contentHeight: 30
        Repeater {
            model: sidePanel.currentIndex < 1 ? [qsTr("Hardware"), qsTr(
                                                     "Software"), qsTr(
                                                     "Weighting")] : [qsTr("Calculation"), qsTr(
                                                                          "Chart")]
            TabButton {
                id: tabButton
                text: modelData
                font.capitalization: Font.MixedCase
                font.pointSize: 10
                enabled: {
                    if (sidePanel.currentIndex < 1) {
                        if (index < 2) {
                            if(softwareConfiguration !== null) {
                                if (softwareConfiguration.blockedConfiguration) {
                                    return false
                                }
                            }
                        }
                    }
                    return true
                }
                contentItem: Label {
                    text: tabButton.text
                    font: tabButton.font
                    color: index == tabBar.currentIndex ? "black" : "gray"
                    verticalAlignment: Qt.AlignVCenter
                }
            }
        }
        onCurrentIndexChanged: {
            if (sidePanel.currentIndex < 1) {
                if (softwareConfiguration.blockedConfiguration) {
                    tabBar.currentIndex = 2
                }
                stackLayout.currentIndex = tabBar.currentIndex
            } else {
                stackLayout.currentIndex = tabBar.currentIndex + 3
            }
        }
    }
    StackLayout {
        id: stackLayout
        anchors {
            fill: parent
            topMargin: tabBar.height
        }
        HardwareSettings {
            listViewWidth: 0.2 * window.width
        }
        SoftwareSettings {
            id: softwareSettings
            onGoToHardwareSettings: {
                tabBar.currentIndex = 0
            }
            onCalibratedChannelSetChanged: {
                if (resultsModel !== null) {
                    resultsModel.calibratedChannelSet = chans
                }
                timeChartView.isSzobteChan1Calibrated = chans[0]
                timeChartView.isSzobteChan2Calibrated = chans[1]
                timeChartView.isSzobteChan3Calibrated = chans[2]
                timeChartView.isSzobteChan4Calibrated = chans[3]
                timeChartView.isSzobteChan5Calibrated = chans[4]
                timeChartView.isSzobteChan6Calibrated = chans[5]
                timeChartView.isSzobteChan7Calibrated = chans[6]
                timeChartView.isSzobteChan8Calibrated = chans[7]
                timeChartView.isSzobteChan9Calibrated = chans[8]
                timeChartView.isSzobteChan10Calibrated = chans[9]
                timeChartView.isImChan1Calibrated = chans[10]
                timeChartView.isImChan2Calibrated = chans[11]
                timeChartView.isImChan3Calibrated = chans[12]
                timeChartView.isImChan4Calibrated = chans[13]
                timeChartView.isImChan5Calibrated = chans[14]
                timeChartView.isImChan6Calibrated = chans[15]
                timeChartView.isImChan7Calibrated = chans[16]
                timeChartView.isImChan8Calibrated = chans[17]
                timeChartView.isImChan9Calibrated = chans[18]
                timeChartView.isImChan10Calibrated = chans[19]
                timeChartView.isImChan11Calibrated = chans[20]
                timeChartView.isImChan12Calibrated = chans[21]
            }
            onReferenceChannelChanged: {
                if (resultsModel !== null) {
                    resultsModel.referenceChannel = chan
                }
                timeChartView.isSzobteChan1Reference = false
                timeChartView.isSzobteChan2Reference = false
                timeChartView.isSzobteChan3Reference = false
                timeChartView.isSzobteChan4Reference = false
                timeChartView.isSzobteChan5Reference = false
                timeChartView.isSzobteChan6Reference = false
                timeChartView.isSzobteChan7Reference = false
                timeChartView.isSzobteChan8Reference = false
                timeChartView.isSzobteChan9Reference = false
                timeChartView.isSzobteChan10Reference = false
                timeChartView.isIntuitionReference = false
                timeChartView.isTenzoMReference = false
                switch (chan) {
                case 1:
                    timeChartView.isSzobteChan1Reference = true
                    break
                case 2:
                    timeChartView.isSzobteChan2Reference = true
                    break
                case 3:
                    timeChartView.isSzobteChan3Reference = true
                    break
                case 4:
                    timeChartView.isSzobteChan4Reference = true
                    break
                case 5:
                    timeChartView.isSzobteChan5Reference = true
                    break
                case 6:
                    timeChartView.isSzobteChan6Reference = true
                    break
                case 7:
                    timeChartView.isSzobteChan7Reference = true
                    break
                case 8:
                    timeChartView.isSzobteChan8Reference = true
                    break
                case 9:
                    timeChartView.isSzobteChan9Reference = true
                    break
                case 10:
                    timeChartView.isSzobteChan10Reference = true
                    break
                case 11:
                    timeChartView.isIntuitionReference = true
                    break
                case 24:
                    timeChartView.isTenzoMReference = true
                }
            }
        }
        TimeChartView {
            id: timeChartView
        }
        ResultsTableView {}
        ResultsChartView {}
    }
    Connections {
        target: hardwareManager
        function onNewAverageData(h, t, ch, data) {
            data = Number(data).toFixed(1)
            if (h === 0) {
                switch (ch) {
                case 1:
                    timeChartView.szobteChan1Value = data
                    timeChartView.appendSzobteChan1(t, data)
                    break
                case 2:
                    timeChartView.szobteChan2Value = data
                    timeChartView.appendSzobteChan2(t, data)
                    break
                case 3:
                    timeChartView.szobteChan3Value = data
                    timeChartView.appendSzobteChan3(t, data)
                    break
                case 4:
                    timeChartView.szobteChan4Value = data
                    timeChartView.appendSzobteChan4(t, data)
                    break
                case 5:
                    timeChartView.szobteChan5Value = data
                    timeChartView.appendSzobteChan5(t, data)
                    break
                case 6:
                    timeChartView.szobteChan6Value = data
                    timeChartView.appendSzobteChan6(t, data)
                    break
                case 7:
                    timeChartView.szobteChan7Value = data
                    timeChartView.appendSzobteChan7(t, data)
                    break
                case 8:
                    timeChartView.szobteChan8Value = data
                    timeChartView.appendSzobteChan8(t, data)
                    break
                case 9:
                    timeChartView.szobteChan9Value = data
                    timeChartView.appendSzobteChan9(t, data)
                    break
                case 10:
                    timeChartView.szobteChan10Value = data
                    timeChartView.appendSzobteChan10(t, data)
                }
            }
            if (h === 1) {
                timeChartView.intuitionValue = data
                timeChartView.appendIntuition(t, data)
            }
            if(h === 2) {
                switch(ch) {
                case 1:
                    timeChartView.imChan1Value = data
                    timeChartView.appendImChan1(t, data)
                    break
                case 2:
                    timeChartView.imChan2Value = data
                    timeChartView.appendImChan2(t, data)
                    break
                case 3:
                    timeChartView.imChan3Value = data
                    timeChartView.appendImChan3(t, data)
                    break
                case 4:
                    timeChartView.imChan4Value = data
                    timeChartView.appendImChan4(t, data)
                    break
                case 5:
                    timeChartView.imChan5Value = data
                    timeChartView.appendImChan5(t, data)
                    break
                case 6:
                    timeChartView.imChan6Value = data
                    timeChartView.appendImChan6(t, data)
                    break
                case 7:
                    timeChartView.imChan7Value = data
                    timeChartView.appendImChan7(t, data)
                    break
                case 8:
                    timeChartView.imChan8Value = data
                    timeChartView.appendImChan8(t, data)
                    break
                case 9:
                    timeChartView.imChan9Value = data
                    timeChartView.appendImChan9(t, data)
                    break
                case 10:
                    timeChartView.imChan10Value = data
                    timeChartView.appendImChan10(t, data)
                    break
                case 11:
                    timeChartView.imChan11Value = data
                    timeChartView.appendImChan11(t, data)
                    break
                case 12:
                    timeChartView.imChan12Value = data
                    timeChartView.appendImChan12(t, data)
                    break
                case 13:
                    timeChartView.imValue = data
                    timeChartView.appendIm(t, data)
                }
            }
            if( h === 3) {
                timeChartView.tenzoMValue = data
                timeChartView.appendTenzoM(t, data)
            }
        }
    }
    Dialog {
        id: dialog
        anchors.centerIn: parent
        standardButtons: Dialog.Ok | Dialog.Cancel
        modal: true
        GridLayout {
            anchors.fill: parent
            columns: 2
            ComboBox {
                model: ["2019", "2020", "2021", "2022", "2023"]
                onCurrentTextChanged: {
                    monthGrid.year = currentText
                }
                Component.onCompleted: {
                    var currentYear = new Date().getFullYear()
                    currentIndex = currentYear - 2019
                }
            }
            ComboBox {
                model: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                onCurrentIndexChanged: {
                    monthGrid.month = currentIndex
                }
                Component.onCompleted: {
                    var currentMonth = new Date().getMonth()
                    currentIndex = currentMonth
                }
            }
            DayOfWeekRow {
                locale: monthGrid.locale
                Layout.columnSpan: 2
                Layout.fillWidth: true
            }
            MonthGrid {
                id: monthGrid
                property var currentDate
                locale: Qt.locale("ru_RU")
                Layout.columnSpan: 2
                Layout.fillWidth: true
                delegate: Label {
                    property bool selected: {
                        if(model.date === undefined || monthGrid.currentDate === undefined) {
                            return false
                        }
                        return model.date.getTime() === monthGrid.currentDate.getTime()
                    }
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: model.day
                    color: selected ? "white" : "black"
                    background: Rectangle {
                        color: selected ? Material.accent : "transparent"
                    }
                    onVisibleChanged: {
                        if(visible && model.today) {
                            monthGrid.currentDate = model.date
                            resultIndexComboBox.updateModel(monthGrid.currentDate)
                        }
                    }
                }
                onClicked: {
                    currentDate = date
                    resultIndexComboBox.updateModel(currentDate)
                }
            }
            ComboBox {
                id: resultIndexComboBox
                Layout.columnSpan: 2
                Layout.fillWidth: true
                function updateModel(date) {
                    resultIndexComboBox.model = dbworker.getMeasurementsByDate(date)
                }
                onCurrentIndexChanged: {
                    commentLabel.text = dbworker.getComment(monthGrid.currentDate, currentIndex + 1)
                }
            }
            Label {
                id: commentLabel
            }
        }
        onAccepted: {
            if(resultIndexComboBox.currentIndex == -1) {
                return;
            }
            resultsModel.clear()
            softwareConfiguration.openFromDb = false
            softwareConfiguration.blockedConfiguration = false
            softwareConfiguration.openFromDb = true
            softwareConfiguration.blockedConfiguration = true
            sidePanel.setCurrentIndex(1)
            hamburgerButtonEnabled = false
            var experimentId = softwareConfiguration.setDataFromDb(monthGrid.currentDate, resultIndexComboBox.currentIndex)
        }
    }
    Dialog {
        id: saveDialog
        anchors.centerIn: parent
        standardButtons: Dialog.Ok | Dialog.Cancel
        modal: true
        TextField {
            id: commentTextField
            implicitWidth: 300
        }
        onAccepted: {
            softwareConfiguration.saveData(commentTextField.text)
        }
    }
    Connections {
        target: dbworker
        function onIntuitionStateChanged(state) {
            timeChartView.recreateLineSeries()
        }
    }
    Connections {
        target: dbworker
        function onTenzoMStateChanged(state) {
            timeChartView.recreateLineSeries()
        }
    }

    function szobteUpdateActiveChansList() {
        timeChartView.recreateLineSeries()
        var activeChans = []
        for (var i = 1; i <= 10; i++) {
            if (dbworker.szobteGetChannelState(i)) {
                activeChans.push(i)
            }
        }
        hardwareManager.szobteSetActiveChans(activeChans)
    }
    function imUpdateActiveChansList() {
        timeChartView.recreateLineSeries()
        var activeChans = []
        for (var i = 1; i <= 12; i++) {
            if (dbworker.imGetChannelState(i)) {
                activeChans.push(i)
            }
        }
        hardwareManager.imSetActiveChans(activeChans)
    }
    Connections {
        target: hardwareManager
        function onSzobteNeedUpdateActiveChansList() {
            szobteUpdateActiveChansList()
        }
    }
    Connections {
        target: hardwareManager
        function onImNeedUpdateActiveChansList() {
            imUpdateActiveChansList()
        }
    }
    Component.onCompleted: {
        szobteUpdateActiveChansList()
        imUpdateActiveChansList()
    }
}
