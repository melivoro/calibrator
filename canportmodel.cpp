#include "canportmodel.h"

CanPort::CanPort(const QString& name, const QString& description): m_name(name), m_description(description) {
}

QString CanPort::getName() const {
    return m_name;
}

QString CanPort::getDescription() const {
    return m_description;
}

CanPortModel::CanPortModel(QObject *parent): QAbstractListModel(parent) {
    canInitializeLibrary();
    reload();
}

void CanPortModel::reload() {
    beginResetModel();
    canports.clear();
    int c, i;
    canStatus stat = canGetNumberOfChannels(&c);
    if(canOK == stat) {
        for(i = 0; i < c; i++) {
            char tmp[255];
            stat = canGetChannelData(i, canCHANNELDATA_DEVDESCR_ASCII, &tmp, sizeof(tmp));
            if(stat < 0) {
                char buf[128];
                canGetErrorText(stat, buf, 128);
                qDebug() << "CanPortModel::reload(): canGetChannelData error:" << buf;
            } else {
                QString name = QString("CAN%1").arg(i);
                QString description = QString(tmp);
                //qDebug() << "CanPortModel::reload(): canGetChannelData:" << name << description;
                addCanPort(CanPort(name, description));
            }
        }
    } else {
        char buf[128];
        canGetErrorText(stat,buf,128);
        qDebug() << "CanPortModel::reload(): canGetNumberOfChannels error:" << buf;
    }
    endResetModel();
}

QVariant CanPortModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() > canports.count()) {
        return QVariant();
    }
    const CanPort & cp = canports[index.row()];
    if (role == NameRole) {
        return cp.getName();
    } else if (role == DescriptionRole) {
        return cp.getDescription();
    }
    return QVariant();
}

int CanPortModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return canports.size();
}

QHash<int, QByteArray> CanPortModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[DescriptionRole] = "description";
    return roles;
}

void CanPortModel::addCanPort(CanPort cp) {
    canports << cp;
}
