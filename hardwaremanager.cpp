#include "hardwaremanager.h"

HardwareManager::HardwareManager(QObject *parent) : QObject(parent)
{
    for(int i = 0; i < 10; i++) {
        m_szobteZero[i] = 0;
        m_imZero[i] = 0;
        m_szobteSoftwareCorrectionFactors[i] = 1;
        m_imSoftwareCorrectionFactors[i] = 1;
    }
    m_imZero[10] = 0;
    m_imZero[11] = 0;
    m_imSoftwareCorrectionFactors[10] = 1;
    m_imSoftwareCorrectionFactors[11] = 1;
    m_commonCorrectionFactor = 1;
    m_intuitionZero = 0;
    m_tenzoMZero = 0;
    m_szobteThread = new QThread;
    m_szobteAmplifier = new SzobteAmplifier;
    m_szobteAmplifier->moveToThread(m_szobteThread);
    m_imThread = new QThread;
    m_imAmplifier = new IMAmplifier;
    m_imAmplifier->moveToThread(m_imThread);
    m_im2Thread = new QThread;
    m_im2Amplifier = new IM2Amplifier;
    m_im2Amplifier->moveToThread(m_im2Thread);
    m_intuitionThread = new QThread;
    m_intuitionAmplifier = new IntuitionAmplifier;
    m_intuitionAmplifier->moveToThread(m_intuitionThread);
    m_tenzoMScales = new TenzoMScales;
    connect(m_szobteThread, &QThread::finished, m_szobteThread, &QThread::deleteLater);
    connect(m_szobteThread, &QThread::finished, m_szobteAmplifier, &SerialPort::deleteLater);
    connect(m_imThread, &QThread::started, m_imAmplifier, &IMAmplifier::run);
    connect(m_imAmplifier, &IMAmplifier::finished, m_imThread, &QThread::terminate);
    connect(m_intuitionThread, &QThread::finished, m_intuitionThread, &QThread::deleteLater);
    connect(m_intuitionThread, &QThread::finished, m_intuitionAmplifier, &SerialPort::deleteLater);
    connect(this, &HardwareManager::setSzobtePort, m_szobteAmplifier, &SzobteAmplifier::setPort);
    connect(m_szobteAmplifier, &SzobteAmplifier::portChanged, this, &HardwareManager::onSzobtePortChanged);
    connect(m_szobteAmplifier, &SzobteAmplifier::newData, this, &HardwareManager::onSzobteNewData);
    connect(this, &HardwareManager::setImPort, m_imAmplifier, &IMAmplifier::setPort);
    connect(this, &HardwareManager::setImComPort, m_im2Amplifier, &IM2Amplifier::setPort);
    connect(m_imAmplifier, &IMAmplifier::portChanged, this, &HardwareManager::onImPortChanged);
    connect(m_im2Amplifier, &IM2Amplifier::portChanged, this, &HardwareManager::onImComPortChanged);
    connect(this, &HardwareManager::setIntuitionPort, m_intuitionAmplifier, &IntuitionAmplifier::setPort);
    connect(m_intuitionAmplifier, &IntuitionAmplifier::portChanged, this, &HardwareManager::onIntuitionPortChanged);
    connect(m_intuitionAmplifier, &IntuitionAmplifier::newData, this, &HardwareManager::onIntuitionNewData);
    connect(&m_timer, &QTimer::timeout, this, &HardwareManager::onTimer);
    connect(this, &HardwareManager::zero, this, &HardwareManager::onZero);
    connect(this, &HardwareManager::tenzoMIpAddrChanged, m_tenzoMScales, &TenzoMScales::onIpAddressChanged);
    connect(this, &HardwareManager::tenzoMPortChanged, m_tenzoMScales, &TenzoMScales::onPortChanged);
    connect(this, &HardwareManager::tenzoMBigBagsWeightChanged, this, &HardwareManager::onTenzoMBigBagsWeightChanged);
    connect(this, &HardwareManager::imSetListenOnly, this, &HardwareManager::onImListenOnlyChanged);
    connect(this, &HardwareManager::imSetSaveToLog, this, &HardwareManager::onImSaveToLogChanged);
    connect(m_imAmplifier, &IMAmplifier::measuringStarted, m_im2Amplifier, &IM2Amplifier::onMeasuringStarted);
    connect(m_imAmplifier, &IMAmplifier::portChanged, m_im2Amplifier, &IM2Amplifier::onCanPortChanged);
    m_timer.setInterval(300);
    m_timer.start();
    m_szobteThread->start();
    m_imThread->start();
    m_im2Thread->start();
    m_intuitionThread->start();
}

HardwareManager::~HardwareManager() {
    delete m_tenzoMScales;
}

void HardwareManager::onSzobtePortChanged(QString port) {
    m_szobtePort = port;
    emit szobtePortChanged(m_szobtePort);
}

void HardwareManager::onImPortChanged(QString port) {
    m_imPort = port;
    emit imPortChanged(m_imPort);
}

void HardwareManager::onImComPortChanged(QString port)
{
    m_imComPort = port;
    emit imComPortChanged(m_imComPort);
}

void HardwareManager::onIntuitionPortChanged(QString port) {
    m_intuitionPort = port;
    emit intuitionPortChanged(m_intuitionPort);
}

void HardwareManager::szobteSetActiveChans(QList<int> chans) {
    emit m_szobteAmplifier->setActiveChans(chans);
}

void HardwareManager::imSetActiveChans(QList<int> chans) {
    emit m_imAmplifier->setActiveChans(chans);
}

void HardwareManager::setImFilterLength(int chan, int value)
{
    if((chan < 1) || (chan > 12)) {
        return;
    }
    m_imAmplifier->setFilterLength(chan, value);
}

void HardwareManager::setImWeightingLimit(int chan, int value)
{
    if((chan < 1) || (chan > 12)) {
        return;
    }
    m_imAmplifier->setWeightingLimit(chan, value);
}

void HardwareManager::setImSensitivity(int chan, double value)
{
    if((chan < 1) || (chan > 12)) {
        return;
    }
    m_imAmplifier->setSensitivity(chan, value);
}

void HardwareManager::setImCanId(int amp, int value)
{
    if((amp < 1) || (amp > 6)) {
        return;
    }
    m_imAmplifier->setCanId(amp, value);
}

void HardwareManager::setImPgaGain(int amp, int value)
{
    if((amp < 1) || (amp > 6)) {
        return;
    }
    m_imAmplifier->setPgaGain(amp, value);
}

void HardwareManager::onSzobteNewData(qint64 t, int ch, double data) {
    emit newData(SZOBTE, t, ch, data - m_szobteZero[ch-1]);
}

void HardwareManager::onIntuitionNewData(qint64 t, double data) {
    emit newData(Intuition, t, 1, data - m_intuitionZero);
}

void HardwareManager::onTimer() {
    qint64 t = QDateTime::currentDateTime().toMSecsSinceEpoch();
    if(m_szobteAmplifier->isOpen()) {
        for(int i = 1; i <= 10; i++) {
            emit newAverageData(SZOBTE, t, i, (m_szobteAmplifier->getAverage(i) - m_szobteZero[i-1]) / (m_szobteSoftwareCorrectionFactors[i-1] * m_commonCorrectionFactor));
        }
    }
    if(m_intuitionAmplifier->isOpen()) {
        emit newAverageData(Intuition, t, 1, m_intuitionAmplifier->getAverage() - m_intuitionZero);
    }
    if(m_tenzoMScales->running()) {
        emit newAverageData(TenzoM, t, 1, m_tenzoMScales->getAverage() - m_tenzoMZero);
    }
    if(m_imAmplifier->isOpen()) {
        for(int i = 1; i <= 12; i++) {
            emit newAverageData(IM, t, i, (m_imAmplifier->getAverage(i, m_imAmplifier->getFilterLength(i)) - m_imZero[i-1]) / (m_imSoftwareCorrectionFactors[i-1] * m_commonCorrectionFactor));
        }
    }
    if(m_im2Amplifier->isOpen()) {
        emit newAverageData(IM, t, 13, (m_im2Amplifier->getAverage() - m_im2Zero) / m_commonCorrectionFactor);
    }
}

void HardwareManager::onZero() {
    for(int i = 1; i <= 10; i++) {
        m_szobteZero[i-1] = m_szobteAmplifier->getAverage(i);
        m_imZero[i-1] = m_imAmplifier->getAverage(i);
    }
    m_imZero[10] = m_imAmplifier->getAverage(11);
    m_imZero[11] = m_imAmplifier->getAverage(12);
    m_im2Zero = m_im2Amplifier->getAverage();
    m_intuitionZero = m_intuitionAmplifier->getAverage();
    m_tenzoMZero = m_tenzoMScales->getAverage();
}

void HardwareManager::onTenzoMBigBagsWeightChanged(double weight)
{
    m_tenzoMBigBagsWeight = weight;
}

void HardwareManager::onImListenOnlyChanged(bool listenOnly)
{
    m_imAmplifier->setListenOnly(listenOnly);
}

void HardwareManager::onImSaveToLogChanged(bool saveToLog)
{
    m_imAmplifier->setSaveToLog(saveToLog);
    m_im2Amplifier->setSaveToLog(saveToLog);
}

void HardwareManager::onSzobteSoftwareCorrectionFactorChanged(int chan, qreal softwareCorrectionFactor)
{
    m_szobteSoftwareCorrectionFactors[chan-1] = softwareCorrectionFactor;
}

void HardwareManager::onImSoftwareCorrectionFactorChanged(int chan, qreal softwareCorrectionFactor)
{
    m_imSoftwareCorrectionFactors[chan-1] = softwareCorrectionFactor;
}

void HardwareManager::onImFilterLengthChanged(int chan, int value)
{
    setImFilterLength(chan, value);
    qDebug() << "HardwareManager::onImFilterLengthChanged";
    for(int i = 1; i <= 12; i++) {
        qDebug() << i << m_imAmplifier->getFilterLength(i);
    }
    qDebug() << "================";
}

void HardwareManager::onImWeightingLimitChanged(int chan, int value)
{
    setImWeightingLimit(chan, value);
    qDebug() << "HardwareManager::onImWeightingLimitChanged";
    for(int i = 1; i <= 12; i++) {
        qDebug() << i << m_imAmplifier->getWeightingLimit(i);
    }
    qDebug() << "================";
}

void HardwareManager::onImSensitivityChanged(int chan, double value)
{
    setImSensitivity(chan, value);
    qDebug() << "HardwareManager::onImSensitivityChanged";
    for(int i = 1; i <= 12; i++) {
        qDebug() << i << m_imAmplifier->getSensitivity(i);
    }
    qDebug() << "================";
}

void HardwareManager::onImCanIdChanged(int amp, int value)
{
    setImCanId(amp, value);
    qDebug() << "HardwareManager::onImCanIdChanged";
    for(int i = 1; i <= 6; i++) {
        qDebug() << i << m_imAmplifier->getCanId(i);
    }
    qDebug() << "================";
}

void HardwareManager::onImPgaGainChanged(int amp, int value)
{
    setImPgaGain(amp, value);
    qDebug() << "HardwareManager::onImPgaGainChanged";
    for(int i = 1; i <= 6; i++) {
        qDebug() << i << m_imAmplifier->getPgaGain(i);
    }
    qDebug() << "================";
}

void HardwareManager::onImNeedUpdateActiveChans()
{
    emit imNeedUpdateActiveChansList();
}

void HardwareManager::onSzobteNeedUpdateActiveChans() {
    emit szobteNeedUpdateActiveChansList();
}
