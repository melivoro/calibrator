import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQml.Models 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

Item {
    GridLayout {
        anchors.fill: parent
        columns: 2
        Label {
            font.pointSize: 10
            text: qsTr("Canport:")
        }
        ComboBox {
            id: imCanPortComboBox
            Layout.fillWidth: true
            Layout.preferredHeight: 35
            model: DelegateModel {
                id: imDelegateModel
                model: canPortModel
                delegate: ItemDelegate {
                    text: name + " (" + description + ")"
                }
            }
            onDisplayTextChanged: {
                dbworker.imPort = displayText
            }
            onActivated: {
                displayText = imDelegateModel.items.get(
                            currentIndex).model.name
                hardwareManager.imPort = displayText
            }
            Connections {
                target: hardwareManager
                function onImPortChanged(port) {
                    if (port === "") {
                        imCanPortComboBox.displayText = ""
                    }
                }
            }
            Component.onCompleted: {
                for(var i = 0; i < canPortModel.rowCount(); i++) {
                    var port = dbworker.imPort
                    if(port === imDelegateModel.items.get(i).model.name) {
                        displayText = port
                        hardwareManager.imPort = port
                        break
                    }
                }
            }
        }
        Label {
            font.pointSize: 10
            text: qsTr("Comport:")
        }
        ComboBox {
            id: imComPortComboBox
            Layout.fillWidth: true
            Layout.preferredHeight: 35
            model: DelegateModel {
                id: imComPortDelegateModel
                model: comPortModel
                delegate: ItemDelegate {
                    text: name + " (" + description + ")"
                }
            }
            onDisplayTextChanged: {
                dbworker.imComPort = displayText
            }
            onActivated: {
                displayText = imComPortDelegateModel.items.get(
                            currentIndex).model.name
                hardwareManager.imComPort = displayText
            }
            Connections {
                target: hardwareManager
                function onImComPortChanged(port) {
                    if (port === "") {
                        imComPortComboBox.displayText = ""
                    }
                }
            }
            Component.onCompleted: {
                for(var i = 0; i < comPortModel.rowCount(); i++) {
                    var port = dbworker.imComPort
                    if(port === imComPortDelegateModel.items.get(i).model.name) {
                        displayText = port
                        hardwareManager.imComPort = displayText
                        break
                    }
                }
            }
        }
        Label {
            font.pointSize: 10
            text: qsTr("Listen only:")
        }
        CheckBox {
            onToggled: {
                dbworker.imListenOnly = checked
            }
            Component.onCompleted: {
                checked = dbworker.imListenOnly
                hardwareManager.imSetListenOnly(checked)
            }
        }
        Label {
            font.pointSize: 10
            text: qsTr("Save to log:")
        }
        CheckBox {
            onToggled: {
                dbworker.imSaveToLog = checked
            }
            Component.onCompleted: {
                checked = dbworker.imSaveToLog
                hardwareManager.imSetSaveToLog(checked)
            }
        }
        Rectangle {
            Layout.columnSpan: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: Material.primary
            RowLayout {
                anchors {
                    fill: parent
                }
                ToolButton {
                    icon.source: "images/left.png"
                    Layout.preferredWidth: 25
                    Layout.preferredHeight: 25
                    Layout.alignment: Qt.AlignTop
                    Layout.topMargin: 10
                    enabled: imChanSettingsListView.currentIndex != 0
                    onClicked: {
                        if (imChanSettingsListView.currentIndex > 0) {
                            imChanSettingsListView.currentIndex--
                        }
                    }
                }
                ListView {
                    id: imChanSettingsListView
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    clip: true
                    contentHeight: height
                    contentWidth: width
                    orientation: ListView.Horizontal
                    snapMode: ListView.SnapToItem
                    interactive: false
                    model: 12
                    delegate: ScrollView {
                        id: imChanSettingsDelegate
                        signal indexChanged
                        property int amplifierNumber
                        property int channelNumber
                        onIndexChanged: {
                            channelNumber = imChanSettingsListView.currentIndex + 1
                            amplifierNumber = Math.ceil(channelNumber/2)
                            serialNumberTextField.text = dbworker.imGetSerialNumber(amplifierNumber)
                            canIdSpinBox.value = dbworker.imGetCanId(amplifierNumber)
                            filterLengthSpinBox.value = dbworker.imGetFilterLength(channelNumber)
                            weightingLimitSpinBox.value = dbworker.imGetChannelWeightingLimit(channelNumber)
                            sensitivityDoubleSpinBox.value = Math.round(dbworker.imGetChannelSensitivity(channelNumber)*1000000)
                            channelStateSwitch.checked = dbworker.imGetChannelState(channelNumber)
                            sensorModelIndexComboBox.currentIndex = dbworker.imGetSensorModelIndex(channelNumber)
                            sensorSerialNumberTextField.text = dbworker.imGetSensorSerialNumber(channelNumber)
                            var pgaGain = dbworker.imGetPgaGain(amplifierNumber)
                            switch(pgaGain) {
                            case 0:
                            case 1:
                                pgaGainComboBox.currentIndex = 0
                                break
                            case 64:
                                pgaGainComboBox.currentIndex = 1
                                break
                            case 128:
                                pgaGainComboBox.currentIndex = 2
                                break
                            }
                        }
                        width: imChanSettingsListView.width
                        height: imChanSettingsListView.height
                        GridLayout {
                            anchors {
                                fill: parent
                                margins: 10
                            }
                            columns: 2
                            columnSpacing: 10
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("Channel %1").arg(index + 1)
                                font.pointSize: 15
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("%1 of %2").arg(index + 1).arg(12)
                                color: "gray"
                                font.pointSize: 10
                                Layout.columnSpan: 2
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("Strain amplifier %1").arg(imChanSettingsDelegate.amplifierNumber)
                                font {
                                    bold: true
                                    pointSize: 15
                                }
                                Layout.columnSpan: 2
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                font {
                                    bold: true
                                    pointSize: 12
                                }
                                text: qsTr("Settings per amplifier:")
                                Layout.columnSpan: 2
                            }
                            Item {
                                height: 5
                                Layout.columnSpan: 2
                            }
                            Label {
                                font.pointSize: 10
                                text: qsTr("Serial number:")
                            }
                            Label {
                                font.pointSize: 10
                                text: qsTr("Can id:")
                            }


                            TextField {
                                id: serialNumberTextField
                                Layout.preferredWidth: 200
                                validator: RegExpValidator {
                                    regExp: /^\d{1,10}$|^$/
                                }
                                onEditingFinished: {
                                    if(text !== dbworker.imGetSerialNumber(amplifierNumber)) {
                                        dbworker.imSetSerialNumber(amplifierNumber, text)
                                    }
                                }
                            }
                            SpinBox {
                                id: canIdSpinBox
                                Layout.preferredWidth: 200
                                editable: true
                                from: 1
                                to: 255
                                onValueChanged: {
                                    if(value !== dbworker.imGetCanId(amplifierNumber)) {
                                        dbworker.imSetCanId(amplifierNumber, value)
                                    }
                                }
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("ADC PGA gain")
                                font.pointSize: 10
                                Layout.columnSpan: 2
                            }
                            ComboBox {
                                id: pgaGainComboBox
                                Layout.columnSpan: 2
                                Layout.preferredHeight: 35
                                Layout.preferredWidth: 200
                                model: [qsTr("PGA_GAIN_1"), qsTr("PGA_GAIN_64"), qsTr("PGA_GAIN_128")]
                                onActivated: {
                                    var value = currentText.replace("PGA_GAIN_", "")
                                    if(value !== dbworker.imGetPgaGain(amplifierNumber)) {
                                        dbworker.imSetPgaGain(amplifierNumber, value)
                                    }
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                font {
                                    bold: true
                                    pointSize: 12
                                }
                                text: qsTr("Settings per channel:")
                                Layout.columnSpan: 2
                            }
                            Item {
                                height: 5
                                Layout.columnSpan: 2
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Weighting limit")
                                font.pointSize: 10
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Sensitivity")
                                font.pointSize: 10
                            }


                            SpinBox {
                                id: weightingLimitSpinBox
                                Layout.preferredWidth: 200
                                editable: true
                                to: 100000
                                onValueChanged: {
                                    if(value !== dbworker.imGetChannelWeightingLimit(index+1)) {
                                        dbworker.imSetChannelWeightingLimit(index+1, value)
                                    }
                                }
                            }
                            DoubleSpinBox {
                                id: sensitivityDoubleSpinBox
                                Layout.preferredWidth: 200
                                editable: true
                                to: 10000000
                                onRealValueChanged: {
                                    if(value !== Math.round(dbworker.imGetChannelSensitivity(index+1)*1000000)) {
                                        dbworker.imSetChannelSensitivity(index+1, realValue)
                                    }
                                }
                            }

                            Label {
                                Layout.columnSpan: 2
                                text: qsTr("Filter length")
                                font.pointSize: 10
                            }

                            SpinBox {
                                id: filterLengthSpinBox
                                Layout.columnSpan: 2
                                Layout.preferredWidth: 200
                                editable: true
                                to: 100
                                onValueChanged: {
                                    if(value !== dbworker.imGetFilterLength(channelNumber)) {
                                        dbworker.imSetFilterLength(channelNumber, value)
                                    }
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("Sensor")
                                font {
                                    bold: true
                                    pointSize: 15
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Model")
                                font.pointSize: 10
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Serial number")
                                font.pointSize: 10
                            }
                            ComboBox {
                                id: sensorModelIndexComboBox
                                Layout.preferredHeight: 35
                                Layout.preferredWidth: 200
                                model: [qsTr("Suzhou LSZ-S03G-10t"), qsTr("Other")]
                                onActivated: {
                                    if(currentIndex !== dbworker.imGetSensorModelIndex(channelNumber)) {
                                        dbworker.imSetSensorModelIndex(channelNumber, currentIndex)
                                    }
                                }
                            }
                            TextField {
                                id: sensorSerialNumberTextField
                                Layout.preferredWidth: 200
                                validator: RegExpValidator {
                                    regExp: /^\d{1,10}$/
                                }
                                onTextChanged: {
                                    dbworker.imSetSensorSerialNumber(channelNumber, text)
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("Add channel to weighting")
                                font {
                                    bold: true
                                    pointSize: 10
                                }
                            }
                            Switch {
                                id: channelStateSwitch
                                text: checked ? qsTr("On"): qsTr("Off")        
                                onCheckedChanged: {
                                    if(checked !== dbworker.imGetChannelState(index+1)) {
                                        dbworker.imSetChannelState(index+1, checked)
                                    }
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                        }
                    }
                    onCurrentIndexChanged: {
                        positionViewAtIndex(currentIndex, ListView.Center)
                        itemAtIndex(currentIndex).indexChanged()
                    }
                }
                ToolButton {
                    icon.source: "images/right.png"
                    Layout.preferredWidth: 25
                    Layout.preferredHeight: 25
                    Layout.alignment: Qt.AlignTop
                    Layout.topMargin: 10
                    enabled: imChanSettingsListView.currentIndex != 11
                    onClicked: {
                        if (imChanSettingsListView.currentIndex < 11) {
                            imChanSettingsListView.currentIndex++
                        }
                    }
                }
            }
        }
    }
}
