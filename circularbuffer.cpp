#include "circularbuffer.h"

CircularBuffer::CircularBuffer(): i(0) {
    m_buf.resize(100);
    std::fill_n(m_buf.begin(), 100, 0);
}

void CircularBuffer::Add(double data) {
    m_buf[i++] = data;
    if(i == 100) {
        i = 0;
    }
}

double CircularBuffer::GetAverage(int num) {
    double res = 0;
    for(int j = 1; j <= num; j++) {
        if(i-j>=0) {
            res = res + m_buf[i-j];
        } else {
            res = res + m_buf[i-j+100];
        }
    }
    return res / num;
}
