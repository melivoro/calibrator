#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QSerialPort>

class SerialPort : public QSerialPort
{
    Q_OBJECT
public:
    explicit SerialPort(QObject *parent = nullptr);
    Q_PROPERTY(QString port MEMBER m_port WRITE setPort NOTIFY portChanged)

signals:
    void portChanged(QString);

public slots:
    void setPort(QString port);

private slots:
    virtual void onReadyRead();

protected:
    virtual void checkData(QString) = 0;
    QString m_port;
};

#endif // SERIALPORT_H
