#include "intuitionamplifier.h"

IntuitionAmplifier::IntuitionAmplifier(QObject *parent): SerialPort(parent) {
    setBaudRate(9600);
}

void IntuitionAmplifier::checkData(QString line) {
    double data;
    QRegExp rx1(".*G(.*) kg");
    rx1.indexIn(line);
    bool ok;
    data = rx1.capturedTexts()[1].toDouble(&ok);
    if(ok) {
        emit newData(QDateTime::currentDateTime().toMSecsSinceEpoch(), data);
        average.Add(data);
    }
}

double IntuitionAmplifier::getAverage() {
    return average.GetAverage(10);
}
