#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

#include "comportmodel.h"
#include "canportmodel.h"
#include "softwareconfiguration.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOrganizationName("Merkator");
    app.setOrganizationDomain("merkatorgroup.ru");
    QQuickStyle::setStyle("Material");
    QQmlApplicationEngine engine;
    ComPortModel comPortModel;
    CanPortModel canPortModel;
    SoftwareConfiguration softwareConfiguration;
    engine.rootContext()->setContextProperty("hardwareManager", softwareConfiguration.getHwManager());
    engine.rootContext()->setContextProperty("comPortModel", &comPortModel);
    engine.rootContext()->setContextProperty("canPortModel", &canPortModel);
    engine.rootContext()->setContextProperty("dbworker", softwareConfiguration.getDbWorker());
    engine.rootContext()->setContextProperty("resultsModel", softwareConfiguration.getResultsModel());
    engine.rootContext()->setContextProperty("softwareConfiguration", &softwareConfiguration);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
