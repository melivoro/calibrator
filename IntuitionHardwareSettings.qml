import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQml.Models 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

Item {
    GridLayout {
        anchors.fill: parent
        columns: 2
        Label {
            font.pointSize: 10
            text: qsTr("Comport:")
        }
        ComboBox {
            id: intuitionComPortComboBox
            Layout.fillWidth: true
            Layout.preferredHeight: 35
            model: DelegateModel {
                id: intuitionDelegateModel
                model: comPortModel
                delegate: ItemDelegate {
                    text: name + " (" + description + ")"
                }
            }
            onDisplayTextChanged: {
                dbworker.intuitionPort = displayText
            }
            onActivated: {
                displayText = intuitionDelegateModel.items.get(
                            currentIndex).model.name
                hardwareManager.intuitionPort = displayText
            }
            Connections {
                target: hardwareManager
                function onIntuitionPortChanged(port) {
                    if (port === "") {
                        intuitionComPortComboBox.displayText = ""
                    }
                }
            }
            Component.onCompleted: {
                for (var i = 0; i < comPortModel.rowCount(); i++) {
                    var port = dbworker.intuitionPort
                    if (port === intuitionDelegateModel.items.get(
                                i).model.name) {
                        displayText = port
                        hardwareManager.intuitionPort = displayText
                        break
                    }
                }
            }
        }
        Label {
            font.pointSize: 10
            text: qsTr("Serial number:")
        }
        TextField {
            Layout.fillWidth: true
            validator: RegExpValidator {
                regExp: /^\d{1,10}$/
            }
            onTextChanged: {
                dbworker.intuitionSerialNumber = text
            }
            Component.onCompleted: {
                text = dbworker.intuitionSerialNumber
            }
        }
        Rectangle {
            Layout.columnSpan: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: Material.primary
            GridLayout {
                anchors {
                    left: parent.left
                    top: parent.top
                    margins: 20
                }
                columns: 2
                columnSpacing: 10
                Label {
                    text: qsTr("Sensor")
                    font {
                        bold: true
                        pointSize: 15
                    }
                }
                Item {
                    height: 10
                    Layout.columnSpan: 2
                }
                Label {
                    Layout.fillWidth: true
                    text: qsTr("Model")
                    font.pointSize: 10
                }
                Label {
                    Layout.fillWidth: true
                    text: qsTr("Serial number")
                    font.pointSize: 10
                }
                ComboBox {
                    Layout.preferredHeight: 35
                    Layout.preferredWidth: 200
                    model: [qsTr("Suzhou LSZ-S03G-10t"), qsTr("Other")]
                    onActivated: {
                        if(currentIndex !== dbworker.intuitionGetSensorModelIndex()) {
                            dbworker.intuitionSetSensorModelIndex(currentIndex)
                        }
                    }
                    Component.onCompleted: {
                        currentIndex = dbworker.intuitionGetSensorModelIndex()
                    }
                }
                TextField {
                    Layout.preferredWidth: 200
                    validator: RegExpValidator {
                        regExp: /^\d{1,10}$/
                    }
                    onTextChanged: {
                        dbworker.intuitionSetSensorSerialNumber(text)
                    }
                    Component.onCompleted: {
                        text = dbworker.intuitionGetSensorSerialNumber()
                    }
                }
                Item {
                    height: 10
                    Layout.columnSpan: 2
                }
                Label {
                    text: qsTr("Add to weighting")
                    font {
                        bold: true
                        pointSize: 10
                    }
                }
                Switch {
                    text: checked ? qsTr("On"): qsTr("Off")
                    onCheckedChanged: {
                        if(checked !== dbworker.intuitionGetState()) {
                            dbworker.intuitionSetState(checked)
                        }
                    }

                    Component.onCompleted: {
                        checked = dbworker.intuitionGetState()
                    }
                }
            }
        }
    }
}
