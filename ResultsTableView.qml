import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

Item {
    ColumnLayout {
        anchors {
            fill: parent
            topMargin: 0
            margins: 20
        }
        ScrollView {
            Layout.fillWidth: true
            Layout.preferredHeight: 300
            TextArea {
                anchors.fill: parent
                font.pointSize: 12
                readOnly: true
                text: softwareConfiguration !== null ? softwareConfiguration.resultString: ""
            }
        }
        Row {
            Layout.fillWidth: true
            Repeater {
                id: headersRepeater
                model: resultsModel !== null ? resultsModel.headers: 1
                Label {
                    text: modelData
                    height: 30
                    width: 120
                    verticalAlignment: Qt.AlignVCenter
                    horizontalAlignment: Qt.AlignHCenter
                    font.pointSize: 12
                    background: Rectangle {
                        color: Material.primary
                    }
                }
            }
        }
        TableView {
            id: tableView
            Layout.fillWidth: true
            Layout.fillHeight: true
            boundsBehavior: Flickable.StopAtBounds
            clip: true
            model: resultsModel
            delegate: TextField {
                text: modelData
                readOnly: {
                    if(resultsModel == null) {
                        return false
                    }
                    if(column < resultsModel.columnCount() - 1) {
                        return false
                    }
                    return true
                }
                enabled: !readOnly
                font.pointSize: 10
                implicitWidth: 120
                horizontalAlignment: Qt.AlignHCenter
                onEditingFinished: {
                    resultsModel.setData(resultsModel.index(row, column), text)
                }
            }
            ScrollBar.vertical: ScrollBar {
                policy: ScrollBar.AsNeeded
            }
        }
    }
}
