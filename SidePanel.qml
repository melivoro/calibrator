import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

Drawer {
    property string backgroundColor
    property int currentIndex: selectModeListView.currentIndex
    property string currentText: selectModeModel.get(
                                     selectModeListView.currentIndex).txt
    Material.background: backgroundColor
    enter: Transition {
        NumberAnimation {
            duration: 500
        }
    }
    exit: Transition {
        NumberAnimation {
            duration: 500
        }
    }
    ListView {
        id: selectModeListView
        anchors {
            fill: parent
            margins: 10
            leftMargin: 0
        }
        spacing: 20
        delegate: Item {
            width: parent.width
            height: 20
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    selectModeListView.currentIndex = index
                    sidePanel.close()
                }
            }
            RowLayout {
                spacing: 10
                Rectangle {
                    Layout.preferredHeight: 20
                    Layout.preferredWidth: 5
                    color: index == selectModeListView.currentIndex ? "white" : "transparent"
                }
                Image {
                    Layout.preferredHeight: 20
                    Layout.preferredWidth: 20
                    source: img
                    fillMode: Image.PreserveAspectFit
                }
                Label {
                    Layout.fillWidth: true
                    text: txt
                    color: "white"
                    font.pointSize: 10
                }
            }
        }
        model: ListModel {
            id: selectModeModel
            ListElement {
                img: "images/m10s.png"
                txt: qsTr("Measurements")
            }
            ListElement {
                img: "images/result.png"
                txt: qsTr("Result")
            }
        }
    }
    function setCurrentIndex(index) {
        selectModeListView.currentIndex = index
    }
}
