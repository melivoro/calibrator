#include "tenzomscales.h"

TenzoMScales::TenzoMScales() {
    connect(this, &QTcpSocket::stateChanged, this, &TenzoMScales::onStateChanged);
    connect(this, &QTcpSocket::readyRead, this, &TenzoMScales::onReadyRead);
    connect(&m_timer, &QTimer::timeout, this, &TenzoMScales::requestData);
    setSocketOption(QAbstractSocket::KeepAliveOption, true);
    m_timer.setInterval(100);
    m_timer.start();
}

// https://www.tenso-m.ru/pdf/vks.pdf
unsigned char TenzoMScales::tenzoMCrc(unsigned char *InputData, unsigned char BytesNumber, unsigned char Offset) {
    unsigned char Data, CrcCode = 0, Polinom = 0x69;
    for(int i = Offset; i < BytesNumber + Offset; i++) {
        Data = InputData[i];
        for(int j = 0; j < 8; j++) {
            if(CrcCode & (1 << 7)) {
                CrcCode *= 2;
                if(Data & (1 << 7)) {
                    CrcCode++;
                }
                CrcCode ^= Polinom;
            } else {
                CrcCode *= 2;
                if(Data & (1 << 7)) {
                    CrcCode++;
                }
            }
            Data *= 2;
        }
    }
    return CrcCode;
}

QByteArray TenzoMScales::createCmdBuffer(unsigned char addr, unsigned char cop) {
    return createCmdBuffer(addr, cop, QByteArray());
}

QByteArray TenzoMScales::createCmdBuffer(unsigned char addr, unsigned char cop, QByteArray data) {
    QByteArray result;
    result.append(addr);
    result.append(cop);
    result.append(data);
    result.append((unsigned char) 0);
    unsigned char crc = tenzoMCrc((unsigned char*) result.data(), result.size(), 0);
    result[result.size()-1] = crc;
    result.push_front(0xff);
    result.append(0xff);
    result.append(0xff);
    return result;
}

double TenzoMScales::getAverage()
{
    return average.GetAverage(10);
}

bool TenzoMScales::running()
{
    return m_running;
}

void TenzoMScales::onIpAddressChanged(QString ipAddr)
{
    if(m_ipAddr == ipAddr) {
        return;
    }
    m_ipAddr = ipAddr;
    abort();
    verifyAndConnectToHost();
    m_goodIP = false;
}

void TenzoMScales::onPortChanged(int port)
{
    if(m_port == port) {
        return;
    }
    m_port = port;
    abort();
    verifyAndConnectToHost();
    m_goodIP = false;
}

void TenzoMScales::onStateChanged(QAbstractSocket::SocketState state)
{
    switch(state) {
    case UnconnectedState:
        qDebug() << "Disconnected from Tenzo-M";
        m_running = false;
        if(m_goodIP) {
            verifyAndConnectToHost();
        }
        break;
    case ConnectedState:
        qDebug() << "Connected to Tenzo-M";
        m_running = true;
        m_goodIP = true;
        break;
    default:
        qDebug() << "state:" << state;
        break;
    }
}

void TenzoMScales::requestData()
{
    if(state() == ConnectedState) {
        write(createCmdBuffer(0x01, 0xc3));
    }
}

void TenzoMScales::verifyAndConnectToHost()
{
    QHostAddress address(m_ipAddr);
    if(!address.isNull()) {
        if(address.toString() == m_ipAddr) {
            connectToHost(m_ipAddr, m_port);
        }
    }
}

void TenzoMScales::onReadyRead()
{
    QByteArray data = readAll();
    int size = data.size();
    if(size < 3) {
        return;
    }
    if((data.at(0) != -1) || (data.at(size-1) != -1) || (data.at(size-2) != -1)) {
        return;
    }
    data = data.mid(1, size-3);
    if(checkTenzoMCrc(data)) {
        if((unsigned char)data.at(1) == 0xc3) { // вес брутто
            QByteArray bcd = data.mid(2, 3);
            std::reverse(bcd.begin(), bcd.end()); // little endian
            double value = QString(bcd.toHex()).toDouble(); // without sign and decimal point
            if(data.at(5) & 0x80) { // sign bit
                value = -value;
            }
            average.Add(value);
        }
    }
}

bool TenzoMScales::checkTenzoMCrc(QByteArray input) {
    bool result = false;
    QByteArray test = input;
    test[input.size() -1] = 0;
    unsigned char calculatedCrc = tenzoMCrc((unsigned char*) test.data(), test.size(), 0);
    unsigned char providedCrc = input[input.size()-1];
    if(calculatedCrc == providedCrc) {
        result = true;
    }
    return result;
}
