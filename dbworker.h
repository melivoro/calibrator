#ifndef DBWORKER_H
#define DBWORKER_H

#include <QApplication>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDate>
#include <QDebug>

class SensorSettings {
public:
    int chan() const;
    void setChan(int chan);

    int index() const;
    void setIndex(int index);

    QString serial() const;
    void setSerial(const QString &serial);

    double correctionFactor() const;
    void setCorrectionFactor(double correctionFactor);

    double sensitivity() const;
    void setSensitivity(double sensitivity);

    int weightingLimit() const;
    void setWeightingLimit(int weightingLimit);

    int filterLength() const;
    void setFilterLength(int filterLength);

    double adcPgaGain() const;
    void setAdcPgaGain(double adcPgaGain);

    double softwareCorrectionFactor() const;
    void setSoftwareCorrectionFactor(double softwareCorrectionFactor);

private:
    int m_chan;
    int m_index;
    QString m_serial;
    double m_correctionFactor;
    double m_sensitivity;
    int m_weightingLimit;
    int m_filterLength;
    double m_adcPgaGain;
    double m_softwareCorrectionFactor;
};

class DBWorker : public QObject
{
    Q_OBJECT
public:
    explicit DBWorker(QObject *parent = nullptr);
    Q_PROPERTY(QString szobtePort READ getSzobtePort WRITE setSzobtePort NOTIFY szobtePortChanged)
    Q_PROPERTY(QString szobteSerialNumber READ getSzobteSerialNumber WRITE setSzobteSerialNumber NOTIFY szobteSerialNumberChanged)
    Q_PROPERTY(QString intuitionPort READ getIntuitionPort WRITE setIntuitionPort NOTIFY intuitionPortChanged)
    Q_PROPERTY(QString intuitionSerialNumber READ getIntuitionSerialNumber WRITE setIntuitionSerialNumber NOTIFY intuitionSerialNumberChanged)
    Q_PROPERTY(QString imPort READ getImPort WRITE setImPort NOTIFY imPortChanged)
    Q_PROPERTY(QString imComPort READ getImComPort WRITE setImComPort NOTIFY imComPortChanged)
    Q_PROPERTY(bool imListenOnly READ getImListenOnly WRITE setImListenOnly NOTIFY imListenOnlyChanged)
    Q_PROPERTY(bool imSaveToLog READ getImSaveToLog WRITE setImSaveToLog NOTIFY imSaveToLogChanged)
    Q_INVOKABLE bool szobteGetChannelState(int);
    Q_INVOKABLE void szobteSetChannelState(int, bool);
    Q_PROPERTY(QStringList szobteEnabledChansModel MEMBER m_szobteEnabledChansModel NOTIFY szobteEnabledChansModelChanged)
    Q_PROPERTY(QString tenzoMIPAddress READ getTenzoMIPAddress WRITE setTenzoMIPAddress NOTIFY tenzoMIPAddressChanged)
    Q_PROPERTY(int tenzoMPort READ getTenzoMPort WRITE setTenzoMPort NOTIFY tenzoMPortChanged)
    Q_PROPERTY(QString tenzoMSerialNumber READ getTenzoMSerialNumber WRITE setTenzoMSerialNumber NOTIFY tenzoMSerialNumberChanged)
    Q_PROPERTY(int tenzoMBigBagsWeight READ getTenzoMBigBagsWeight WRITE setTenzoMBigBagsWeight NOTIFY tenzoMBigBagsWeightChanged)
    Q_PROPERTY(bool tenzoMState READ getTenzoMState WRITE setTenzoMState NOTIFY tenzoMStateChanged)
    Q_PROPERTY(QStringList imEnabledChansModel MEMBER m_imEnabledChansModel NOTIFY imEnabledChansModelChanged)
    QList<int> szobteGetEnabledChans();
    QList<int> imGetEnabledChans();
    Q_INVOKABLE bool intuitionGetState();
    Q_INVOKABLE void intuitionSetState(bool);
    Q_INVOKABLE qreal szobteGetChannelCorrectionFactor(int);
    Q_INVOKABLE void szobteSetChannelCorrectionFactor(int, qreal);
    Q_INVOKABLE qreal szobteGetChannelSoftwareCorrectionFactor(int);
    Q_INVOKABLE void szobteSetChannelSoftwareCorrectionFactor(int, qreal);
    Q_INVOKABLE qreal szobteGetChannelSensitivity(int);
    Q_INVOKABLE void szobteSetChannelSensitivity(int, qreal);
    Q_INVOKABLE int szobteGetChannelWeightingLimit(int);
    Q_INVOKABLE void szobteSetChannelWeightingLimit(int, int);
    Q_INVOKABLE int szobteGetChannelFilterLength(int);
    Q_INVOKABLE void szobteSetChannelFilterLength(int, int);
    Q_INVOKABLE QString szobteGetSensorSerialNumber(int);
    Q_INVOKABLE void szobteSetSensorSerialNumber(int, QString);
    Q_INVOKABLE int szobteGetSensorModelIndex(int);
    Q_INVOKABLE void szobteSetSensorModelIndex(int, int);
    Q_INVOKABLE int intuitionGetSensorModelIndex();
    Q_INVOKABLE void intuitionSetSensorModelIndex(int);
    Q_INVOKABLE QString intuitionGetSensorSerialNumber();
    Q_INVOKABLE void intuitionSetSensorSerialNumber(QString);
    Q_INVOKABLE QString imGetSerialNumber(int);
    Q_INVOKABLE void imSetSerialNumber(int, QString);
    Q_INVOKABLE int imGetCanId(int);
    Q_INVOKABLE void imSetCanId(int, int);
    Q_INVOKABLE int imGetPgaGain(int);
    Q_INVOKABLE void imSetPgaGain(int, int);
    Q_INVOKABLE int imGetFilterLength(int);
    Q_INVOKABLE void imSetFilterLength(int, int);
    Q_INVOKABLE qreal imGetChannelSensitivity(int);
    Q_INVOKABLE void imSetChannelSensitivity(int, qreal);
    Q_INVOKABLE int imGetChannelWeightingLimit(int);
    Q_INVOKABLE void imSetChannelWeightingLimit(int, int);
    Q_INVOKABLE bool imGetChannelState(int);
    Q_INVOKABLE void imSetChannelState(int, bool);
    Q_INVOKABLE qreal imGetChannelSoftwareCorrectionFactor(int);
    Q_INVOKABLE void imSetChannelSoftwareCorrectionFactor(int, qreal);
    Q_INVOKABLE QString imGetSensorSerialNumber(int);
    Q_INVOKABLE void imSetSensorSerialNumber(int, QString);
    Q_INVOKABLE int imGetSensorModelIndex(int);
    Q_INVOKABLE void imSetSensorModelIndex(int, int);
    Q_INVOKABLE QString getComment(QDate, int);
    int getCalibratedIndex();
    void setCalibratedIndex(int);
    int getReferenceIndex();
    void setReferenceIndex(int);
    int getReferenceChannel();
    void setReferenceChannel(int);
    QString getSzobteSerialNumber();
    QString getIntuitionSerialNumber();
    QString getTenzoMIPAddress();
    int getTenzoMPort();
    QString getTenzoMSerialNumber();
    int getTenzoMBigBagsWeight();
    bool getTenzoMState();
    bool getImListenOnly();
    bool getImSaveToLog();
    void getMeasurementSettings(QDate, int, int*, QString*, int*, QString*, int*, int*);
    void setMeasurementSettings(QDate, int, QString, int, QString, int, int*);
    QList<SensorSettings> getSensorSettingsList(int);
    QList<QList<double>> getData(int);
    void setSensorSettings(int, int, int, QString, double, double, int, int, double, double);
    void setExperimentData(int, int, int, double);
    void setSingleChanResults(int, double);
    void setComment(int, QString);
    Q_INVOKABLE QStringList getMeasurementsByDate(QDate);
    void getSoftwareCorrectionFactor(int, QString, int, int, QString, double, double, int, int, double, QVariantList*);

signals:
    void szobtePortChanged();
    void szobteSerialNumberChanged();
    void intuitionPortChanged();
    void intuitionSerialNumberChanged();
    void imPortChanged();
    void imComPortChanged();
    void imSerialNumberChanged(int, QString);
    void imCanIdChanged(int, int);
    void imPgaGainChanged(int, int);
    void imFilterLengthChanged(int, int);
    void imChannelSensitivityChanged(int, qreal);
    void imChannelWeightingLimitChanged(int, int);
    void imChannelStateChanged(int, bool);
    void imEnabledChansModelChanged();
    void imChannelCorrectionFactorChanged(int, qreal);
    void imChannelSoftwareCorrectionFactorChanged(int, qreal);
    void imSensorSerialNumberChanged(int, QString);
    void imSensorModelIndexChanged(int, int);
    void szobteChannelStateChanged(int, bool);
    void intuitionStateChanged(bool);
    void szobteEnabledChansModelChanged();
    void szobteChannelCorrectionFactorChanged(int, qreal);
    void szobteChannelSoftwareCorrectionFactorChanged(int, qreal);
    void szobteChannelSensitivityChanged(int, qreal);
    void szobteChannelWeightingLimitChanged(int, int);
    void szobteChannelFilterLengthChanged(int, int);
    void szobteSensorSerialNumberChanged(int, QString);
    void szobteSensorModelIndexChanged(int, int);
    void intuitionSensorModelIndexChanged(int);
    void intuitionSensorSerialNumberChanged(QString);
    void tenzoMIPAddressChanged();
    void tenzoMPortChanged();
    void tenzoMSerialNumberChanged();
    void tenzoMBigBagsWeightChanged();
    void tenzoMStateChanged(bool);
    void imListenOnlyChanged(bool);
    void imSaveToLogChanged(bool);
    void calibratedIndexChanged(int);
    void referenceIndexChanged(int);
    void referenceChanChanged(int);

private slots:
    void onSzobteChannelStateChanged(int, bool);
    void onImChannelStateChanged(int, bool);

private:
    int dbConnect();
    int init();
    QString getConfigValue(QString);
    int setConfigValue(QString, QString);
    QString getSzobtePort();
    void setSzobtePort(QString);
    void setSzobteSerialNumber(QString);
    QString getIntuitionPort();
    void setIntuitionPort(QString);
    void setIntuitionSerialNumber(QString);
    QString getImPort();
    void setImPort(QString);
    QString getImComPort();
    void setImComPort(QString);
    void szobteRefreshEnabledChansModel();
    void setTenzoMIPAddress(QString);
    void setTenzoMPort(int);
    void setTenzoMSerialNumber(QString);
    void setTenzoMBigBagsWeight(int);
    void setTenzoMState(bool);
    void setImListenOnly(bool);
    void setImSaveToLog(bool);
    void imRefreshEnabledChansModel();

    QStringList m_szobteEnabledChansModel;
    QStringList m_imEnabledChansModel;
};

#endif // DBWORKER_H
