#ifndef SZOBTEAMPLIFIER_H
#define SZOBTEAMPLIFIER_H

#include <QDebug>
#include <QTime>
#include "serialport.h"
#include "circularbuffer.h"

class SzobteAmplifier : public SerialPort
{
    Q_OBJECT
public:
    explicit SzobteAmplifier(QObject *parent = nullptr);
    double getAverage(int);

signals:
    void setActiveChans(QList<int>);
    void newData(qint64, int, double);

private slots:
    void onSetActiveChans(QList<int>);

private:
    CircularBuffer average[10];
    void checkData(QString);
    QMap<int, bool> m_activeChans;
};

#endif // SZOBTEAMPLIFIER
