#ifndef IMAMPLIFIER_H
#define IMAMPLIFIER_H

#include <QApplication>
#include <QMap>
#include <QTimer>
#include <QThread>
#include <QDate>
#include <QDir>
#include <QDebug>
#include "canlib.h"
#include "j1939.h"
#include "circularbuffer.h"

enum IMState {
    IM_StateNone,
    IM_StateStop,
    IM_StateInit,
    IM_StateConnected,
};

class IMAmplifier : public QObject
{
    Q_OBJECT
public:
    explicit IMAmplifier(QObject *parent = nullptr);
    double getAverage(int, int);
    double getAverage(int);
    uint32_t getCanId(int);
    void setCanId(int, uint32_t);
    uint32_t getPgaGain(int);
    void setPgaGain(int, uint32_t);
    int getFilterLength(int);
    void setFilterLength(int, int);
    int getWeightingLimit(int);
    void setWeightingLimit(int, int);
    double getSensitivity(int);
    void setSensitivity(int, double);
    bool isOpen() const;
    void setListenOnly(bool listenOnly);
    void setSaveToLog(bool saveToLog);

public slots:
    void setPort(QString port);
    void run();

signals:
    void finished();
    void stopStarted();
    void initStarted();
    void acksUpdated();
    void portChanged(QString);
    void setActiveChans(QList<int>);
    void measuringStarted();

private slots:
    void onStopStarted();
    void onInitStarted();
    void onAcksUpdated();
    void onPortChanged(QString);
    void onInitTimeout();
    void onSetActiveChans(QList<int>);

private:
    QString m_port;
    CircularBuffer m_average[12];
    bool m_running;
    int m_canHandle;
    bool m_connected;
    bool m_busOn;
    bool m_isOpen;
    uint8_t m_da;
    uint8_t m_sa;
    IMState m_state;
    uint32_t m_canIds[6];
    uint32_t m_pgaGain[6];
    int m_filterLength[12];
    int m_weightingLimit[12];
    double m_sensitivity[12];
    QMap<unsigned int, bool> m_acks;
    QMap<int, bool> m_activeChans;
    QTimer *m_initTimer;
    void refresh();
    bool m_listenOnly;
    bool m_saveToLog;
    QString m_logFilename;
    void writeToLog(unsigned long, long, unsigned int, unsigned char*);
};

#endif // IMAMPLIFIER_H
