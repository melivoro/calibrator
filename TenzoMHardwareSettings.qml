import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQml.Models 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

Item {
    GridLayout {
        anchors.fill: parent
        columns: 2
        Label {
            font.pointSize: 10
            text: qsTr("IP Address:")
        }
        TextField {
            Layout.fillWidth: true
            validator: RegExpValidator {
                regExp: /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
            }
            onTextChanged: {
                dbworker.tenzoMIPAddress = text
                hardwareManager.tenzoMIpAddrChanged(text)
            }
            Component.onCompleted: {
                text = dbworker.tenzoMIPAddress
                hardwareManager.tenzoMIpAddrChanged(text)
            }
        }
        Label {
            font.pointSize: 10
            text: qsTr("Port:")
        }
        SpinBox {
            Layout.preferredWidth: 200
            editable: true
            to: 65535
            onValueChanged: {
                dbworker.tenzoMPort = value
                hardwareManager.tenzoMPortChanged(value)
            }
            Component.onCompleted: {
                value = dbworker.tenzoMPort
                hardwareManager.tenzoMPortChanged(value)
            }
        }
        Label {
            font.pointSize: 10
            text: qsTr("Serial number:")
        }
        TextField {
            Layout.fillWidth: true
            validator: RegExpValidator {
                regExp: /^\d{1,10}$/
            }
            onTextChanged: {
                dbworker.tenzoMSerialNumber = text
            }
            Component.onCompleted: {
                text = dbworker.tenzoMSerialNumber
            }
        }
        Rectangle {
            Layout.columnSpan: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: Material.primary
            GridLayout {
                anchors {
                    left: parent.left
                    top: parent.top
                    margins: 20
                }
                columns: 2
                columnSpacing: 10
                Label {
                    Layout.columnSpan: 2
                    text: qsTr("Big Bags Weight:")
                    font.pointSize: 10
                }
                SpinBox {
                    Layout.preferredWidth: 200
                    Layout.columnSpan: 2
                    editable: true
                    to: 100000
                    onValueChanged: {
                        dbworker.tenzoMBigBagsWeight = value
                        hardwareManager.tenzoMBigBagsWeightChanged(value)
                    }
                    Component.onCompleted: {
                        value = dbworker.tenzoMBigBagsWeight
                        hardwareManager.tenzoMBigBagsWeightChanged(value)
                    }
                }
                Label {
                    text: qsTr("Add to weighting")
                    Layout.preferredWidth: 200
                    font {
                        bold: true
                        pointSize: 10
                    }
                }
                Switch {
                    text: checked ? qsTr("On") : qsTr("Off")
                    onCheckedChanged: {
                        if (checked !== dbworker.tenzoMState) {
                            dbworker.tenzoMState = checked
                        }
                    }
                    Component.onCompleted: {
                        checked = dbworker.tenzoMState
                    }
                }
            }
        }
    }
}
