import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQml.Models 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

Item {
    GridLayout {
        anchors.fill: parent
        columns: 2
        Label {
            font.pointSize: 10
            text: qsTr("Comport:")
        }
        ComboBox {
            id: szobteComPortComboBox
            Layout.fillWidth: true
            Layout.preferredHeight: 35
            model: DelegateModel {
                id: szobteDelegateModel
                model: comPortModel
                delegate: ItemDelegate {
                    text: name + " (" + description + ")"
                }
            }
            onDisplayTextChanged: {
                dbworker.szobtePort = displayText
            }
            onActivated: {
                displayText = szobteDelegateModel.items.get(
                            currentIndex).model.name
                hardwareManager.szobtePort = displayText
            }
            Connections {
                target: hardwareManager
                function onSzobtePortChanged(port) {
                    if (port === "") {
                        szobteComPortComboBox.displayText = ""
                    }
                }
            }
            Component.onCompleted: {
                for(var i = 0; i < comPortModel.rowCount(); i++) {
                    var port = dbworker.szobtePort
                    if(port === szobteDelegateModel.items.get(i).model.name) {
                        displayText = port
                        hardwareManager.szobtePort = displayText
                        break
                    }
                }
            }
        }
        Label {
            font.pointSize: 10
            text: qsTr("Serial number:")
        }
        TextField {
            Layout.fillWidth: true
            validator: RegExpValidator {
                regExp: /^\d{1,10}$/
            }
            onTextChanged: {
                dbworker.szobteSerialNumber = text
            }
            Component.onCompleted: {
                text = dbworker.szobteSerialNumber
            }
        }
        Label {
            font.pointSize: 10
            text: qsTr("Frequency:")
        }
        SpinBox {
            Layout.preferredHeight: 35
            from: 1
            to: 100
            editable: true
            value: 100
        }
        Rectangle {
            Layout.columnSpan: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: Material.primary
            RowLayout {
                anchors {
                    fill: parent
                }
                ToolButton {
                    icon.source: "images/left.png"
                    Layout.preferredWidth: 25
                    Layout.preferredHeight: 25
                    Layout.alignment: Qt.AlignTop
                    Layout.topMargin: 10
                    enabled: szobteChanSettingsListView.currentIndex != 0
                    onClicked: {
                        if (szobteChanSettingsListView.currentIndex > 0) {
                            szobteChanSettingsListView.currentIndex--
                        }
                    }
                }
                ListView {
                    id: szobteChanSettingsListView
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    clip: true
                    contentHeight: height
                    contentWidth: width
                    orientation: ListView.Horizontal
                    snapMode: ListView.SnapToItem
                    interactive: false
                    model: 10
                    delegate: ScrollView {
                        width: szobteChanSettingsListView.width
                        height: szobteChanSettingsListView.height
                        GridLayout {
                            anchors {
                                fill: parent
                                margins: 10
                            }
                            columns: 2
                            columnSpacing: 10
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("Channel %1").arg(index + 1)
                                font.pointSize: 15
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("%1 of %2").arg(index + 1).arg(10)
                                color: "gray"
                                font.pointSize: 10
                                Layout.columnSpan: 2
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("Strain amplifier")
                                font {
                                    bold: true
                                    pointSize: 15
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Correction factor")
                                font.pointSize: 10
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Sensitivity")
                                font.pointSize: 10
                            }
                            DoubleSpinBox {
                                Layout.preferredWidth: 200
                                editable: true
                                onRealValueChanged: {
                                    if(value !== Math.round(dbworker.szobteGetChannelCorrectionFactor(index+1)*1000000)) {
                                        dbworker.szobteSetChannelCorrectionFactor(index+1, realValue)
                                    }
                                }
                                Component.onCompleted: {
                                    value = Math.round(dbworker.szobteGetChannelCorrectionFactor(index+1)*1000000)
                                }
                            }
                            DoubleSpinBox {
                                Layout.preferredWidth: 200
                                editable: true
                                to: 10000000
                                onRealValueChanged: {
                                    if(value !== Math.round(dbworker.szobteGetChannelSensitivity(index+1)*1000000)) {
                                        dbworker.szobteSetChannelSensitivity(index+1, realValue)
                                    }
                                }
                                Component.onCompleted: {
                                    value = Math.round(dbworker.szobteGetChannelSensitivity(index+1)*1000000)
                                }
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Weighting limit")
                                font.pointSize: 10
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Filter length")
                                font.pointSize: 10
                            }
                            SpinBox {
                                Layout.preferredWidth: 200
                                editable: true
                                to: 100000
                                onValueChanged: {
                                    if(value !== dbworker.szobteGetChannelWeightingLimit(index+1)) {
                                        dbworker.szobteSetChannelWeightingLimit(index+1, value)
                                    }
                                }
                                Component.onCompleted: {
                                    value = dbworker.szobteGetChannelWeightingLimit(index+1)
                                }
                            }
                            SpinBox {
                                Layout.preferredWidth: 200
                                editable: true
                                //from: 1
                                to: 100
                                onValueChanged: {
                                    if(value !== dbworker.szobteGetChannelFilterLength(index+1)) {
                                        dbworker.szobteSetChannelFilterLength(index+1, value)
                                    }
                                }
                                Component.onCompleted: {
                                    value = dbworker.szobteGetChannelFilterLength(index+1)
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("Sensor")
                                font {
                                    bold: true
                                    pointSize: 15
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Model")
                                font.pointSize: 10
                            }
                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Serial number")
                                font.pointSize: 10
                            }
                            ComboBox {
                                signal selected
                                Layout.preferredHeight: 35
                                Layout.preferredWidth: 200
                                model: [qsTr("Suzhou LSZ-S03G-10t"), qsTr("Other")]
                                onActivated: {
                                    selected()
                                }
                                onSelected: {
                                    if(currentIndex !== dbworker.szobteGetSensorModelIndex(index+1)) {
                                        dbworker.szobteSetSensorModelIndex(index+1, currentIndex)
                                    }
                                }
                                Component.onCompleted: {
                                    currentIndex = dbworker.szobteGetSensorModelIndex(index+1)
                                }
                            }
                            TextField {
                                Layout.preferredWidth: 200
                                validator: RegExpValidator {
                                    regExp: /^\d{1,10}$/
                                }
                                onTextChanged: {
                                    dbworker.szobteSetSensorSerialNumber(index+1, text)
                                }
                                Component.onCompleted: {
                                    text = dbworker.szobteGetSensorSerialNumber(index+1)
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                            Label {
                                text: qsTr("Add channel to weighting")
                                font {
                                    bold: true
                                    pointSize: 10
                                }
                            }
                            Switch {
                                text: checked ? qsTr("On"): qsTr("Off")
                                onCheckedChanged: {
                                    if(checked !== dbworker.szobteGetChannelState(index+1)) {
                                        dbworker.szobteSetChannelState(index+1, checked)
                                    }
                                }

                                Component.onCompleted: {
                                    checked = dbworker.szobteGetChannelState(index+1)
                                }
                            }
                            Item {
                                height: 10
                                Layout.columnSpan: 2
                            }
                        }
                    }
                    onCurrentIndexChanged: {
                        positionViewAtIndex(currentIndex, ListView.Center)
                    }
                }
                ToolButton {
                    icon.source: "images/right.png"
                    Layout.preferredWidth: 25
                    Layout.preferredHeight: 25
                    Layout.alignment: Qt.AlignTop
                    Layout.topMargin: 10
                    enabled: szobteChanSettingsListView.currentIndex != 9
                    onClicked: {
                        if (szobteChanSettingsListView.currentIndex < 9) {
                            szobteChanSettingsListView.currentIndex++
                        }
                    }
                }
            }
        }
    }
}
