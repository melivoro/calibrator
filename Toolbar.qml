import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ToolBar {
    property bool hamburgerButtonEnabled
    property bool enableButtons
    property int toolbarHeight
    property string backgroundColor
    property string hamburgerButtonColor
    signal hamburgerButtonClicked
    signal newClicked
    signal openClicked
    signal saveClicked
    property alias text: txt.text
    background: Rectangle {
        color: backgroundColor
        implicitHeight: toolbarHeight
    }
    RowLayout {
        anchors {
            fill: parent
            rightMargin: 10
        }
        Rectangle {
            Layout.preferredHeight: toolbarHeight
            Layout.preferredWidth: toolbarHeight
            color: hamburgerButtonColor
            enabled: hamburgerButtonEnabled
            ToolButton {
                anchors.fill: parent
                contentItem: Image {
                    fillMode: Image.PreserveAspectFit
                    source: "images/hamburger.png"
                }
                onClicked: {
                    hamburgerButtonClicked()
                }
            }
        }
        Label {
            id: txt
            Layout.preferredHeight: toolbarHeight
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignVCenter
            horizontalAlignment: Qt.AlignLeft
            verticalAlignment: Qt.AlignVCenter
            font.bold: true
            font.pointSize: 10
        }
        Item {
            Layout.fillWidth: true
        }
        ToolButton {
            text: qsTr("New")
            Layout.preferredHeight: toolbarHeight
            font {
                capitalization: Font.MixedCase
                pointSize: 10
            }
            onClicked: {
                newClicked()
            }
        }
        ToolButton {
            text: qsTr("Open")
            icon.source: "images/open.png"
            Layout.preferredHeight: toolbarHeight
            font {
                capitalization: Font.MixedCase
                pointSize: 10
            }
            onClicked: {
                openClicked()
            }
        }
        ToolButton {
            text: qsTr("Save")
            enabled: softwareConfiguration !== null ? softwareConfiguration.blockedConfiguration: false
            icon.source: "images/save.png"
            Layout.preferredHeight: toolbarHeight
            font {
                capitalization: Font.MixedCase
                pointSize: 10
            }
            onClicked: {
                saveClicked()
            }
        }
        ToolButton {
            text: qsTr("Export")
            enabled: enableButtons
            icon.source: "images/export.png"
            Layout.preferredHeight: toolbarHeight
            font {
                capitalization: Font.MixedCase
                pointSize: 10
            }
        }
        ToolSeparator {
            visible: enableButtons
            Layout.preferredHeight: toolbarHeight
        }
        ToolButton {
            text: qsTr("Pegov R.")
            icon.source: "images/human.png"
            Layout.preferredHeight: toolbarHeight
            font {
                capitalization: Font.MixedCase
                pointSize: 10
            }
        }
    }
}
