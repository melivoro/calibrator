#include "comportmodel.h"

ComPort::ComPort(const QString& name, const QString& description): m_name(name), m_description(description) {
}

QString ComPort::getName() const {
    return m_name;
}

QString ComPort::getDescription() const {
    return m_description;
}

ComPortModel::ComPortModel(QObject *parent): QAbstractListModel(parent) {
    reload();
}

Q_INVOKABLE void ComPortModel::reload() {
    beginResetModel();
    comports.clear();
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        addComPort(ComPort(info.portName(), info.description()));
    }
    endResetModel();
}

QVariant ComPortModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() > comports.count()) {
        return QVariant();
    }
    const ComPort & cp = comports[index.row()];
    if (role == NameRole) {
        return cp.getName();
    } else if (role == DescriptionRole) {
        return cp.getDescription();
    }
    return QVariant();
}

int ComPortModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return comports.size();
}

QHash<int, QByteArray> ComPortModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[DescriptionRole] = "description";
    return roles;
}

void ComPortModel::addComPort(ComPort cp) {
    comports << cp;
}
