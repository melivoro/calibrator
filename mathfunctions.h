#ifndef MATHFUNCTIONS_H
#define MATHFUNCTIONS_H

#include <QList>
#include <QDebug>

void approximate(QList<QPair<double, double>> data, double *a, double *b, double *errorMax) {
    int n = data.size();
    double sumx = 0;
    double sumy = 0;
    double sumx2 = 0;
    double sumxy = 0;
    for(int i = 0; i < n; i++) {
        sumx += data[i].first;
        sumy += data[i].second;
        sumx2 += data[i].first * data[i].first;
        sumxy += data[i].first * data[i].second;
    }
    *a = (n*sumxy - (sumx*sumy)) / (n*sumx2 - sumx*sumx);
    *b = (sumy - *a*sumx) / n;
    *errorMax = 0;
    for(int i = 0; i < n; i++) {
        double error = abs(*a * data[i].first + *b - data[i].second);
        if (error > *errorMax) {
            *errorMax = error;
        }
    }
    return;
}

void approximateq(QList<QPair<double, double>> data, double *aq, double *bq, double *cq, double *xmin, double *xmax) {
    int n = data.size();
    double sumx = 0;
    double sumy = 0;
    double sumx2 = 0;
    double sumx3 = 0;
    double sumx4 = 0;
    double sumxy = 0;
    double sumx2y = 0;
    for(int i = 0; i < n; i++) {
        if(i == 0) {
            *xmin = data[i].first;
            *xmax = data[i].first;
        }
        if(data[i].first > *xmax) {
            *xmax = data[i].first;
        }
        if(data[i].first < *xmin) {
            *xmin = data[i].first;
        }
        sumx += data[i].first;
        sumy += data[i].second;
        sumx2 += data[i].first * data[i].first;
        sumx3 += data[i].first * data[i].first * data[i].first;
        sumx4 += data[i].first * data[i].first * data[i].first * data[i].first;
        sumxy += data[i].first * data[i].second;
        sumx2y += data[i].first * data[i].first * data[i].second;
    }
    /*
     * sumx4 * a + sumx3 * b + sumx2 * c = sumx2y
     * sumx3 * a + sumx2 * b + sumx * c = sumxy
     * sumx2 * a + sumx * b + n * c = sumy
     */
    double det = sumx4 * sumx2 * n + 2 * sumx3 * sumx2 * sumx - sumx2 * sumx2 * sumx2 - sumx4 * sumx * sumx - sumx3 * sumx3 * n;
    double detA = sumx2y * sumx2 * n + sumx3 * sumx * sumy + sumx2 * sumxy * sumx - sumx2 * sumx2 * sumy - sumx * sumx * sumx2y - sumx3 * sumxy * n;
    double detB = sumx4 * sumxy * n + sumx2 * sumx2y * sumx + sumx3 * sumx2 * sumy - sumx2 * sumx2 * sumxy - sumx4 * sumx * sumy - sumx3 * sumx2y * n;
    double detC = sumx4 * sumx2 * sumy + sumx3 * sumx2 * sumxy + sumx3 * sumx2y * sumx - sumx2 * sumx2 * sumx2y - sumx4 * sumxy * sumx - sumx3 * sumx3 * sumy;
    *aq = detA / det;
    *bq = detB / det;
    *cq = detC / det;
    /*
     * f(x) = aq * x * x + (bq - a) * x + (cq - b)
     * Это разность между аппроксимированной параболой и аппроксимированной прямой
     * Проверяем значение f(x) в граничных точках (минимальное и максимальное значение X), а также точку экстремума
     * (если она окажется в измеряемом диапазоне)
     * Максимальное значение f(x) для этих трёх точек и есть величина нелинейности (в килограммах)
     */
}

double calculateNonLinearity(double a, double b, double c, double xmin, double xmax)
{
    /*
     * f(x) = a * x^2 + b * x + c
     * f'(x) = 2 * a * x + b = 0
     * x = -b / (2 * a)
     */
    double extremum = -0.5 * b / a;
    double fxmin = a * xmin * xmin + b * xmin + c;
    double fxmax = a * xmax * xmax + b * xmax + c;
    double max = abs(fxmin) < abs(fxmax) ? fxmax: fxmin;
    if((xmin <= extremum) && (extremum <= xmax)) {
        double fextr = a * extremum * extremum + b * extremum + c;
        max = abs(max) < abs(fextr) ? fextr: max;
    }
    return max;
}

#endif // MATHFUNCTIONS_H
