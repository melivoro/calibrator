#ifndef COMPORTMODEL_H
#define COMPORTMODEL_H

#include <QAbstractListModel>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

class ComPort {
public:
    ComPort(const QString&, const QString&);
    QString getName() const;
    QString getDescription() const;
private:
    QString m_name;
    QString m_description;
};

class ComPortModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ComPortModel(QObject *parent = nullptr);
    enum ComPortRoles {
        NameRole = Qt::UserRole + 1,
        DescriptionRole
    };
    Q_INVOKABLE void reload();
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent) const;
    QHash<int, QByteArray> roleNames() const;
private:
    void addComPort(ComPort);
    QList<ComPort> comports;
};

#endif // COMPORTMODEL_H
