QT += qml quick quickcontrols2 serialport charts sql network

TEMPLATE = app

CONFIG += c++11 sdk_no_version_check

win32: RC_ICONS += Calibrator.ico
macx: ICON = Calibrator.icns

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        canportmodel.cpp \
        circularbuffer.cpp \
        comportmodel.cpp \
        dbworker.cpp \
        hardwaremanager.cpp \
        im2amplifier.cpp \
        imamplifier.cpp \
        intuitionamplifier.cpp \
        main.cpp \
        serialport.cpp \
        softwareconfiguration.cpp \
        szobteamplifier.cpp \
        tenzomscales.cpp \
        j1939.cpp

RESOURCES += qml.qrc

TRANSLATIONS += \
    Calibrator_ru_RU.ts

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    canportmodel.h \
    circularbuffer.h \
    comportmodel.h \
    dbworker.h \
    hardwaremanager.h \
    im2amplifier.h \
    imamplifier.h \
    intuitionamplifier.h \
    mathfunctions.h \
    serialport.h \
    softwareconfiguration.h \
    szobteamplifier.h \
    tenzomscales.h \
    canlib.h \
    canevt.h \
    canstat.h \
    predef.h \
    obsolete.h \
    j1939.h

win32: LIBS += canlib32.lib

DISTFILES += \
    canlib32.lib

#DISTFILES += \
#    QmlImports.qml
