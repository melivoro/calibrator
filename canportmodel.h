#ifndef CANPORTMODEL_H
#define CANPORTMODEL_H

#include <QAbstractListModel>
#include <QDebug>
#include "canlib.h"

class CanPort {
public:
    CanPort(const QString&, const QString&);
    QString getName() const;
    QString getDescription() const;
private:
    QString m_name;
    QString m_description;
};

class CanPortModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit CanPortModel(QObject *parent = nullptr);
    enum ComPortRoles {
        NameRole = Qt::UserRole + 1,
        DescriptionRole
    };
    Q_INVOKABLE void reload();
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent) const;
    QHash<int, QByteArray> roleNames() const;
private:
    void addCanPort(CanPort);
    QList<CanPort> canports;
};

#endif // CANPORTMODEL_H
