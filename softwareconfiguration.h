#ifndef SOFTWARECONFIGURATION_H
#define SOFTWARECONFIGURATION_H

#include <qqml.h>
#include <QAbstractTableModel>
#include "dbworker.h"
#include "hardwaremanager.h"

class ResultsModel : public QAbstractTableModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_ADDED_IN_MINOR_VERSION(1)

public:
    ResultsModel();
    int rowCount(const QModelIndex& = QModelIndex()) const override;
    int columnCount(const QModelIndex & = QModelIndex()) const override;
    QVariant data(const QModelIndex&, int) const override;
    bool setData(const QModelIndex&, const QVariant&, int) override;
    QHash<int, QByteArray> roleNames() const override;
    Q_PROPERTY(QList<bool> calibratedChannelSet MEMBER m_calibratedChannelSet WRITE setCalibratedList)
    Q_PROPERTY(int referenceChannel MEMBER m_referenceChan WRITE setReferenceChannel)
    Q_PROPERTY(QStringList headers READ getHeaders NOTIFY headersChanged)
    Q_INVOKABLE void append(QList<double>);
    QList<int> getCalibratedChannels(int);
    Q_INVOKABLE int getDataUpSize();
    Q_INVOKABLE int getDataDownSize();
    Q_INVOKABLE int getErrorSize();
    Q_INVOKABLE double getPointX(int, int);
    Q_INVOKABLE double getPointY(int, int);
    Q_INVOKABLE double getMinX() const;
    Q_INVOKABLE double getMaxX() const;
    QList<QList<double> > getData() const;
    void setCalibratedList(QList<bool>);
    void setReferenceChannel(int);

signals:
    void headersChanged();
    void newData();
    void newDataProcessed(double, double, double, double, double, double, double, double, double, double);
    void clear();

private slots:
    void onNewData();
    void onClear();

private:
    int m_referenceChan;
    QList<bool> m_calibratedChannelSet;
    QString m_referenceHeader;
    QStringList m_calibratedHeaders;
    QList<QList<double>> m_data;
    QList<QPair<double, double>> m_dataUp;
    QList<QPair<double, double>> m_dataDown;
    QList<QPair<double, double>> m_error;
    double m_minX;
    double m_maxX;
    QStringList getHeaders();
    double getReference(int);
    double getCalibrated(int);
};

class ResultsAndSettings {
public:
    double a() const;
    void setA(double a);

    double b() const;
    void setB(double b);

    double aUp() const;
    void setAUp(double aUp);

    double bUp() const;
    void setBUp(double bUp);

    double aDown() const;
    void setADown(double aDown);

    double bDown() const;
    void setBDown(double bDown);

    double aq() const;
    void setAq(double aq);

    double bq() const;
    void setBq(double bq);

    double cq() const;
    void setCq(double cq);

    double nonLinearity() const;
    void setNonLinearity(double nonLinearity);

    int pNom() const;
    void setPNom(int pNom);

    int percent() const;
    void setPercent(int percent);

    double fUp() const;

    double fDown() const;

    double hyst() const;

    double hystPercent() const;

    double nonLinearityPercent() const;

    SensorSettings referenceSensor() const;
    void setReferenceSensor(const SensorSettings &referenceSensor);

    QList<SensorSettings> calibratedSensors() const;
    void setCalibratedSensors(const QList<SensorSettings> &calibratedSensors);

    int referenceIndex() const;
    void setReferenceIndex(int referenceIndex);

    int calibratedIndex() const;
    void setCalibratedIndex(int calibratedIndex);

    QString referenceSerial() const;
    void setReferenceSerial(const QString &referenceSerial);

    QString calibratedSerial() const;
    void setCalibratedSerial(const QString &calibratedSerial);

private:
    double m_a;
    double m_b;
    double m_aUp;
    double m_bUp;
    double m_aDown;
    double m_bDown;
    double m_aq;
    double m_bq;
    double m_cq;
    double m_nonLinearity;
    int m_pNom;
    int m_percent;
    double m_fUp;
    double m_fDown;
    double m_hyst;
    double m_hystPercent;
    double m_nonLinearityPercent;
    int m_referenceIndex;
    int m_calibratedIndex;
    QString m_referenceSerial;
    QString m_calibratedSerial;
    SensorSettings m_referenceSensor;
    QList<SensorSettings> m_calibratedSensors;

    void refresh();
};

class SoftwareConfiguration : public QObject
{
    Q_OBJECT
public:
    explicit SoftwareConfiguration(QObject *parent = nullptr);
    DBWorker* getDbWorker();
    HardwareManager* getHwManager();
    ResultsModel* getResultsModel();
    Q_PROPERTY(int calibratedIndex READ getCalibratedIndex WRITE setCalibratedIndex NOTIFY calibratedIndexChanged)
    Q_PROPERTY(int referenceIndex READ getReferenceIndex WRITE setReferenceIndex NOTIFY referenceIndexChanged)
    Q_PROPERTY(int calibratedEnabledChannelsNumber READ getCalibratedEnabledChannelsNumber NOTIFY calibratedEnabledChannelsNumberChanged)
    Q_PROPERTY(int referenceChan READ getReferenceChannel WRITE setReferenceChannel NOTIFY referenceChannelChanged)
    Q_PROPERTY(bool blockedConfiguration MEMBER m_blockedConfiguration NOTIFY blockedConfigurationChanged)
    Q_PROPERTY(bool openFromDb MEMBER m_openFromDb NOTIFY openFromDbChanged)
    Q_PROPERTY(QString resultString MEMBER m_resultString NOTIFY resultStringChanged)
    Q_INVOKABLE void saveData(QString);
    Q_INVOKABLE void setDataFromDb(QDate, int);
    Q_INVOKABLE QVariantList getSoftwareCorrectionFactor(int, int);

signals:
    void calibratedIndexChanged();
    void referenceIndexChanged();
    void calibratedEnabledChannelsNumberChanged();
    void referenceChannelChanged();
    void blockedConfigurationChanged(bool);
    void openFromDbChanged(bool);
    void resultStringChanged(QString);

private slots:
    void onCalibratedAmplifierIndexChanged();
    void onSzobteEnabledChansModelChanged();
    void onCalculateResult(double, double, double, double, double, double, double, double, double, double);
    void onBlockedConfigurationChanged(bool);

private:
    DBWorker m_dbWorker;
    ResultsModel m_resultsModel;
    HardwareManager m_hardwareManager;
    ResultsAndSettings m_resultsAndSettings;
    bool m_openFromDb;
    bool m_blockedConfiguration;
    QString m_resultString;
    int getCalibratedIndex();
    void setCalibratedIndex(int);
    int getReferenceIndex();
    void setReferenceIndex(int);
    int getCalibratedEnabledChannelsNumber();
    int getReferenceChannel();
    void setReferenceChannel(int);
};

#endif // SOFTWARECONFIGURATION_H
