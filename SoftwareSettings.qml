import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQml.Models 2.15

ScrollView {
    signal goToHardwareSettings
    signal calibratedChannelSetChanged(var chans)
    signal referenceChannelChanged(var chan)
    clip: true
    ListModel {
        id: calibratedChanModel
        function refresh() {
            clear()
            if (softwareConfiguration.calibratedIndex === 0) {
                for (var i in dbworker.szobteEnabledChansModel) {
                    if(i > 0) {
                        if(softwareConfiguration.referenceIndex == 0) {
                            if(parseInt(dbworker.szobteEnabledChansModel[i]) === softwareConfiguration.referenceChan) {
                                continue
                            }
                        }
                        append({"ch": dbworker.szobteEnabledChansModel[i]})
                    }
                }
            } else {
                if(softwareConfiguration.calibratedIndex === 1) {
                    for (i in dbworker.imEnabledChansModel) {
                        if(i > 0) {
                            if(softwareConfiguration.referenceIndex == 0) {
                                if(parseInt(dbworker.imEnabledChansModel[i]) === softwareConfiguration.referenceChan) {
                                    continue
                                }
                            }
                            append({"ch": dbworker.imEnabledChansModel[i]})
                        }
                    }
                }
            }
            updateCalibratedChannelSet()
        }
        Component.onCompleted: {
            refresh()
        }
    }
    Connections {
        target: dbworker
        function onSzobteEnabledChansModelChanged() {
            if(softwareConfiguration.calibratedIndex === 0) {
                calibratedChanModel.refresh()
            }
        }
    }
    Connections {
        target: dbworker
        function onImEnabledChansModelChanged() {
            if(softwareConfiguration.calibratedIndex === 1) {
                calibratedChanModel.refresh()
            }
        }
    }

    GridLayout {
        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            margins: 10
        }
        columns: 4
        columnSpacing: 10
        Label {
            Layout.columnSpan: 4
            font.pointSize: 20
            text: qsTr("Calibrated")
        }
        Item {
            height: 10
            Layout.columnSpan: 4
        }
        Label {
            Layout.columnSpan: 4
            text: qsTr("Model")
            font.pointSize: 10
        }
        ComboBox {
            id: calibratedAmplifierComboBox
            Layout.columnSpan: 4
            Layout.preferredHeight: 35
            Layout.preferredWidth: 200
            model: [qsTr("OT-EP01-10"), qsTr("IM")]
            onActivated: {
                softwareConfiguration.calibratedIndex = currentIndex
                calibratedChanModel.refresh()
            }
            onCurrentTextChanged: {
                firstCalibratedComboBox.refresh()
            }

            Component.onCompleted: {
                currentIndex = softwareConfiguration.calibratedIndex
            }
        }
        Item {
            height: 10
            Layout.columnSpan: 4
        }
        Label {
            Layout.columnSpan: 4
            text: qsTr("Common correction factor")
            font.pointSize: 10
        }
        DoubleSpinBox {
            Layout.preferredWidth: 200
            editable: true
            onRealValueChanged: {
                hardwareManager.commonCorrectionFactor = realValue
            }
            Component.onCompleted: {
                value = 1000000
            }
        }
        Item {
            height: 10
            Layout.columnSpan: 4
        }
        Label {
            text: qsTr("Channel")
            font.pointSize: 10
        }
        Label {
            text: qsTr("Correction factor")
            font.pointSize: 10
            Layout.columnSpan: 3
        }
        ComboBox {
            id: mainCalibratedChansComboBox
            Layout.preferredHeight: 35
            Layout.preferredWidth: 200
            model: {
                if (calibratedAmplifierComboBox.currentIndex === 0) {
                    if (dbworker !== null) {
                        return dbworker.szobteEnabledChansModel
                    }
                } else {
                    if(dbworker !== null) {
                        return dbworker.imEnabledChansModel
                    }
                }
            }
            onActivated: {
                if(mainReferenceChansComboBox.visible) {
                    if (currentText === mainReferenceChansComboBox.currentText) {
                        currentIndex = -1
                        return
                    }
                }
                for(var i in additionalChannelsListView.contentItem.children) {
                    if(additionalChannelsListView.contentItem.children[i].chan !== undefined) {
                        if(currentText === additionalChannelsListView.contentItem.children[i].chan) {
                            currentIndex = -1
                            break
                        }
                    }
                }
            }
            onCurrentTextChanged: {
                updateCalibratedChannelSet()
                firstCalibratedComboBox.refresh()
            }
        }
        ComboBox {
            id: firstCalibratedComboBox
            property string comment: {
                if(firstCalibratedModel === undefined) {
                    return ""
                }
                if(firstCalibratedModel.get(currentIndex) === undefined) {
                    return ""
                }
                return firstCalibratedModel.get(currentIndex).description
            }
            ListModel {
                id: firstCalibratedModel
            }
            Layout.preferredHeight: 35
            Layout.preferredWidth: 350
            model: DelegateModel {
                model: firstCalibratedModel
                delegate: ItemDelegate {
                    text: factor.toFixed(6) + " (" + description + ")"
                }
            }
            displayText: {
                if(firstCalibratedModel === undefined) {
                    return ""
                }
                if(firstCalibratedModel.get(currentIndex) === undefined) {
                    return ""
                }
                return firstCalibratedModel.get(currentIndex).factor.toFixed(6)
            }
            function refresh() {
                firstCalibratedModel.clear()
                firstCalibratedModel.append({"factor": 1, "description":qsTr("Default value")})
                var calIndex = calibratedAmplifierComboBox.currentIndex
                var calChan = parseInt(mainCalibratedChansComboBox.currentText)
                var factorItem = softwareConfiguration.getSoftwareCorrectionFactor(calIndex, calChan)
                for(var i = factorItem.length - 1; i >= 0 ; i--) {
                    firstCalibratedModel.append({"factor": factorItem[i].first, "description": factorItem[i].second})
                }
                currentIndex = 0
            }
            onDisplayTextChanged: {
                var chan = parseInt(mainCalibratedChansComboBox.currentText)
                if(isNaN(chan)) {
                    return
                }
                var value = parseFloat(displayText)
                if(isNaN(value)) {
                    return
                }
                if(calibratedAmplifierComboBox.currentIndex === 0) {
                    dbworker.szobteSetChannelSoftwareCorrectionFactor(chan, value)
                } else {
                    dbworker.imSetChannelSoftwareCorrectionFactor(chan, value)
                }
            }
        }
        Label {
            text: firstCalibratedComboBox.comment
            font.pointSize: 10
            Layout.columnSpan: 2
        }
        Label {
            text: qsTr("Only the selected channels are displayed in the list. To edit go to <a href='hw'>hardware settings.</a>")
            font.pointSize: 10
            Layout.preferredWidth: 200
            Layout.columnSpan: 4
            wrapMode: Label.WordWrap
            onLinkActivated: {
                goToHardwareSettings()
            }
        }
        Item {
            height: 10
            Layout.columnSpan: 4
        }
        ListView {
            id: additionalChannelsListView
            Layout.fillWidth: true
            Layout.preferredHeight: contentHeight
            Layout.columnSpan: 4
            interactive: false
            spacing: 10
            model: calibratedChanModel
            delegate: GridLayout {
                columns: 4
                rowSpacing: 0
                columnSpacing: 10
                property alias chan: chanComboBox.currentText
                Label {
                    text: qsTr("Channel")
                    font.pointSize: 10
                }
                Label {
                    text: qsTr("Correction factor")
                    font.pointSize: 10
                    Layout.columnSpan: 3
                }
                ComboBox {
                    id: chanComboBox
                    Layout.preferredHeight: 35
                    Layout.preferredWidth: 200
                    model: {
                        if (calibratedAmplifierComboBox.currentIndex === 0) {
                            if (dbworker !== null) {
                                return dbworker.szobteEnabledChansModel
                            }
                        } else {
                            if(dbworker !== null) {
                                return dbworker.imEnabledChansModel
                            }
                        }
                        return []
                    }
                    onActivated: {
                        if (currentText === mainCalibratedChansComboBox.currentText) {
                            currentIndex = -1
                            return
                        }
                        if(mainReferenceChansComboBox.visible) {
                            if (currentText === mainReferenceChansComboBox.currentText) {
                                currentIndex = -1
                                return
                            }
                        }
                        var counter = 0
                        for (var i in additionalChannelsListView.contentItem.children) {
                            if (additionalChannelsListView.contentItem.children[i].chan
                                    !== undefined) {
                                if (currentText === additionalChannelsListView.contentItem.children[i].chan) {
                                    counter++
                                    if (counter > 1) {
                                        currentIndex = -1
                                        break
                                    }
                                }
                            }
                        }
                    }
                    onCurrentTextChanged: {
                        updateCalibratedChannelSet()
                        deletableCalibratedComboBox.refresh()
                    }
                    Component.onCompleted: {
                        if (softwareConfiguration.calibratedIndex === 0) {
                            for(var i in dbworker.szobteEnabledChansModel) {
                                if(ch === dbworker.szobteEnabledChansModel[i]) {
                                    currentIndex = i
                                    return
                                }
                            }
                        } else {
                            for(i in dbworker.imEnabledChansModel) {
                                if(ch === dbworker.imEnabledChansModel[i]) {
                                    currentIndex = i
                                    return
                                }
                            }
                        }
                    }
                }
                ComboBox {
                    id: deletableCalibratedComboBox
                    property string comment: {
                        if(deletableCalibratedModel === undefined) {
                            return ""
                        }
                        if(deletableCalibratedModel.get(currentIndex) === undefined) {
                            return ""
                        }
                        return deletableCalibratedModel.get(currentIndex).description
                    }
                    ListModel {
                        id: deletableCalibratedModel
                    }
                    Layout.preferredHeight: 35
                    Layout.preferredWidth: 350
                    model: DelegateModel {
                        model: deletableCalibratedModel
                        delegate: ItemDelegate {
                            text: factor.toFixed(6) + " (" + description + ")"
                        }
                    }
                    displayText: {
                        if(deletableCalibratedModel === undefined) {
                            return ""
                        }
                        if(deletableCalibratedModel.get(currentIndex) === undefined) {
                            return ""
                        }
                        return deletableCalibratedModel.get(currentIndex).factor.toFixed(6)
                    }
                    onDisplayTextChanged: {
                        var chan = parseInt(chanComboBox.currentText)
                        if(isNaN(chan)) {
                            return
                        }
                        var value = parseFloat(displayText)
                        if(isNaN(value)) {
                            return
                        }
                        if(calibratedAmplifierComboBox.currentIndex === 0) {
                            dbworker.szobteSetChannelSoftwareCorrectionFactor(chan, value)
                        } else {
                            dbworker.imSetChannelSoftwareCorrectionFactor(chan, value)
                        }
                    }
                    function refresh() {
                        deletableCalibratedModel.clear()
                        deletableCalibratedModel.append({"factor": 1, "description":qsTr("Default value")})
                        if(calibratedAmplifierComboBox === undefined) {
                            return
                        }
                        var calIndex = calibratedAmplifierComboBox.currentIndex
                        var calChan = parseInt(chanComboBox.currentText)
                        var factorItem = softwareConfiguration.getSoftwareCorrectionFactor(calIndex, calChan)
                        for(var i = factorItem.length - 1; i >= 0 ; i--) {
                            deletableCalibratedModel.append({"factor": factorItem[i].first, "description": factorItem[i].second})
                        }
                        currentIndex = 0
                    }
                }
                Label {
                    text: deletableCalibratedComboBox.comment
                    font.pointSize: 10
                }
                ToolButton {
                    icon.source: "images/delete.png"
                    Layout.preferredWidth: 35
                    Layout.preferredHeight: 35
                    onClicked: {
                        calibratedChanModel.remove(index)
                    }
                }
            }
            onCountChanged: {
                updateCalibratedChannelSet()
            }
        }
        Button {
            text: qsTr("Add channel")
            font.capitalization: Font.MixedCase
            font.pointSize: 10
            Layout.preferredHeight: 40
            enabled: softwareConfiguration !== null ? calibratedChanModel.count < softwareConfiguration.calibratedEnabledChannelsNumber - 1 : false
            onClicked: {
                var newChan = ""
                if (softwareConfiguration.calibratedIndex === 0) {
                    for (var i in dbworker.szobteEnabledChansModel) {
                        if(dbworker.szobteEnabledChansModel[i] === mainCalibratedChansComboBox.currentText) {
                            continue
                        }
                        var found = false
                        for(var j in additionalChannelsListView.contentItem.children) {
                            if(additionalChannelsListView.contentItem.children[j].chan !== undefined) {
                                if(dbworker.szobteEnabledChansModel[i] === additionalChannelsListView.contentItem.children[j].chan) {
                                    found = true
                                    break
                                }
                            }
                        }
                        if(found === false) {
                            newChan = dbworker.szobteEnabledChansModel[i]
                            break
                        }
                    }
                } else {
                    for (i in dbworker.imEnabledChansModel) {
                        if(dbworker.imEnabledChansModel[i] === mainCalibratedChansComboBox.currentText) {
                            continue
                        }
                        found = false
                        for(j in additionalChannelsListView.contentItem.children) {
                            if(additionalChannelsListView.contentItem.children[j].chan !== undefined) {
                                if(dbworker.imEnabledChansModel[i] === additionalChannelsListView.contentItem.children[j].chan) {
                                    found = true
                                    break
                                }
                            }
                        }
                        if(found === false) {
                            newChan = dbworker.imEnabledChansModel[i]
                            break
                        }
                    }
                }
                calibratedChanModel.append({
                           "ch": newChan
                       })
            }
        }
        Item {
            height: 20
            Layout.columnSpan: 4
        }
        Label {
            Layout.columnSpan: 4
            font.pointSize: 20
            text: qsTr("Reference")
        }
        Item {
            height: 10
            Layout.columnSpan: 4
        }
        Label {
            Layout.columnSpan: 4
            text: qsTr("Model")
            font.pointSize: 10
        }
        ComboBox {
            id: referenceAmplifierComboBox
            Layout.columnSpan: 4
            Layout.preferredHeight: 35
            Layout.preferredWidth: 200
            model: [qsTr("OT-EP01-10"), qsTr("Intuition 20i"), qsTr(
                    "Tenzo-M VK-10")]
            onActivated: {
                softwareConfiguration.referenceIndex = currentIndex
                switch(currentIndex) {
                case 0:
                    referenceChannelChanged(softwareConfiguration.referenceChan)
                    break
                case 1:
                    referenceChannelChanged(11)
                    break
                case 2:
                    referenceChannelChanged(24)
                    break
                }
            }
            Component.onCompleted: {
                currentIndex = softwareConfiguration.referenceIndex
                switch(currentIndex) {
                case 0:
                    referenceChannelChanged(softwareConfiguration.referenceChan)
                    break
                case 1:
                    referenceChannelChanged(11)
                    break
                case 2:
                    referenceChannelChanged(24)
                    break
                }
            }
        }   
        Item {
            height: 20
            Layout.columnSpan: 4
        }
        Label {
            visible: referenceAmplifierComboBox.currentIndex == 0
            text: qsTr("Channel")
            font.pointSize: 10
        }
        Label {
            visible: referenceAmplifierComboBox.currentIndex == 0
            text: qsTr("Correction factor")
            font.pointSize: 10
            Layout.columnSpan: 3
        }
        ComboBox {
            id: mainReferenceChansComboBox
            Layout.preferredHeight: 35
            Layout.preferredWidth: 200
            visible: referenceAmplifierComboBox.currentIndex == 0
            model: {
                if (calibratedAmplifierComboBox.currentIndex === 0) {
                    if (dbworker !== null) {
                        return dbworker.szobteEnabledChansModel
                    }
                } else {
                    if (dbworker !== null) {
                        return dbworker.imEnabledChansModel
                    }
                }
            }
            onActivated: {
                if (currentText === mainCalibratedChansComboBox.currentText) {
                    currentIndex = -1
                    referenceChannelChanged(0)
                    return
                }
                for(var i in additionalChannelsListView.contentItem.children) {
                    if(additionalChannelsListView.contentItem.children[i].chan !== undefined) {
                        if(currentText === additionalChannelsListView.contentItem.children[i].chan) {
                            currentIndex = -1
                            break
                        }
                    }
                }
                softwareConfiguration.referenceChan = currentText
                referenceChannelChanged(softwareConfiguration.referenceChan)
            }
            onCurrentTextChanged: {
                updateCalibratedChannelSet()
                szobteReferenceComboBox.refresh()
            }
            Component.onCompleted: {
                currentIndex = -1
                if(referenceAmplifierComboBox.currentIndex == 0) {
                    for(var i in dbworker.szobteEnabledChansModel) {
                        if(parseInt(dbworker.szobteEnabledChansModel[i]) === softwareConfiguration.referenceChan) {
                            currentIndex = i
                            return
                        }
                    }
                }
            }
        }
        ComboBox {
            id: szobteReferenceComboBox
            property string comment: {
                if(szobteReferenceModel === undefined) {
                    return ""
                }
                if(szobteReferenceModel.get(currentIndex) === undefined) {
                    return ""
                }
                return szobteReferenceModel.get(currentIndex).description
            }
            ListModel {
                id: szobteReferenceModel
            }
            Layout.preferredHeight: 35
            Layout.preferredWidth: 350
            model: DelegateModel {
                model: szobteReferenceModel
                delegate: ItemDelegate {
                    text: factor.toFixed(6) + " (" + description + ")"
                }
            }
            displayText: {
                if(szobteReferenceModel === undefined) {
                    return ""
                }
                if(szobteReferenceModel.get(currentIndex) === undefined) {
                    return ""
                }
                return szobteReferenceModel.get(currentIndex).factor.toFixed(6)
            }
            onDisplayTextChanged: {
                if(referenceAmplifierComboBox.currentIndex !== 0) { // работает только для китайского!
                    return
                }
                var chan = parseInt(mainReferenceChansComboBox.currentText)
                if(isNaN(chan)) {
                    return
                }
                var value = parseFloat(displayText)
                if(isNaN(value)) {
                    return
                }
                dbworker.szobteSetChannelSoftwareCorrectionFactor(chan, value)
            }
            function refresh() {
                szobteReferenceModel.clear()
                szobteReferenceModel.append({"factor": 1, "description":qsTr("Default value")})
                var calIndex = calibratedAmplifierComboBox.currentIndex
                var calChan = parseInt(mainReferenceChansComboBox.currentText)
                var factorItem = softwareConfiguration.getSoftwareCorrectionFactor(calIndex, calChan)
                for(var i = factorItem.length - 1; i >= 0 ; i--) {
                    szobteReferenceModel.append({"factor": factorItem[i].first, "description": factorItem[i].second})
                }
                currentIndex = 0
            }
            visible: referenceAmplifierComboBox.currentIndex == 0
            onVisibleChanged: {
                if(visible) {
                    refresh()
                }
            }
        }
        Label {
            visible: referenceAmplifierComboBox.currentIndex == 0
            text: szobteReferenceComboBox.comment
            font.pointSize: 10
            Layout.columnSpan: 2
        }
        Label {
            visible: referenceAmplifierComboBox.currentIndex == 0
            text: qsTr("Only the selected channels are displayed in the list. To edit go to <a href='hw'>hardware settings.</a>")
            font.pointSize: 10
            Layout.preferredWidth: 200
            Layout.columnSpan: 4
            wrapMode: Label.WordWrap
            onLinkActivated: {
                goToHardwareSettings()
            }
        }
        Item {
            visible: referenceAmplifierComboBox.currentIndex == 0
            height: 20
            Layout.columnSpan: 4
        }
    }
    function updateCalibratedChannelSet() {
        var calibratedChannelList = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
        var ch = parseInt(mainCalibratedChansComboBox.currentText)
        if(ch > 0) {
            if(softwareConfiguration.calibratedIndex === 0) {
                calibratedChannelList[ch-1] = true
            } else {
                calibratedChannelList[ch+9] = true
            }
        }
        for(var i in additionalChannelsListView.contentItem.children) {
            if(additionalChannelsListView.contentItem.children[i].chan !== undefined) {
                ch = parseInt(additionalChannelsListView.contentItem.children[i].chan)
                if(ch > 0) {
                    if(softwareConfiguration.calibratedIndex === 0) {
                        calibratedChannelList[ch-1] = true
                    } else {
                        calibratedChannelList[ch+9] = true
                    }
                }
            }
        }
        calibratedChannelSetChanged(calibratedChannelList)
    }
}
